package chess.gui;

import chess.board.*;
import chess.operations.Serialization;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

public class AnalysisStateScore extends JFrame {

    public static void analyse(BidirectionalTree<State> tree, String title, Player w, Player b, ShareInstances si) {
        //System.out.println("AnalysisStateScore");
        if(tree == null) {
            JOptionPane.showMessageDialog(null,"Internt fel. Inget analysträd att visa.");
            return;
        }
        //System.out.println("AnalysisStateScore tree != null");
        AnalysisStateScore ass = new AnalysisStateScore(tree, title, w, b, si);
        ass.setVisible(true);
        //System.out.println("AnalysisStateScore efter");
    }

    private BidirectionalTree<State> tree = null;
    private DefaultTableModel model = null;
    private JPanel topPanel;
    private JScrollPane scrollPane;
    private JTable table = null;
    private Vector<Vector<Object>> rows = null;
    private ShareInstances si;
    private Player white;
    private Player black;

    public AnalysisStateScore(BidirectionalTree<State> tree, String title, Player w, Player b, ShareInstances si) {
        this.tree = tree;
        this.si = si;
        setTitle(title);
        white = w;
        black = b;
        setupTable();
        setLocation(0, 0);
        setSize(200, 500);
        setContentPane(scrollPane);
    }

    private void setupTable() {
        Vector<String> columnNames = new Vector<>();
        columnNames.add("Drag");
        columnNames.add("Poäng");
        rows = new Vector<>();
        for(int idx = 0 ; idx < tree.successors.size() ; idx++) {
            Vector<Object> row = new Vector<>();
            BidirectionalTree<State> bs = tree.successors.get(idx);
            System.out.println("Barn "+idx+" har "+bs.successors.size()+" efterföljare. Värdet "+bs.value);
            State s = bs.current;
            String m = s.lastMove.toString();
            row.add(new TableCell(m, bs, () -> visualiseBoard(bs)));
            row.add(new TableCell(""+bs.value, bs, () -> analysisNextStep(bs)));
            rows.add(row);
        }

        model = new DefaultTableModel(rows, columnNames);
        table = new JTable(model);
        //table.setModel(model);
        table.getColumn("Drag").setCellRenderer(new ButtonRenderer());
        table.getColumn("Drag").setCellEditor(new ButtonEditor(new JCheckBox()));
        table.getColumn("Poäng").setCellRenderer(new ButtonRenderer());
        table.getColumn("Poäng").setCellEditor(new ButtonEditor(new JCheckBox()));

        scrollPane = new JScrollPane(table);
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());

        topPanel.add(scrollPane, BorderLayout.CENTER);
        button.addActionListener(e ->  {
                TableButton btn = (TableButton) e.getSource();
                //System.out.println("Klick "+btn);
                TableCell tc = (TableCell) ((Vector) rows.get(btn.getRow())).get(btn.getColumn());
                tc.run();
                //AnalysisStateScore ass = new AnalysisStateScore(tc.bs, getTitle()+"->"+tc.toString(), illustration);
                //System.out.println("KLICK: "+tc);
                //ass.setVisible(true);
            }
        );
    }

    private void analysisNextStep(BidirectionalTree<State> bs) {
        AnalysisStateScore ass = new AnalysisStateScore(bs, getTitle()+"->"+bs.current.lastMove.toString(), white , black, si);
        ass.setVisible(true);
        MatchIllustration ill = si.illustration;
        if(ill == null) {
            ill = new MatchIllustration((Match)null, 2);
            si.illustration = ill;
            final JFrame f = new JFrame("Schackbräde");
            f.setLocation(0, 0);
            f.add(ill);
            f.pack();
            f.setVisible(true);
            f.addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    si.illustration = null;
                }
            });
        }
        ill.setState(bs.current);
        ill.repaint();

        java.util.List<String> list = AnalysisTableOfMoves.staticEvaluation(ill, white, black, ":2");
        if(si.analysis != null) {
            java.util.List<String> all = new ArrayList<>();
            all.addAll(si.analysis);
            all.addAll(list);
            Collections.sort(all);
            all.forEach(s -> System.out.println("| "+s));
            si.analysis = list;
        }
    }

    private void visualiseBoard(BidirectionalTree<State> bs) {
        //System.out.println("visualiseBoard "+Serialization.getStateString(bs.current));
        MatchIllustration ill = si.illustration;
        if(ill == null) {
            ill = new MatchIllustration((Match)null, 2);
            si.illustration = ill;
            final JFrame f = new JFrame("Schackbräde");
            f.setLocation(0, 0);
            f.add(ill);
            f.pack();
            f.setVisible(true);
            f.addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    si.illustration = null;
                }
            });
        }
        ill.setState(bs.current);
        ill.repaint();
    }

    public static class ButtonRenderer extends JButton implements TableCellRenderer {

        public ButtonRenderer() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table,
                                                       Object value, // -> TableCell
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            TableCell tc = (TableCell) value;
            tc.setRow(row);
            tc.setColumn(column);
            setText(value.toString());
            //addActionListener(e -> System.out.println("ButtonRenderer, action listener"));
            //System.out.println("value klass "+value.getClass().getName()); // -> TableCell
            return this;
        }

    }

    TableButton button = new TableButton();

    public class ButtonEditor extends DefaultCellEditor {

        private String label;

        public ButtonEditor(JCheckBox checkBox) {
            super(checkBox);
        }
        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            label = value.toString();
            button.setText(label);
            button.setRow(row);
            button.setColumn(column);
            //button.addActionListener(e -> System.out.println("Klick "+label));
            return button;
        }

        public Object getCellEditorValue() {
            TableCell tc = (TableCell) ((Vector) rows.get(button.getRow())).get(button.getColumn());
            return new TableCell(label, tc); // fixa
        }

    }

    public class TableButton extends JButton {
        private int row;
        private int column;

        public String toString() {
            return "TableButton{row="+row+", column="+column+"}";
        }

        public TableButton() {
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getRow() {
            return row;
        }


        public void setColumn(int column) {
            this.column = column;
        }

        public int getColumn() {
            return column;
        }

        /*
        private Runnable runnable;

        public void setRunnable(Runnable runnable) {
            this.runnable = runnable;
        }

        public Runnable getRunnable() {
            return runnable;
        }
         */
    }

    public class TableCell {

        private String appearance = null;
        private BidirectionalTree<State> bs = null;
        private Runnable runnable = null;

        public TableCell(String appearance, BidirectionalTree<State> bs, Runnable runnable) {
            this.appearance = appearance;
            this.bs = bs;
            this.runnable = runnable;
        }

        public TableCell(String appearance, TableCell other) {
            this.appearance = appearance;
            this.bs = other.bs;
            this.runnable = other.runnable;
            this.row = other.row;
            this.column = other.column;
        }

        public String toString() {
            return appearance;
        }

        public String toStringMore() {
            return appearance+", row="+row+", column="+column;
        }

        private int row;

        public void setRow(int row) {
            this.row = row;
        }

        public int getRow() {
            return row;
        }

        private int column;

        public void setColumn(int column) {
            this.column = column;
        }

        public int getColumn() {
            return column;
        }

        public void run() {
            if(runnable == null) {
                JOptionPane.showMessageDialog(null,"Internt fel. Ingen exekutabel.");
                return;
            }
            runnable.run();
        }

    }

}
