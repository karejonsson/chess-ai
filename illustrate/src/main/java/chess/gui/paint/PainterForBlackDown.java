package chess.gui.paint;

import chess.board.Board;
import chess.board.Square;
import chess.board.pieces.Piece;

import java.awt.*;
import java.awt.image.ImageObserver;

public class PainterForBlackDown extends Painter {

    public PainterForBlackDown() {
        super(1.0);
    }

    public PainterForBlackDown(double magn) {
        super(magn);
    }

    @Override
    public void fillRect(byte rank, byte file) {
        fillRect_internal((byte)(Board.ranks-1-rank), (byte)(Board.files-1-file));
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file) {
        drawFigure_internal(piece, (byte)(Board.ranks-1-rank), (byte)(Board.files-1-file));
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file, boolean dragging, Square from) {
        drawFigure_internal(piece, (byte)(Board.ranks-1-rank), (byte)(Board.files-1-file), dragging, new Square((byte)(Board.ranks-1-from.rank), (byte)(Board.files-1-from.file)));
    }

    @Override
    public void draw(Graphics g, ImageObserver canvas) {
        g.drawImage(buffer, 0, 0, canvas);
    }

    @Override
    public Square getPoint(int x, int y) {
        return new Square((byte)(y/dely), (byte)(Board.files-1 - x/delx));
    }

}

