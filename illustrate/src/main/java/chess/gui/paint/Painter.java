package chess.gui.paint;

import chess.board.Board;
import chess.board.Square;
import chess.board.pieces.Piece;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.Arrays;

public abstract class Painter {

    public static final byte STDDELX = 25;
    public static final byte STDDELY = 25;

    protected int delx;
    protected int dely;
    protected double magn;

    protected Image buffer;
    protected Graphics gctx;

    Painter(double magn) {
        this.delx = (int)(STDDELX*magn);
        this.dely = (int)(STDDELY*magn);
        this.magn = magn;
    }

    public boolean hasImageBuffer() {
        return buffer != null;
    }

    public void setImageBuffer(Image buffer) {
        this.buffer = buffer;
        gctx = buffer.getGraphics();
    }

    protected void fillRect_internal(byte rank, byte file) {
        if(gctx == null) {
            return;
        }
        if((rank+file)%2 == 1) {
            gctx.setColor(Color.gray);
        }
        else {
            gctx.setColor(Color.lightGray);
        }
        gctx.fillRect(file*delx,rank*dely, delx, dely);
    }

    protected void drawFigure_internal(Piece piece, byte rank, byte file) {
        drawFigure_internal(piece, rank, file, false, null);
    }

    protected void drawFigure_internal(Piece piece, byte rank, byte file, boolean dragging, Square from) {
        if(gctx == null) {
            return;
        }
        if (dragging && from.isOn(rank, file))
            gctx.setColor(Color.red);
        else if (piece.getColor())
            gctx.setColor(Color.white);
        else
            gctx.setColor(Color.black);
        gctx.setFont(new Font("Serif", Font.BOLD,STDDELY-5));
        draw_internal(gctx, file*delx, (Board.ranks-rank)*dely, piece.index(), magn);
    }

    private int[][] x_magnShapes= new int[8][];
    private int[][] y_magnShapes= new int[8][];

    private int[] getXShape(byte idx, double magn) {
        if(x_magnShapes[idx] != null) {
            return x_magnShapes[idx];
        }
        int[] shape = new int[PiecesGraphic.x_shapes[idx].length];
        for(int i = 0 ; i < shape.length ; i++) {
            shape[i] = (int)(PiecesGraphic.x_shapes[idx][i]*magn);
        }
        x_magnShapes[idx] = shape;
        return shape;
    }

    private int[] getYShape(byte idx, double magn) {
        if(y_magnShapes[idx] != null) {
            return y_magnShapes[idx];
        }
        int[] shape = new int[PiecesGraphic.y_shapes[idx].length];
        for(int i = 0 ; i < shape.length ; i++) {
            shape[i] = (int)(PiecesGraphic.y_shapes[idx][i]*magn);
        }
        y_magnShapes[idx] = shape;
        return shape;
    }

    private void draw_internal(Graphics g, int x, int y, byte idx, double magn) {
        x -= (int)(magn*2);
        y += (int)(magn*3);
        int[] x_shape = getXShape(idx, magn);
        int[] y_shape = getYShape(idx, magn);
        final int len = x_shape.length;
        int[] X = Arrays.copyOf(x_shape, len);
        int[] Y = Arrays.copyOf(y_shape, len);
        for (int i = 0 ; i < len ; i++) {
            X[i] += x;
            Y[i] = y-Y[i];
        }
        g.fillPolygon(X, Y, len);
        if(idx == 0) {
            g.fillOval(
                    x+(int)(magn*(Painter.STDDELX-1)/2),
                    y-(int)(magn*(Painter.STDDELX-2)),
                    (int)(magn*((Painter.STDDELX-1)/4)),
                    (int)(magn*((Painter.STDDELY-1)/4)));
        }
        if(idx == 5) {
            g.drawLine(
                    x+(int)(magn*((Painter.STDDELX+1)/2)),
                    y-(int)(magn*Painter.STDDELY),
                    x+(int)(magn*((Painter.STDDELX+9)/2)),
                    y-(int)(magn*Painter.STDDELY));
            g.drawLine(
                    x+(int)(magn*((Painter.STDDELX+5)/2)),
                    y-(int)(magn*(Painter.STDDELY+2)),
                    x+(int)(magn*((Painter.STDDELX+5)/2)),
                    y-(int)(magn*(Painter.STDDELY-2)));
        }
    }

    public abstract void fillRect(byte rank, byte file);
    public abstract void drawFigure(Piece piece, byte rank, byte file);
    public abstract void drawFigure(Piece piece, byte rank, byte file, boolean dragging, Square from);
    public abstract void draw(Graphics g, ImageObserver chessCanvas);
    public abstract Square getPoint(int x, int y);

}
