package chess.gui.paint;

public class PiecesGraphic {

    public static final int[] x_bishop = {5,13,17,25,25,17,18,20,20,19,17,18,14,13,12,11,10,10,11,12,5,5};
    public static final int[] y_bishop = {5,7,7,5,8,8,10,15,17,20,25,16,25,23,21,18,15,12,10,8,8,5};

    public static final int[] x_archbishop = {5,13,17,25,25,18,19,21,21,20,18,19,15,14,13,11,11,9,10,7,5,4,6,8,11,13,14,15,14,13,5};
    public static final int[] y_archbishop = {5,7,7,5,8,8,10,15,17,20,25,16,26,23,23,25,23,24,22,17,15,14,13,12,15,17,16,16,13,11,5};

    public static final int[] x_king = {10,20,22,22,24,25,25,24,22,20,18,17,15,13,12,10,8,6,5,5,6,8,8,10};
    public static final int[] y_king = {5,5,6,10,13,15,17,18,20,20,18,22,23,22,18,20,20,18,17,15,13,10,6,5};

    public static final int[] x_knight = {11,25,24,23,22,20,17,14,12,12,10,11,8,6,5,7,9,12,14,15,16,15,14,13,11};
    public static final int[] y_knight = {5,5,12,16,19,21,23,23,25,23,24,22,17,15,14,13,12,15,17,16,16,13,11,8,5};

    public static final int[] x_pawn = {5,25,23,21,18,20,20,18,22,20,17,13,10,8,12,10,10,12,9,7,5};
    public static final int[] y_pawn = {5,5,7,8,9,11,14,16,16,17,18,18,17,16,16,14,11,9,8,7,5};

    public static final int[] x_queen = {10,20,20,27,19,20,15,10,11,2,10,10};
    public static final int[] y_queen = {5,5,10,18,14,25,16,25,14,18,10,5};

    public static final int[] x_rook = {5,5,10,10,5,5,25,25,20,20,25,25,22,22,18,18,12,12,8,8,5};
    public static final int[] y_rook = {25,18,18,8,8,5,5,8,8,18,18,25,25,22,22,25,25,22,22,25,25};

    public static final int[] x_chancellor = {5,25,25,20,20,25,25,22,22,18,18,15,17,14,12,12,10,11,8,6,5,7,9,12,14,15,16,15,14,13,11,5};
    public static final int[] y_chancellor = {5,5,8,8,18,18,25,25,22,22,25,25,23,23,25,23,24,22,17,15,14,13,12,15,17,16,16,13,11,8,5,5};

    public static final int[][] x_shapes = new int[][] {
        x_pawn,
        x_knight,
        x_bishop,
        x_rook,
        x_queen,
        x_king,
        x_chancellor,
        x_archbishop
    };

    public static final int[][] y_shapes = new int[][] {
        y_pawn,
        y_knight,
        y_bishop,
        y_rook,
        y_queen,
        y_king,
        y_chancellor,
        y_archbishop
    };

}
