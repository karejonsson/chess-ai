package chess.gui.paint;

import chess.board.Board;
import chess.board.Square;
import chess.board.pieces.Piece;

import java.awt.*;
import java.awt.image.ImageObserver;

public class PainterForWhiteDown extends Painter {

    public PainterForWhiteDown() {
        super(1.0);
    }

    public PainterForWhiteDown(double magn) {
        super(magn);
    }

    @Override
    public void fillRect(byte rank, byte file) {
        fillRect_internal(rank, file);
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file) {
        drawFigure_internal(piece, rank, file);
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file, boolean dragging, Square from) {
        drawFigure_internal(piece, rank, file, dragging, from);
    }

    @Override
    public void draw(Graphics g, ImageObserver canvas) {
        g.drawImage(buffer, 0, 0, canvas);
    }

    @Override
    public Square getPoint(int x, int y) {
        return new Square((byte)(Board.ranks-1 - (int) y/dely), (byte)(x/delx));
    }

}

