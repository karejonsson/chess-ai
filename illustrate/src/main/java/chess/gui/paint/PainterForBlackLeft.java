package chess.gui.paint;

import chess.board.Board;
import chess.board.Square;
import chess.board.pieces.Piece;

import java.awt.Graphics;
import java.awt.image.ImageObserver;

public class PainterForBlackLeft extends Painter {

    public PainterForBlackLeft() {
        super(1.0);
    }

    public PainterForBlackLeft(double magn) {
        super(magn);
    }

    @Override
    public void fillRect(byte rank, byte file) {
        fillRect_internal((byte)(Board.files-1-file), (byte)(Board.ranks-1-rank));
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file) {
        drawFigure_internal(piece, (byte)(Board.files-1-file), (byte)(Board.ranks-1-rank));
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file, boolean dragging, Square from) {
        drawFigure_internal(piece, (byte)(Board.files-1-file), (byte)(Board.ranks-1-rank), dragging, new Square((byte)(Board.files-1-from.file), (byte)(Board.ranks-1-from.rank)));
    }

    @Override
    public void draw(Graphics g, ImageObserver canvas) {
        g.drawImage(buffer, 0, 0, canvas);
    }

    @Override
    public Square getPoint(int x, int y) {
        return new Square((byte)(Board.ranks-1 - (byte) x/dely), (byte)(y/delx));
    }

}

