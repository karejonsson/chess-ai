package chess.gui.paint;

import chess.board.Board;
import chess.board.Square;
import chess.board.pieces.Piece;

import java.awt.Graphics;
import java.awt.image.ImageObserver;

public class PainterForWhiteLeft extends Painter {

    public PainterForWhiteLeft() {
        super(1.0);
    }

    public PainterForWhiteLeft(double magn) {
        super(magn);
    }

    @Override
    public void fillRect(byte rank, byte file) {
        fillRect_internal(file, rank);
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file) {
        drawFigure_internal(piece, file, rank);
    }

    @Override
    public void drawFigure(Piece piece, byte rank, byte file, boolean dragging, Square from) {
        drawFigure_internal(piece, file, rank, dragging, new Square(from.file, from.rank));
    }

    @Override
    public void draw(Graphics g, ImageObserver canvas) {
        g.drawImage(buffer, 0, 0, canvas);
    }

    @Override
    public Square getPoint(int x, int y) {
        return new Square((byte)(x/dely), (byte)(Board.files-1 - (byte) y/delx));
    }

}

