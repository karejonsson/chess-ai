package chess.gui;

import chess.board.MatchSurveillor;
import chess.board.Move;
import chess.board.State;
import chess.gui.paint.PainterForWhiteDown;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.ArrayList;

public class TableOfMoves {

    private MatchSurveillor surveillor = null;
    private chess.gui.paint.Painter painter = null;
    private AbstractTableModel tm = null;
    private JTable table = null;

    private ArrayList<RowOfMoves> rows = null;
    private int ctr = 0;
    private JFrame f = null;
    private JTextArea ta = null;

    public TableOfMoves(MatchSurveillor surveillor) {
        this.surveillor = surveillor;
        //ill = new MatchIllustration(board);
        painter = new PainterForWhiteDown();
        setupTable();
        addMyselfToFrame();
        init();
        surveillor.addMoveListener(this, m -> addMove(m.state));
        surveillor.addInitListener(() -> init());
    }

    private void addMyselfToFrame() {
        f = new JFrame("Gjorda drag");
        //bottom.setSize(200, 1600);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        f.setLocation(1000, 0);
        f.setSize(500, 1600);
        f.setContentPane(scrollPane);
        JMenuBar mb = new JMenuBar();
        JMenu handle = new JMenu("Funktioner");
        mb.add(handle);
        JMenuItem mi = new JMenuItem("Skriv ut");
        handle.add(mi);
        mi.addActionListener(e -> System.out.println("HEJ"));
        f.setJMenuBar(mb);
        f.setVisible(true);
    }

    private void setupTable() {
        ArrayList<String> columnNames = new ArrayList<>();
        columnNames.add("Drag");
        columnNames.add("Vit");
        columnNames.add("Svart");
        rows = new ArrayList<>();

        tm = new AbstractTableModel() {
            public int getColumnCount() { return columnNames.size(); }
            public String getColumnName(int col) {
                return columnNames.get(col);
            }
            public int getRowCount() { return rows.size(); }
            public Object getValueAt(int row, int col) {
                if(col == 0) {
                    return row+1;
                }
                if(row >= rows.size()) {
                    return "";
                }
                RowOfMoves _row = rows.get(row);
                Move m = col == 1 ?
                        (_row.white != null ? _row.white.lastMove : null) :
                        (_row.black != null ? _row.black.lastMove : null);
                return new JLabel(m == null ? "" : m.toString());
                /*
                JButton btn = new JButton(m == null ? "" : m.toString());
                btn.addActionListener(e -> System.out.println(""+m));
                return btn;
                 */
            }
            public boolean isCellEditable(int row, int col) {
                return true;
            }
            public void setValueAt(Object value, int row, int col) {
                try {
                    while(row >= rows.size()) {
                        rows.add(new RowOfMoves());
                    }
                    RowOfMoves _row = rows.get(row);
                    if(value instanceof State) {
                        if(col == 1) {
                            _row.white = (State) value;
                        }
                        else {
                            _row.black = (State) value;
                            if(_row.white == null && _row.white.previous != null) {
                                _row.white = _row.white.previous;
                            }
                        }
                    }
                    fireTableCellUpdated(row, col);
                }
                catch(Exception e) {}
            }
        };
        table = new JTable(tm);

        table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                if (value instanceof JComponent) {
                    //((JComponent) value).updateUI();//<------------------
                    return (JComponent) value;
                } else {
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            }
        });
    }

    public void repaint() {
        tm.fireTableDataChanged();
        table.repaint();
        f.repaint();
    }

    public void init() {
        rows.clear();
        ctr = 0;
        repaint();
    }

    public void addMove(State state) {
        // The move is done, inverse logic
        if(!state.turn) {
            //System.out.println("Protokollet vit: "+state.lastMove);
            tm.setValueAt(state, ctr, 1);
        }
        else {
            //System.out.println("Protokollet svart: "+state.lastMove);
            tm.setValueAt(state, ctr, 2);
            ctr++;
        }
        repaint();
    }

}
