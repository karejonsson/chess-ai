package chess.gui;

import chess.board.*;
import chess.gui.paint.PainterForWhiteDown;
import chess.operations.Serialization;
import chess.rules.Check;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

public class AnalysisTableOfMoves {

    private MatchNotifying board = null;
    private MatchSurveillor surveillor = null;
    private MatchIllustration ill = null;
    private chess.gui.paint.Painter painter = null;

    private DefaultTableModel model;
    private JPanel topPanel;
    private JScrollPane scrollPane;
    private JTable table = null;

    private Vector rows = null;
    private int ctr = 0;
    private JFrame f = null;
    private JPanel r = null;
    private JTextArea ta = null;

    private ShareInstances si = new ShareInstances();

    public AnalysisTableOfMoves(MatchNotifying board) {
        this.board = board;
        this.surveillor = board.surveillor;
        ill = new MatchIllustration(board);
        painter = new PainterForWhiteDown();
        setupTable();
        addMyselfToFrame();
        init();
        surveillor.addMoveListener(this, m -> addMove(m.state));
        surveillor.addInitListener(() -> init());
        addAction("Matchen", () -> ill.setMatch(board));
        addAction("Sträng", () -> System.out.println(Serialization.getStateString(ill.getState())));
        addAction("Analys", () -> analyse());
        addAction("Statisk värdering, minns", () -> staticEvaluation());
        addAction("Statisk värdering, jämför", () -> staticEvaluationCompare());
        addAction("Matt?", () -> System.out.println(Check.isMate(ill.getState())));
    }

    private void staticEvaluation() {
        si.analysis = staticEvaluation(ill, board.getPlayer(true), board.getPlayer(false), ":1");
    }

    private void staticEvaluationCompare() {
        if(si.analysis == null) {
            JOptionPane.showMessageDialog(null,"Inget att utgå från");
            return;
        }
        java.util.List<String> next = staticEvaluation(ill, board.getPlayer(true), board.getPlayer(false), ":2");
        java.util.List<String> all = new ArrayList<>();
        all.addAll(si.analysis);
        all.addAll(next);
        Collections.sort(all);
        all.forEach(s -> System.out.println("| "+s));
    }

    public static void staticEvaluation(Wrapper<MatchIllustration> illustration, MatchNotifying board) {
        MatchIllustration ill = illustration.value;
        if(ill == null) {
            ill = new MatchIllustration((Match)null, 2);
            illustration.value = ill;
            final JFrame f = new JFrame("Schackbräde");
            f.setLocation(0, 0);
            f.add(ill);
            f.pack();
            f.setVisible(true);
            f.addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    illustration.value = null;
                }
            });
        }
        ill.setState(board.state);
        ill.repaint();
        staticEvaluation(ill, board.getPlayer(true), board.getPlayer(false), ":1");
    }

    public static java.util.List<String> staticEvaluation(MatchIllustration ill, Player w, Player b, String extra) {
        State state = ill.getState();
        java.util.List<String> trace = new ArrayList<>();
        java.util.List<String> out = new ArrayList<>();
        try {
            final int i = w.staticEval(state, trace);
            int score = i;
            System.out.println("Vits statiska analys ----------------------------------------");
            for (String line : trace) {
                System.out.println(line);
                if(line.substring(0, 5). contains(":")) {
                    int idx = line.indexOf(":");
                    String replacement = line.substring(0, idx)+extra+line.substring(idx);
                    out.add(replacement);
                }
                else {
                    out.add(line);
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"Vits statiska analys fungerar ej\n"+e.getMessage());
        }

        trace.clear();
        try {
            final int i = b.staticEval(state, trace);
            System.out.println("Svarts statiska analys ----------------------------------------");
            for(String line : trace) {
                System.out.println(line);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"Svarts statiska analys fungerar ej\n"+e.getMessage());
        }
        return out;
    }

    private static int getMoveNr(State s) {
        int depth = 0;
        while(s != null) {
            depth++;
            s = s.previous;
        }
        return (int) (depth/2);
    }

    private void analyse() {
        //System.out.println("AnalysisTableOfMoves.analyse");
        State scur = ill.getState();
        if(scur == null) {
            JOptionPane.showMessageDialog(null,"Internt fel. Inget tillstånd uppslaget.");
            return;
        }
        //System.out.println("AnalysisTableOfMoves.analyse 1");
        State prev = scur.previous;
        if(prev == null) {
            JOptionPane.showMessageDialog(null,"Internt fel. Inget föregående tillstånd uppslaget.");
            return;
        }
        //System.out.println("AnalysisTableOfMoves.analyse 2");
        Player p = board.getPlayer(prev.turn);
        BidirectionalTree<State> tree = p.analysis(prev);
        if(tree == null) {
            JOptionPane.showMessageDialog(null,"Spelaren som gjorde draget implementerar inte analysfunktionen");
            return;
        }
        //System.out.println("AnalysisTableOfMoves.analyse 3");
        AnalysisStateScore.analyse(tree,
                scur.lastMove.toString()+
                        ", "+(scur.turn ? "Svart" : "Vit")+
                ", drag "+getMoveNr(scur),
                board.getPlayer(true),
                board.getPlayer(false),
                si
                );
        //System.out.println("AnalysisTableOfMoves.analyse 4");
    }

    private void addMyselfToFrame() {
        f = new JFrame("Gjorda drag");
        //bottom.setSize(200, 1600);
        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        JScrollPane scrollPane = new JScrollPane(table);
        split.setLeftComponent(scrollPane);
        r = new JPanel();
        r.setLayout(new BoxLayout(r, BoxLayout.Y_AXIS));

        if(ill != null) {
            r.add(ill);
        }

        final JTextArea ta = new JTextArea();
        r.add(ta);
        split.setRightComponent(r);
        //split.setOneTouchExpandable(true);
        split.setDividerLocation(250);
        table.setFillsViewportHeight(true);
        f.setLocation(1000, 0);
        f.setSize(500, 1600);
        f.setContentPane(split);
        JMenuBar mb = new JMenuBar();
        JMenu handle = new JMenu("Funktioner");
        mb.add(handle);
        JMenuItem mi = new JMenuItem("Skriv ut");
        handle.add(mi);
        mi.addActionListener(e -> printToFile());
        f.setJMenuBar(mb);
        f.setVisible(true);
    }

    private void printToFile() {
        FileWriter fw = null;
        String filePath = null;
        try {
            File out = new File(""+System.currentTimeMillis()+".chessprot");
            filePath = out.getCanonicalPath();
            fw = new FileWriter(out);
            board.write(fw);
            fw.close();
            JOptionPane.showMessageDialog(null,"Skapad fil: "+filePath);
        }
        catch(Exception e) {
            try { fw.close(); } catch(Exception ee) {}
            JOptionPane.showMessageDialog(null,"Kunde ej skriva fil.\n"+e.getMessage());
        }
    }

    public void addAction(String caption, Runnable runnable) {
        JButton evalWBtn = new JButton(caption);
        evalWBtn.addActionListener(e -> runnable.run());
        r.add(evalWBtn);
        repaint();
        f.invalidate();
        f.validate();
        f.repaint();
        r.repaint();
    }

    private void setupTable() {
        Vector<String> columnNames = new Vector<>();
        columnNames.add("Drag");
        columnNames.add("Vit");
        columnNames.add("Svart");
        rows = new Vector<>();

        model = new DefaultTableModel(rows, columnNames);
        table = new JTable(model);
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(35);
        //table.setModel(model);
        table.getColumn("Vit").setCellRenderer(new ButtonRenderer());
        table.getColumn("Vit").setCellEditor(new ButtonEditor(new JCheckBox()));
        table.getColumn("Svart").setCellRenderer(new ButtonRenderer());
        table.getColumn("Svart").setCellEditor(new ButtonEditor(new JCheckBox()));

        scrollPane = new JScrollPane(table);
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());

        topPanel.add(scrollPane, BorderLayout.CENTER);
        button.addActionListener(e ->  {
                 TableButton btn = (TableButton) e.getSource();
                 //String text = btn.getText();
                 show(btn.getRow(), btn.getColumn());
             }
        );
    }

    public void show(int r, int c) {
        //System.out.println("Row="+r+", column="+c);
        TableCell tc = (TableCell) ((Vector) rows.get(r)).get(c);
        ill.setState(tc.state);
        ill.repaint();
    }

    public void repaint() {
        model.fireTableDataChanged();
        table.repaint();
        f.repaint();
    }

    public void init() {
        rows.clear();
        ctr = 0;
        repaint();
    }

    public void addMove(State state) {
        if(!state.turn) {
            Vector row = new Vector();
            row.add(new TableCell(""+(rows.size()+1), null));
            row.add(new TableCell(state.lastMove.toString(), state));
            row.add(new TableCell("", null));
            rows.add(row);
        }
        else {
            if(rows.size() == 0) {
                Vector row = new Vector();
                row.add(new TableCell(""+(rows.size()+1), null));
                row.add(new TableCell("", null));
                row.add(new TableCell(state.lastMove.toString(), state));
                rows.add(row);
            }
            else {
                Vector rom = (Vector) rows.get(rows.size()-1);
                rom.remove(2);
                rom.add(new TableCell(state.lastMove.toString(), state));
            }
        }
        if(ill != null) {
            ill.repaint();
        }
        repaint();
    }

    public static class ButtonRenderer extends JButton implements TableCellRenderer {

        public ButtonRenderer() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table,
                                                       Object value, // -> TableCell
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            TableCell tc = (TableCell) value;
            tc.setRow(row);
            tc.setColumn(column);
            setText(value.toString());
            //addActionListener(e -> System.out.println("ButtonRenderer, action listener"));
            //System.out.println("value klass "+value.getClass().getName()); // -> TableCell
            return this;
        }

    }

    TableButton button = new TableButton();

    public class ButtonEditor extends DefaultCellEditor {

        private String label;

        public ButtonEditor(JCheckBox checkBox) {
            super(checkBox);
        }
        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            label = value.toString();
            button.setText(label);
            button.setRow(row);
            button.setColumn(column);
            //button.addActionListener(e -> System.out.println("Klick "+label));
            return button;
        }

        public Object getCellEditorValue() {
            TableCell tc = (TableCell) ((Vector) rows.get(button.getRow())).get(button.getColumn());
            return new TableCell(label, tc.state); // fixa
        }

    }

    public class TableButton extends JButton {
        public TableButton() {
        }
        private int row;

        public void setRow(int row) {
            this.row = row;
        }

        public int getRow() {
            return row;
        }

        private int column;

        public void setColumn(int column) {
            this.column = column;
        }

        public int getColumn() {
            return column;
        }
    }

    public class TableCell {

        private String appearance = null;
        private State state = null;

        public TableCell(String appearance, State state) {
            this.appearance = appearance;
            this.state = state;
        }

        public String toString() {
            return appearance;
        }

        public String toStringMore() {
            return appearance+", row="+row+", column="+column;
        }

        private int row;

        public void setRow(int row) {
            this.row = row;
        }

        public int getRow() {
            return row;
        }

        private int column;

        public void setColumn(int column) {
            this.column = column;
        }

        public int getColumn() {
            return column;
        }

    }

}
