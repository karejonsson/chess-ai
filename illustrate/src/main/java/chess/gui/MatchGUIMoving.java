package chess.gui;

import chess.board.*;
import chess.operations.Getters;
import chess.operations.Moving;
import chess.rules.Check;
import chess.gui.paint.Painter;
import chess.board.pieces.Piece;
import chess.rules.Moves;

import java.awt.*;
import java.awt.event.*;

public class MatchGUIMoving extends Canvas implements MouseListener, MouseMotionListener {

    private int delx, dely;
    private MatchNotifying board;

    private Painter painter = null;

    private Square from;
    private Square to;
    private boolean dragging;

    private boolean isPromoting;
    private Move tempMove;

    public MatchGUIMoving(MatchNotifying board, Painter painter) {
        this(board, 1.0, painter);
    }

    public MatchGUIMoving(MatchNotifying board, double magn, Painter painter) {
        this.board = board;
        delx = (int)(Painter.STDDELX*magn);
        dely = (int)(Painter.STDDELY*magn);
        this.painter = painter;
        from = new Square();
        to = new Square();
        addMouseMotionListener(this);
        addMouseListener(this);
        dragging = false;
        setSize(Board.files * delx, Board.ranks * dely);
    }

    public void addKeyListener(Component comp) {
        KeyListener promotor = new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                char c = Character.toUpperCase(e.getKeyChar());
                MatchGUIMoving.this.board.surveillor.onGame("typed: " + c);
                if (c == 'Q' || c == 'N' || c == 'R' || c == 'B') {
                    promotionHandler(c);
                }
            }

            @Override
            public void keyPressed(KeyEvent e) { }

            @Override
            public void keyReleased(KeyEvent e) { }
        };
        comp.addKeyListener(promotor);
    }

    public void promotionHandler(char c) {
        if (isPromoting) {
            board.surveillor.onGame("");
            isPromoting = false;

            tempMove.promoteTo = c;
            tempMove.promotion = true;

            Moving.real(board, tempMove);
            board.readyNextMove();
            if(board.continues()) {
                board.surveillor.onMove(board.state.lastMove);
            }
        }
    }

    public void paint(Graphics g) {
        if(!painter.hasImageBuffer()) {
            painter.setImageBuffer(createImage(Board.files * delx + 1, Board.ranks * dely + 1));
        }
        paint(painter);
        painter.draw(g, this);
    }

    public void paint(Painter d) {

        // Draw the board
        for(byte rank = 0 ; rank < Board.ranks ; rank++) {
            for(byte file = Board.files-1 ; file >= 0 ; file--) {
                /* Color in the dark squares */
                d.fillRect(rank, file);
            }
        }

        /* Draw the pieces */
        for(byte rank = 0 ; rank < Board.ranks ; rank++) {
            for(byte file = Board.files-1 ; file >= 0; file --) {
                Piece piece = Getters.getPiece(board.state, rank, file);
                if (piece != null) {
                    // If the square is occupied, draw the piece
                    d.drawFigure(piece, rank, file, dragging, from);
                }
            }
        }
    }

    // Override the update method so that the board is not erased
    public void update(Graphics g) {
        paint(g);
    }

    public void mouseReleased(MouseEvent e) {
        //System.out.println("dragging="+dragging);
        if(dragging) {
            to = painter.getPoint(e.getX(), e.getY());
            Move m = new Move(from, to);
            moveHandler(m);
            dragging = false;
        }
        repaint();
    }

    public void mouseDragged(MouseEvent e) {}

    public void mousePressed(MouseEvent e) {
        Square p = painter.getPoint(e.getX(), e.getY());
        //System.out.println("1: "+Getters.isOccupied(board.state, p));
        //System.out.println("2: "+(Getters.getPiece(board.state, p).getColor() == board.state.turn));

        if (Getters.isOccupied(board.state, p) &&
                Getters.getPiece(board.state, p).getColor() == board.state.turn) {
            from = p;
            dragging = true;
            repaint();
        }
    }

    public void mouseExited(MouseEvent e) {dragging = false;}
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseMoved(MouseEvent e) {}

    public void moveHandler(Move m) {
        // System.out.println("MatchGUIMoving.moveHandler. isPromoting="+isPromoting+", m="+m);
        if(isPromoting) {
            return;
        }
        // make a copy of the current board
        if (Moves.isLegal(board.state, m, Getters.getLastMove(board.state)) && Check.isCheckLegal(board.state, m, Getters.getLastMove(board.state))) {
            // If a promotion is involved, the program needs
            // to know what the promotion is.
            // The program then exits, and picks up at promotionHandler
            if (m.promotion) {
                tempMove = new Move(m.from, m.to);
                board.surveillor.onGame("Enter 'Q','R','B', or 'N'");
                isPromoting = true;
                return;
            }

            // All other moves
            else {
                Moving.real(board, m);
                board.readyNextMove();
                return;
            }
        }
        else {
            board.surveillor.onGame(m.getMessage());
            return;
        }
    }

}