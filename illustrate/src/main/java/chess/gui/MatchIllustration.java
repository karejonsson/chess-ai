package chess.gui;

import chess.board.Board;
import chess.board.State;
import chess.operations.Getters;
import chess.board.Match;
import chess.gui.paint.Painter;
import chess.gui.paint.PainterForWhiteDown;
import chess.board.pieces.Piece;

import java.awt.*;

public class MatchIllustration extends Canvas {

    private final int delx;
    private final int dely;
    private State state;
    private Match match;
    private double magn;

    private Painter d = null;

    public MatchIllustration(Match match) {
        this(match, 1.0);
    }

    public MatchIllustration(Match match, double magn) {
        this.magn = magn;
        delx = (int)(Painter.STDDELX*magn);
        dely = (int)(Painter.STDDELY*magn);
        setMatch(match);
        setSize(Board.files * delx, Board.ranks * dely);
    }

    public MatchIllustration(State state, double magn) {
        this.magn = magn;
        delx = (int)(Painter.STDDELX*magn);
        dely = (int)(Painter.STDDELY*magn);
        setState(state);
        setSize(Board.files * delx, Board.ranks * dely);
    }

    public void paint(Graphics g) {
        if (d == null) {
            d = new PainterForWhiteDown(magn);
            d.setImageBuffer(createImage(Board.files * delx + 1, Board.ranks * dely + 1));
        }
        paint(d);
        d.draw(g, this);
    }

    public void setState(State s) {
        if(s != null) {
            match = null;
        }
        state = s;
    }

    public void setMatch(Match m) {
        if(m != null) {
            state = null;
        }
        match = m;
    }

    public void paint(Painter d) {
        // Draw the board
        for (byte rank = 0; rank < Board.ranks; rank++) {
            for (byte file = Board.files-1; file >= 0; file--) {
                /* Color in the dark squares */
                d.fillRect(rank, file);
            }
        }

        State toDraw = match == null ? null : match.state;
        if(toDraw == null) {
            toDraw = state;
        }

        // Draw the pieces
        if(toDraw != null) {
            for (byte rank = 0; rank < Board.ranks; rank++) {
                for (byte file = Board.files-1; file >= 0; file--) {
                    // If the square is occupied, draw the piece
                    Piece piece = Getters.getPiece(toDraw, rank, file);
                    if (piece != null) {
                        d.drawFigure(piece, rank, file);
                    }
                }
            }
        }
    }

    /* Override the update method so that the board is not erased */
    public void update(Graphics g) {
        paint(g);
    }

    public State getState() {
        if(match != null) {
            return match.state;
        }
        return state;
    }

}