package chess.gui;

import java.awt.*;

public class PromotionFrame extends Frame {

    public PromotionFrame(String s) {
        super(s);
    }

    public void paint(Graphics g) {
        g.setColor(Color.black);
        g.drawString("Type 'Q', 'R', 'B', 'N'", 25, 45);
    }

}
