package chess.functions.staticEvaluation;

import chess.board.Board;
import chess.board.Wrapper;

import java.util.List;

public class PieceEvalPawn {

    public static int evaluatePawnPromotionalStatus(byte[] pawnCount, byte[] pawnRelativeAdvancement, byte relativeRank, byte relativeFile, Weights aw) {
        int score = 0;

        pawnRelativeAdvancement[relativeFile] = (byte) Math.max(pawnRelativeAdvancement[relativeFile], relativeRank);
        pawnCount[relativeFile]++;

        if (pawnCount[relativeFile] > 1) {
            //Doubled Pawn
            score -= aw.penaltyDoublePawn;
        }
        return score;
    }

    public static int pawnStateReward(PrecalcPiece ePiece, byte[] pawnCount, byte[] pawnAdvancement, Weights aw) {
        int score = PieceEvalGeneral.rewardPiece(ePiece, aw);
        score += evaluatePawnPromotionalStatus(pawnCount, pawnAdvancement, ePiece.relativeRank, ePiece.relativeFile, aw);
        return score;
    }

    public static int pawnStateReward(PrecalcPiece ePiece, byte[] pawnCount, byte[] pawnAdvancement, Weights aw, Wrapper<String> trace) {
        int score = PieceEvalGeneral.rewardPiece(ePiece, aw, trace);
        String std = trace.value;
        int contrib = evaluatePawnPromotionalStatus(pawnCount, pawnAdvancement, ePiece.relativeRank, ePiece.relativeFile, aw);
        trace.value = std+", promovering "+contrib;
        return score+contrib;
    }

    public static int rewardPawnStructure(byte[] whitePawnCount, byte[] blackPawnCount, byte[] whitePawnAdvancement, byte[] blackPawnAdvancement, Weights aw) {
        short black_passedPawns = passedPawnsReward(blackPawnAdvancement, whitePawnAdvancement, whitePawnCount, aw);
        //System.out.println("black_passedPawns="+black_passedPawns);
        int black_isolatedPawns = isolatedPawnsPenalty(blackPawnCount, aw);
        //System.out.println("black_isolatedPawns="+black_isolatedPawns);
        int blackPawnPositional = black_passedPawns - black_isolatedPawns;
        //System.out.println("blackPawnPositional="+blackPawnPositional);

        short white_passedPawns = passedPawnsReward(whitePawnAdvancement, blackPawnAdvancement, blackPawnCount, aw);
        //System.out.println("white_passedPawns="+white_passedPawns);
        int white_isolatedPawns = isolatedPawnsPenalty(whitePawnCount, aw);
        //System.out.println("white_isolatedPawns="+white_isolatedPawns);
        int whitePawnPositional = white_passedPawns - white_isolatedPawns;
        //System.out.println("whitePawnPositional="+whitePawnPositional);

        //System.out.println("Tillägg: "+(whitePawnPositional - blackPawnPositional));

        return whitePawnPositional - blackPawnPositional;
    }

    public static int rewardPawnStructure(byte[] whitePawnCount, byte[] blackPawnCount, byte[] whitePawnAdvancement, byte[] blackPawnAdvancement, Weights aw, List<String> trace) {
        short black_passedPawns = passedPawnsReward(blackPawnAdvancement, whitePawnAdvancement, whitePawnCount, aw);
        int black_isolatedPawns = isolatedPawnsPenalty(blackPawnCount, aw);
        int blackPawnPositional = black_passedPawns - black_isolatedPawns;
        trace.add("bondestruktur -"+blackPawnPositional+": svart. passerade "+black_passedPawns+" - isolerade "+black_isolatedPawns);

        short white_passedPawns = passedPawnsReward(whitePawnAdvancement, blackPawnAdvancement, blackPawnCount, aw);
        int white_isolatedPawns = isolatedPawnsPenalty(whitePawnCount, aw);
        int whitePawnPositional = white_passedPawns - white_isolatedPawns;
        trace.add("bondestruktur +"+whitePawnPositional+": vit. passerade "+white_passedPawns+" - isolerade "+white_isolatedPawns);

        int out = whitePawnPositional - blackPawnPositional;
        trace.add("Bondestruktur: +"+out+" relativt vit");
        return out;
    }

    public static int isolatedPawnsPenalty(byte[] own, Weights aw) {
        int score = 0;

        if (own[0] >= 1 && own[1] == 0) {
            score += aw.penaltyIsolatedPawn[0];
        }
        for(byte idx = 1; idx < Board.ranks-2 ; idx++) {
            if (own[idx] >= 1 && own[idx-1] == 0 && own[idx+1] == 0) {
                score += aw.penaltyIsolatedPawn[1];
            }
        }
        if (own[Board.ranks-1] >= 1 && own[Board.ranks-2] == 0) {
            score += aw.penaltyIsolatedPawn[0];
        }

        return score;
    }

    public static boolean nonSidePawnIsPassed(byte[] ownPawnAdvancement, byte[] otherPawnAdvancement, byte[] otherPawnCount, byte idx) {
        byte otherIdx = (byte)(Board.ranks-1-idx);
        byte othersForemost = 0;
        if(otherPawnCount[otherIdx] != 0) {
            othersForemost = (byte) Math.max(othersForemost, (byte) (Board.ranks-1-otherPawnAdvancement[otherIdx]));
        }
        if(otherPawnCount[otherIdx-1] != 0) {
            othersForemost = (byte) Math.max(othersForemost, (byte) (Board.ranks-1-otherPawnAdvancement[otherIdx-1]));
        }
        if(otherPawnCount[otherIdx+1] != 0) {
            othersForemost = (byte) Math.max(othersForemost, (byte) (Board.ranks-1-otherPawnAdvancement[otherIdx+1]));
        }
        if(othersForemost == 0) {
            return true;
        }
        return ownPawnAdvancement[idx] >= othersForemost;
    }

    public static boolean sidePawnIsPassed(byte[] ownPawnAdvancement, byte[] otherPawnAdvancement, byte[] otherPawnCount, byte idx, byte idxSideFile) {
        byte otherIdx = (byte)(Board.ranks-1-idx);
        byte otherIdxSideFile = (byte)(Board.ranks-1-idxSideFile);
        byte othersForemost = 0;
        if(otherPawnCount[otherIdx] != 0) {
            othersForemost = (byte) Math.max(othersForemost, (byte) (Board.ranks-1-otherPawnAdvancement[otherIdx]));
        }
        if(otherPawnCount[otherIdxSideFile] != 0) {
            othersForemost = (byte) Math.max(othersForemost, (byte) (Board.ranks-1-otherPawnAdvancement[otherIdxSideFile]));
        }
        if(othersForemost == 0) {
            return true;
        }
        return ownPawnAdvancement[idx] >= othersForemost;
    }

    public static short passedPawnsReward(byte[] ownPawnAdvancement, byte[] otherPawnAdvancement, byte[] otherPawnCount, Weights aw) {
        short score = 0;

        if(sidePawnIsPassed(ownPawnAdvancement, otherPawnAdvancement, otherPawnCount, (byte)0x0, (byte)0x1)) {
            score += aw.rewardPassedPawn[0];
        }
        for(byte idx = 1 ; idx <= Board.ranks-2 ; idx++) {
            if(nonSidePawnIsPassed(ownPawnAdvancement, otherPawnAdvancement, otherPawnCount, idx)) {
                score += aw.rewardPassedPawn[idx];
            }
        }
        if(sidePawnIsPassed(ownPawnAdvancement, otherPawnAdvancement, otherPawnCount, (byte)(Board.ranks-1), (byte)(Board.ranks-2))) {
            score += aw.rewardPassedPawn[0];
        }
        return score;
    }

}
