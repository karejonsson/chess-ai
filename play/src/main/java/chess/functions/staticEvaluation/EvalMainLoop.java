package chess.functions.staticEvaluation;

import chess.board.Board;
import chess.board.Square;
import chess.board.Wrapper;
import chess.functions.Measures;
import chess.operations.Serialization;

import java.util.List;

public class EvalMainLoop {

    public static void evalScore(PrecalcBoard board, Weights aw) {
        board.Score = 0;

        byte[] whitePawnCount = new byte[Board.files];
        byte[] blackPawnCount = new byte[Board.files];

        byte[] whitePawnAdvancement = new byte[Board.files];
        byte[] blackPawnAdvancement = new byte[Board.files];

        if (board.state.getLegalMoves().size() == 0 && !board.state.check) {
            return;
        }
        if (board.repeatedMove >= 3) {
            return;
        }

        // agility
        board.Score += AgilityEval.betterForBoth(board.whiteMate, board.whiteInCheck, board.endGamePhase, board.whiteCastled, aw);
        board.Score -= AgilityEval.betterForBoth(board.blackMate, board.blackInCheck, board.endGamePhase, board.blackCastled, aw);

        for(PrecalcPiece ePiece : board.lookup.values()) {
            //int scoreInnan = board.Score;
            if (ePiece.pieceColor == Board.WHITE) {
                board.Score += PieceEvalMixed.betterForBoth(ePiece, whitePawnCount, whitePawnAdvancement, aw);
            }
            else {
                board.Score -= PieceEvalMixed.betterForBoth(ePiece, blackPawnCount, blackPawnAdvancement, aw);
            }
        }

        board.Score += PieceEvalPawn.rewardPawnStructure(whitePawnCount, blackPawnCount, whitePawnAdvancement, blackPawnAdvancement, aw);

        boolean whiteSufficientForMate = Measures.possiblyEnoughForMate(board.state.wPieces);
        boolean blackSufficientForMate = Measures.possiblyEnoughForMate(board.state.bPieces);

        if(!whiteSufficientForMate) {
            board.Score = (short) Math.min(0, board.Score);
        }
        if(!blackSufficientForMate) {
            board.Score = (short) Math.max(0, board.Score);
        }
        if(!whiteSufficientForMate && !blackSufficientForMate) {
            board.staleMate = true;
            board.insufficientMaterial = true;
        }
    }

    public static void evalScore(PrecalcBoard board, Weights aw, List<String> trace) {
        trace.add("Slutspel "+board.endGamePhase);
        trace.add("Ställning\n"+ Serialization.getBoardAsStringWithLinebreaksAndTheWholeMidevitt(board.state));
        board.Score = 0;
        int contrib = 0;

        byte[] whitePawnCount = new byte[Board.files];
        byte[] blackPawnCount = new byte[Board.files];

        byte[] whitePawnAdvancement = new byte[Board.files];
        byte[] blackPawnAdvancement = new byte[Board.files];

        if (board.state.getLegalMoves().size() == 0 && !board.state.check) {
            return;
        }
        if (board.repeatedMove >= 3) {
            return;
        }

        // agility
        contrib = AgilityEval.betterForBoth(board.whiteMate, board.whiteInCheck, board.endGamePhase, board.whiteCastled, aw);
        trace.add("+"+contrib+": Vits rörlighet");
        board.Score += contrib;
        contrib = AgilityEval.betterForBoth(board.blackMate, board.blackInCheck, board.endGamePhase, board.blackCastled, aw);
        trace.add("-"+contrib+": Svarts rörlighet");
        board.Score -= contrib;

        Wrapper<String> msg = new Wrapper<>(null);

        for(PrecalcPiece ePiece : board.lookup.values()) {
            Square position = ePiece.square;
            msg.value = null;

            //int scoreInnan = board.Score;
            if (ePiece.pieceColor == Board.WHITE) {
                contrib = PieceEvalMixed.betterForBoth(ePiece, whitePawnCount, whitePawnAdvancement, aw, msg);
                trace.add(""+ePiece.square+": +"+contrib+": Pjäs "+ePiece.piece.toString()+" -> "+msg.value);
                board.Score += contrib;
            }
            else {
                contrib = PieceEvalMixed.betterForBoth(ePiece, blackPawnCount, blackPawnAdvancement, aw, msg);
                trace.add(""+ePiece.square+": -"+contrib+": Pjäs "+ePiece.piece.toString()+" -> "+msg.value);
                board.Score -= contrib;
            }
        }

        contrib = PieceEvalPawn.rewardPawnStructure(whitePawnCount, blackPawnCount, whitePawnAdvancement, blackPawnAdvancement, aw, trace);
        board.Score += contrib;

        boolean whiteSufficientForMate = Measures.possiblyEnoughForMate(board.state.wPieces);
        boolean blackSufficientForMate = Measures.possiblyEnoughForMate(board.state.bPieces);
        trace.add("Vit kan matta "+whiteSufficientForMate+", svart kan matta "+blackSufficientForMate);

        if(!whiteSufficientForMate) {
            board.Score = (short) Math.min(0, board.Score);
        }
        if(!blackSufficientForMate) {
            board.Score = (short) Math.max(0, board.Score);
        }
        if(!whiteSufficientForMate && !blackSufficientForMate) {
            board.staleMate = true;
            board.insufficientMaterial = true;
        }

        trace.add(""+board.Score+": Slutsiffra");
    }

}
