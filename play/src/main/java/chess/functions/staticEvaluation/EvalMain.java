package chess.functions.staticEvaluation;

// https://raw.githubusercontent.com/3583Bytes/ChessCore/master/ChessCoreEngine/Evaluation.cs

import chess.board.Board;
import chess.board.State;
import chess.functions.staticEvaluation.positional.DownloadedPositionRewarder;
import chess.functions.staticEvaluation.positional.FormulaPositionRewarder;
import chess.operations.Initiation;

import java.util.List;

public class EvalMain {

    public static int evaluateBoardScore(State state) {
        Weights out = Weights.aws[Board.chessStyle].copy();
        FormulaPositionRewarder fpw = new FormulaPositionRewarder(
                new short[] { 100, 11, 11, 6, 5, 13 });
        out.positionRewarder = fpw;
        return evaluateBoardScore(state, out);
    }

    public static int evaluateBoardScore(State state, Weights aw) {
        PrecalcBoard eb = PrecalcBoard.create(state);//, aw);
        PrecalcBoard.init(eb, aw);
        //AmbitiousWeights aw = new AmbitiousWeights();
        //System.out.println("EvalBoard="+eb);
        EvalMainLoop.evalScore(eb, aw);
        return eb.Score;
    }

    public static int evaluateBoardScore(State state, List<String> trace) {
        Weights out = Weights.aws[Board.chessStyle].copy();
        FormulaPositionRewarder fpw = new FormulaPositionRewarder(
                new short[] { 100, 11, 11, 6, 5, 13 });
        out.positionRewarder = fpw;
        return evaluateBoardScore(state, out, trace);
    }

    public static int evaluateBoardScore(State state, Weights aw, List<String> trace) {
        PrecalcBoard eb = PrecalcBoard.create(state);//, aw);
        PrecalcBoard.init(eb, aw, trace);
        //AmbitiousWeights aw = new AmbitiousWeights();
        //System.out.println("EvalBoard="+eb);
        EvalMainLoop.evalScore(eb, aw, trace);
        return eb.Score;
    }

    public static void test(int positionStraightWhite, boolean color) {
        byte relativeRank = (byte) (positionStraightWhite / Board.files);
        byte relativeFile = (byte) (positionStraightWhite % Board.ranks);
        byte tableIndex = (byte) (Board.ranks*(Board.ranks-1-relativeRank)+relativeFile);

        if(color == Board.BLACK) {
            relativeRank = (byte) (Board.ranks-1 - relativeRank);
            relativeFile = (byte) (Board.files-1 - relativeFile);
        }

        System.out.println("straight="+positionStraightWhite+", color="+color+", relativeRank="+relativeRank+", relativeFile="+relativeFile+", tableIndex="+tableIndex);
    }

    /*
    public static void main(String[] args) {
        test(0, true);
        test(1, true);
        test(7, true);
        test(8, true);
        test(9, true);
        test(0, false);
        test(1, false);
        test(7, false);
        test(8, false);
        test(9, false);
    }
    */

    public static void main(String[] args) {
        State state = Initiation.createStandardInitialState();
        System.out.println("Gradering "+evaluateBoardScore(state));
    }

}
