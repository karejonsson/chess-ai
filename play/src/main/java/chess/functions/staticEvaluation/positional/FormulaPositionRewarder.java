package chess.functions.staticEvaluation.positional;

import chess.board.Board;
import chess.board.Square;
import chess.board.pieces.*;

import chess.functions.staticEvaluation.Cloning;
import chess.functions.staticEvaluation.Weights;

public class FormulaPositionRewarder implements PositionRewarder {

    private short[] amplitudes;
    private double[][][][] posValues;

    public FormulaPositionRewarder(short[] amplitudes) {
        this.amplitudes = amplitudes;
        posValues = new double[][][][] {
                getPawnTables(),
                getKnightTables(),
                getBishopTables(),
                getRookTables(),
                getQueenTables(),
                getKingTables(),
                getArchbishopTables(),
                getChancellorTables()
        };
    }

    @Override
    public short getReward(byte idx, boolean color, byte nrPiecesOnBoard, Square square) {
        byte relativeRank = (byte)(color ? square.rank : Board.ranks-1-square.rank);
        byte relativeFile = (byte)(color ? square.file : Board.files-1-square.file);
        return (short)(amplitudes[idx]*posValues[idx][4*Board.files-nrPiecesOnBoard][relativeRank][relativeFile]);
    }

    private double getAttenuation(byte remainingPieces) {
        byte removedPieces = (byte)(4*Board.files - remainingPieces);
        double exp = 0.27*(52-removedPieces) -10;
        return 1.0-1.0/(1+Math.exp(exp));
    }

    public static double pawnDist(double r) {
        if(r == 0 || r == 7) {
            return 0;
        }
        return (r-1)/(Board.ranks-3);//(Math.pow((r-3.7)/2.7, 3)+1)*0.62;
    }

    private double[][] getPawnTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = ((double)((Board.files-1)/2.0-f));
                out[r][f] = pawnDist(r)*(1-(Math.pow(df, 2)/Math.pow(r+2, 2)));
            }
        }
        return out;
    }

    private double[][][] getPawnTables() {
        double[][][] out = new double[Board.files*4][][];
        double[][] table = getPawnTable();
        for(int i = 0 ; i < Board.files*4 ; i++) {
            out[i] = table;
        }
        return out;
    }

    public static double knightDist(double dr, double df) {
        return Math.pow(dr, 2)+Math.pow(df, 2);
    }

    public static double knightOut(double dist) {
        return 0.38*(3.64-Math.sqrt(0.5*dist+1.0));
    }

    private double[][] getKnightTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            double dr = r-(double)((Board.ranks-1)/2.0);
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = f-(double)((Board.files-1)/2.0);
                out[r][f] = knightOut(knightDist(dr, df));
            }
        }
        return out;
    }

    private double[][][] getKnightTables() {
        double[][][] out = new double[Board.files*4][][];
        double[][] table = getKnightTable();
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            out[i] = Cloning.mult(table, getAttenuation((byte)(4*Board.files-i)));
        }
        return out;
    }

    public static double bishopDist(double dr, double df) {
        return Math.pow((df-dr)*(df+dr), 2);
    }

    public static double bishopOut(double cross) {
        return 2.0-Math.sqrt(1.0+cross/64);
    }

    private double[][] getBishopTable(double backwards) {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            double dr = r-(double)((Board.ranks-1)/2.0)-backwards;
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = f-(double)((Board.files-1)/2.0);
                out[r][f] = bishopOut(bishopDist(dr, df));
            }
        }
        return out;
    }

    private double[][][] getBishopTables() {
        double[][][] out = new double[Board.files*4][][];
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            double backwards = ((double)(Board.files*4-i))/(Board.files*4);
            out[i] = Cloning.mult(getBishopTable(backwards), getAttenuation((byte)(4*Board.files-i)));
        }
        return out;
    }

    public static double rookDist(double dr, double df) {
        return Math.pow(df*dr, 2)*0.3;
    }

    public static double rookOut(double cross) {
        return 2.0-Math.sqrt(1.0+cross/128.0);
    }

    private double[][] getRookTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            double dr = r-(double)((Board.ranks-1)/2.0);
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = f-(double)((Board.files-1)/2.0);
                out[r][f] = rookOut(rookDist(dr, df));
            }
        }
        return out;
    }

    private double[][][] getRookTables() {
        double[][][] out = new double[Board.files*4][][];
        double[][] table = getRookTable();
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            out[i] = Cloning.mult(table, getAttenuation((byte)(4*Board.files-i)));
        }
        return out;
    }

    private double[][] getQueenTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            double dr = r-(double)((Board.ranks-1)/2.0);
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = f-(double)((Board.files-1)/2.0);
                out[r][f] = (rookOut(rookDist(dr, df))+bishopOut(bishopDist(dr, df)))/2.0;
            }
        }
        return out;
    }

    private double[][][] getQueenTables() {
        double[][][] out = new double[Board.files*4][][];
        double[][] table = getQueenTable();
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            out[i] = Cloning.mult(table, getAttenuation((byte)(4*Board.files-i)));
        }
        return out;
    }

    public static double kingOut(double f, double r) {
        double denom = (Math.exp(1.5*(f-4))+Math.exp(-1.5*(f-4)));;
        return Math.pow(1.0-2.0/denom, 2)*Math.pow((((double)(Board.ranks-r))/Board.ranks), 12);
    }

    private double[][] getKingTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            for(byte f = 0 ; f < Board.files ; f++) {
                out[r][f] = kingOut(f, r);
            }
        }
        return out;
    }

    private double[][][] getKingTables() {
        double[][][] out = new double[Board.files*4][][];
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            byte remainingPieces = (byte)(4*Board.files-i);
            double att = getAttenuation(remainingPieces);
            double shiftKing = ((double)(Board.files*4-i))/(Board.files*4);
            double shiftKnight = 1.0-shiftKing;
            out[i] = new double[Board.ranks][];
            for(byte r = 0 ; r < Board.ranks ; r++) {
                out[i][r] = new double[Board.files];
                double dr = r-(double)((Board.ranks-1)/2.0);
                for(byte f = 0 ; f < Board.files ; f++) {
                    double df = f-(double)((Board.files-1)/2.0);
                    out[i][r][f] = att*(shiftKing*kingOut(f, r)+shiftKnight*knightOut(knightDist(dr, df)));
                }
            }
        }
        return out;
    }

    private double[][] getArchbishopTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            double dr = r-(double)((Board.ranks-1)/2.0);
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = f-(double)((Board.files-1)/2.0);
                out[r][f] = (knightOut(knightDist(dr, df))+bishopOut(bishopDist(dr, df)))/2.0;
            }
        }
        return out;
    }

    private double[][][] getArchbishopTables() {
        double[][][] out = new double[Board.files*4][][];
        double[][] table = getArchbishopTable();
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            out[i] = Cloning.mult(table, getAttenuation((byte)(4*Board.files-i)));
        }
        return out;
    }

    private double[][] getChancellorTable() {
        double[][] out = new double[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks ; r++) {
            out[r] = new double[Board.files];
            double dr = r-(double)((Board.ranks-1)/2.0);
            for(byte f = 0 ; f < Board.files ; f++) {
                double df = f-(double)((Board.files-1)/2.0);
                out[r][f] = (rookOut(rookDist(dr, df))+knightOut(knightDist(dr, df)))/2.0;
            }
        }
        return out;
    }

    private double[][][] getChancellorTables() {
        double[][][] out = new double[Board.files*4][][];
        double[][] table = getChancellorTable();
        for(byte i = 0 ; i < Board.files*4 ; i++) {
            out[i] = Cloning.mult(table, getAttenuation((byte)(4*Board.files-i)));
        }
        return out;
    }

    public static void _main(String[] args) {
        FormulaPositionRewarder fpw = new FormulaPositionRewarder(new short[] { 100, 100, 100, 100, 100, 100, 100, 100 });

        for(byte r = 0 ; r < Board.ranks ; r++) {
            for (byte f = 0; f < Board.files; f++) {
                Square sqr = new Square((byte) r, (byte) f);
                //System.out.println("Springare, vit, 0, " + sqr + ") -> " + fpw.getReward(Knight.idx, true, (byte) 32, sqr));
                System.out.print(""+String.format("%02d", fpw.getReward(King.idx, true, (byte) 32, sqr))+" ");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        FormulaPositionRewarder fpw = new FormulaPositionRewarder(
                new short[] { 100, 11, 11, 6, 5, 13 });
        //double[][] table = fpw.getQueenTable();
        for(byte r = 0 ; r < Board.ranks ; r++) {
            for (byte f = 0; f < Board.files; f++) {
                System.out.print(""+String.format("%02d ", fpw.getReward(Queen.idx, Board.WHITE, (byte)32, new Square(r, f))));
            }
            System.out.println("");
        }
    }

}
