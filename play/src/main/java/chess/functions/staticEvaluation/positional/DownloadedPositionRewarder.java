package chess.functions.staticEvaluation.positional;

import chess.board.Board;
import chess.board.Square;
import chess.functions.staticEvaluation.Weights;

public class DownloadedPositionRewarder implements PositionRewarder {

    public static final short[] ZeroTable8x8 = Weights.getVector(64, (short)0);

    public static final short[] ZeroTable10x8 = Weights.getVector(80, (short)0);

    public static final short[] ZeroTable10x10 = Weights.getVector(100, (short)0);

    private short[][] posValue = null;
    private short[][] posValueEndgame = null;

    public DownloadedPositionRewarder() {
        if(Board.chessStyle == 0) {
            posValue = new short[][] {
                    TablesAsDownloaded.PawnTable8x8, TablesAsDownloaded.KnightTable8x8, TablesAsDownloaded.BishopTable8x8, ZeroTable8x8, ZeroTable8x8, TablesAsDownloaded.KingTable8x8
            };
            posValueEndgame = new short[][] {
                    ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, TablesAsDownloaded.KingTableEndGame8x8
            };
        }
        if(Board.chessStyle == 0) {
            posValue = new short[][] {
                    TablesAsDownloaded.PawnTable10x8, TablesAsDownloaded.KnightTable10x8, TablesAsDownloaded.BishopTable10x8, ZeroTable10x8, ZeroTable10x8, TablesAsDownloaded.KingTable10x8, ZeroTable10x8, ZeroTable10x8
            };
            posValueEndgame = new short[][] {
                    ZeroTable10x8, ZeroTable10x8, ZeroTable10x8, ZeroTable10x8, ZeroTable10x8, TablesAsDownloaded.KingTableEndGame10x8, ZeroTable10x8, ZeroTable10x8
            };
        }
        if(Board.chessStyle == 0) {
            posValue = new short[][]{
                    TablesAsDownloaded.PawnTable10x10, TablesAsDownloaded.KnightTable10x10, TablesAsDownloaded.BishopTable10x10, ZeroTable10x10, ZeroTable10x10, TablesAsDownloaded.KingTable10x10, ZeroTable10x10, ZeroTable10x10
            };
            posValueEndgame = new short[][]{
                    ZeroTable10x10, ZeroTable10x10, ZeroTable10x10, ZeroTable10x10, ZeroTable10x10, TablesAsDownloaded.KingTableEndGame10x10, ZeroTable10x10, ZeroTable10x10
            };
        }
    }

    @Override
    public short getReward(byte idx, boolean color, byte  nrPiecesOnBoard, Square square) {
        // score += (endGamePhase ? aw.posValueEndgame : aw.posValue)[idx][relativeTableIndex];
        //Square position = ePiece.square;//Getters.getSquare(positionStraightWhite);
        //Piece piece = Getters.getPiece(board.state, position);

        byte relativeRank = square.rank;//(byte) (positionStraightWhite / Board.files);
        byte relativeFile = square.file;//(byte) (positionStraightWhite % Board.files);
        byte tableIndex = (byte) (Board.ranks*(Board.ranks-1-relativeRank)+relativeFile);
        byte relativeTableIndex = tableIndex;

        if(color == Board.BLACK) {
            relativeRank = (byte) (Board.ranks-1 - relativeRank);
            relativeFile = (byte) (Board.files-1 - relativeFile);
            relativeTableIndex = (byte) (Board.ranks*(Board.ranks-1-relativeRank)+relativeFile);
        }
        boolean endGamePhase = nrPiecesOnBoard <= 9;
        return (endGamePhase ? posValueEndgame : posValue)[idx][relativeTableIndex];
    }

}
