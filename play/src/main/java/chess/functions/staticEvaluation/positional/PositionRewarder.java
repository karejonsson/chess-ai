package chess.functions.staticEvaluation.positional;

import chess.board.Square;

public interface PositionRewarder {
    short getReward(byte idx, boolean color, byte  nrPiecesOnBoard, Square square);
}
