package chess.functions.staticEvaluation;

import chess.board.Wrapper;
import chess.board.pieces.Pawn;

public class PieceEvalMixed {

    public static int betterForBoth(
            PrecalcPiece ePiece,
            byte[] pawnCount,
            byte[] pawnAdvancement,
            Weights aw) {
        int score = 0;
        byte idx = ePiece.piece.index();

        if(idx == Pawn.idx) {
            score += PieceEvalPawn.pawnStateReward(ePiece, pawnCount, pawnAdvancement, aw);
        }
        else {
            score += PieceEvalGeneral.rewardPiece(ePiece, aw);
        }
        return score;
    }

    public static int betterForBoth(
            PrecalcPiece ePiece,
            byte[] pawnCount,
            byte[] pawnAdvancement,
            Weights aw,
            Wrapper<String> trace) {
        int score = 0;
        byte idx = ePiece.piece.index();

        if (idx == Pawn.idx) {
            score += PieceEvalPawn.pawnStateReward(ePiece, pawnCount, pawnAdvancement, aw, trace);
        }
        else {
            score += PieceEvalGeneral.rewardPiece(ePiece, aw, trace);
        }
        return score;
    }

}
