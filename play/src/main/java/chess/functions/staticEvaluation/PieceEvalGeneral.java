package chess.functions.staticEvaluation;

import chess.board.Wrapper;

public class PieceEvalGeneral {

    public static short rewardPiece(PrecalcPiece ePiece, Weights aw) {
        short score = 0;
        byte idx = ePiece.piece.index();

        //score += aw.posValue[idx][tableIndex];
        //score += (endGamePhase ? aw.posValueEndgame : aw.posValue)[idx][relativeTableIndex];
        score += aw.positionRewarder.getReward(idx, ePiece.piece.getColor(), ePiece.board.nrPiecesOnBoard, ePiece.square);
        score += ePiece.attackingValue;
        score += ePiece.defendingValue;
        score += aw.moveability[idx][ePiece.nrValidMoves];//ePiece.nrValidMoves;// * aw.mobilityKoefficient[idx]);
        if(ePiece.strikesOnBetter > 1) {
            score += aw.multipleStrike*(ePiece.highestAttacking.pieceMaterialValue - ePiece.pieceMaterialValue);
        }


        if(ePiece.lowestDefending == null) {
            // Ingen försvarar
            if(ePiece.lowestAttacking == null) {
                // Ingen försvarar, ingen attackerar
                // -> Inget
            }
            else {
                // Ingen försvarar, någon attackerar
                if(ePiece.state.turn == ePiece.pieceColor) {
                    // -> Ingen försvarar, någon attackerar men det finns ett drag att rädda den
                    score -= (int)(ePiece.pieceMaterialValue*aw.mayNotBeSaved);
                }
                else {
                    // -> Ingen försvarar, någon attackerar och det är den andra att dra
                    score -= (int)(ePiece.pieceMaterialValue*aw.mayVeryWellBe);
                }
            }
        }
        else {
            // Någon försvarar
            if(ePiece.lowestAttacking == null) {
                // Någon försvarar, ingen attackerar
                // -> Lite stabilitet
                score += ePiece.pieceMaterialValue*aw.someStability;
            }
            else {
                // Någon försvarar, någon attackerar
                if(ePiece.pieceMaterialValue > ePiece.lowestAttacking.pieceMaterialValue) {
                    // Någon försvarar, någon som är billigare attackerar
                    // -> Pjäsen står i slag så det kommer minska vilka drag som är rimliga
                    score -= (int)((ePiece.pieceMaterialValue-ePiece.lowestAttacking.pieceMaterialValue)*aw.mayVeryWellBe);//(int)((ePiece.pieceMaterialValue-ePiece.lowestAttacking.pieceMaterialValue)*aw.mayVeryWellBe);
                }
                else {
                    // Någon försvarar, någon som är dyrare attackerar
                    // -> Garderingen kan bli viktigare och immobiliserar något
                    score -= ePiece.pieceMaterialValue*aw.someImmobilisation;
                }
            }
        }

        score += aw.pieceValues[ePiece.piece.index()];

        if(ePiece.board.endGamePhase) {
            score += aw.pieceValueEndGameExtra[ePiece.piece.index()];
        }

        return score;
    }

    public static short rewardPiece(PrecalcPiece ePiece,
                                    Weights aw, Wrapper<String> trace) {
        short score = 0;
        byte idx = ePiece.piece.index();
        int contrib = 0;

        StringBuffer msg = new StringBuffer();

        //contrib = (endGamePhase ? aw.posValueEndgame : aw.posValue)[idx][relativeTableIndex];
        contrib = aw.positionRewarder.getReward(idx, ePiece.piece.getColor(), ePiece.board.nrPiecesOnBoard, ePiece.square);
        //contrib = aw.posValue[idx][tableIndex];
        msg.append("Positionering "+contrib);
        score += contrib;

        contrib = ePiece.attackingValue;// * aw.attackValueKoefficient[idx]);
        msg.append(", Attack "+contrib);
        score += contrib;

        contrib = ePiece.defendingValue;// * aw.defendValueKoefficient[idx]);
        msg.append(", Försvar "+contrib);
        score += contrib;

        contrib = aw.moveability[idx][ePiece.nrValidMoves];//ePiece.nrValidMoves;// * aw.mobilityKoefficient[idx]);
        msg.append(", Rörlighet "+contrib);
        score += contrib;

        if(ePiece.strikesOnBetter > 1) {
            contrib = (int)(aw.multipleStrike*(ePiece.highestAttacking.pieceMaterialValue - ePiece.pieceMaterialValue));
            msg.append(", Gaffelläge "+contrib);
            score += contrib;
        }

        if(ePiece.lowestDefending == null) {
            // Ingen försvarar
            if(ePiece.lowestAttacking == null) {
                // Ingen försvarar, ingen attackerar
                // -> Inget
                msg.append(", ingen interaktion");
            }
            else {
                // Ingen försvarar, någon attackerar
                if(ePiece.state.turn == ePiece.pieceColor) {
                    // -> Ingen försvarar, någon attackerar men det finns ett drag att rädda den
                    contrib = (int)(ePiece.pieceMaterialValue*aw.mayNotBeSaved);
                    score -= contrib;
                    msg.append(", bortställd -"+contrib);
                }
                else {
                    // -> Ingen försvarar, någon attackerar och det är den andra att dra
                    contrib = (int)(ePiece.pieceMaterialValue*aw.mayVeryWellBe);
                    score -= contrib;
                    msg.append(", bortställd -"+contrib);
                }
            }
        }
        else {
            // Någon försvarar
            if(ePiece.lowestAttacking == null) {
                // Någon försvarar, ingen attackerar
                // -> Lite stabilitet
                contrib = (int)(ePiece.pieceMaterialValue*aw.someStability);
                score += contrib;
                msg.append(", stabilitet "+contrib);
            }
            else {
                // Någon försvarar, någon attackerar
                if(ePiece.pieceMaterialValue > ePiece.lowestAttacking.pieceMaterialValue) {
                    // Någon försvarar, någon som är billigare attackerar
                    // -> Pjäsen står i slag så det kommer minska vilka drag som är rimliga
                    contrib = (int)((ePiece.pieceMaterialValue-ePiece.lowestAttacking.pieceMaterialValue)*aw.mayVeryWellBe);//(int)((ePiece.pieceMaterialValue-ePiece.lowestAttacking.pieceMaterialValue)*aw.mayVery.WellBe);
                    score -= contrib;
                    msg.append(", dåligt avbyte -"+contrib);
                }
                else {
                    // Någon försvarar, någon som är dyrare attackerar
                    // -> Garderingen kan bli viktigare och immobiliserar något
                    contrib = (int)(ePiece.pieceMaterialValue*aw.someImmobilisation);
                    score -= contrib;
                    msg.append(", immobilisering -"+contrib);
                }
            }
        }

        contrib = aw.pieceValues[idx];
        msg.append(", Materialvärde "+contrib);
        score += contrib;

        if(ePiece.board.endGamePhase) {
            contrib = aw.pieceValueEndGameExtra[idx];
            msg.append(", Slutspelstillägg "+contrib);
            score += contrib;
        }

        trace.value = msg.toString();

        return score;
    }

}
