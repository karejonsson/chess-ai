package chess.functions.staticEvaluation;

import chess.board.Square;
import chess.board.State;
import chess.board.pieces.Piece;

import java.util.List;

public class PrecalcPiece {
    public PrecalcBoard board;
    public Piece piece;
    public State state;
    public Square square;
    public byte relativeRank;
    public byte relativeFile;

    public boolean pieceColor;
    public int pieceMaterialValue;
    public short defendingValue; //-- + bidrag från viktmatrisen defendingValue
    public short attackingValue; //-- + bidrag från viktmatrisen attackingValue
    public byte nrValidMoves;
    public boolean moved;
    public PrecalcPiece lowestAttacking;
    public PrecalcPiece highestAttacking;
    public byte noAttackRelations; //-- används ej
    public PrecalcPiece lowestDefending;
    public byte noDefendRelations; //-- används ej
    public byte strikesOnBetter;

    public String toString() {
        return "PrecalcPiece{"+
                "piece="+piece+", "+
                "square="+square+", "+
                "pieceMaterialValue="+pieceMaterialValue+", "+
                "defendedValue="+ defendingValue +", "+
                "attackedValue="+ attackingValue +", "+
                "nrValidMoves="+nrValidMoves+", "+
                "moved="+moved+", "+
                "lowestAttacker.piece="+(lowestAttacking != null ? lowestAttacking.piece : "<NULL>")+", "+
                "highestAttack.piece="+(highestAttacking != null ? highestAttacking.piece : "<NULL>")+", "+
                "noAttackRelations="+ noAttackRelations +", "+
                "lowestDefender.piece="+(lowestDefending != null ? lowestDefending.piece : "<NULL>")+", "+
                "noDefendRelations="+ noDefendRelations +
                "}";
    }

    public PrecalcPiece(State state, Piece piece, Square from, Weights aw, PrecalcBoard board) {
        this.piece = piece;
        this.state = state;
        this.square = from;
        this.board = board;
        pieceColor = piece.getColor();
        moved = piece.moves != 0;
        pieceMaterialValue = aw.pieceValues[piece.index()];

        //System.out.println("Innan piece.getValidMoves");
        //nrValidMoves = piece.getValidMoves(from, state).size(); // Beräknas effektivare i EvalBoard-konstruktorn
        //System.out.println("Efter piece.getValidMoves");
        /*
        // Beräknas effektivare i EvalBoard-konstruktorn
        List<Square> reachedOccupiedSquares = piece.getReachedOccupiedSquares(from, state);
        for(Square square : reachedOccupiedSquares) {
            Piece otherPiece = Getters.getPiece(state, square);
            if(otherPiece.getColor() != PieceColor) {
                // Reaches opponent
                int contrib = aw.attackValue[piece.index()][otherPiece.index()];
                attackedValue += contrib;
            }
            else {
                // Reaches own piece
                int contrib = aw.defendValue[piece.index()][otherPiece.index()];
                defendedValue += contrib;
            }
        }

         */
    }

    public PrecalcPiece(State state, Piece piece, Square from, Weights aw, PrecalcBoard board, List<String> trace) {
        this.piece = piece;
        this.state = state;
        this.square = from;
        this.board = board;
        pieceColor = piece.getColor();
        moved = piece.moves != 0;
        pieceMaterialValue = aw.pieceValues[piece.index()];
        //System.out.println("Innan piece.getValidMoves");
        //nrValidMoves = piece.getValidMoves(from, state).size();
        //System.out.println("Efter piece.getValidMoves");
        /*
        // Beräknas effektivare i EvalBoard-konstruktorn
        List<Square> reachedOccupiedSquares = piece.getReachedOccupiedSquares(from, state);
        for(Square square : reachedOccupiedSquares) {
            Piece otherPiece = Getters.getPiece(state, square);

            if(otherPiece.getColor() != PieceColor) {
                // Reaches opponent
                int contrib = aw.attackValue[piece.index()][otherPiece.index()];
                attackedValue += contrib;
                trace.add("+"+contrib+": Attack på "+otherPiece+" på "+Serialization.getSquareString(square));
            }
            else {
                // Reaches own piece
                int contrib = aw.defendValue[piece.index()][otherPiece.index()];
                defendedValue += contrib;
                trace.add("+"+contrib+": Försvar av "+otherPiece+" på "+Serialization.getSquareString(square));
            }
        }
         */
    }

    // Only for value as defended, attacked
    //private static final int[] values = new int[] { 100, 320, 325, 500, 975, 410 };

}
