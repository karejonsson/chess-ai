package chess.functions.staticEvaluation;

import java.util.Arrays;

public class Cloning {

    public static <T> T[] clone(T[] array) {
        return Arrays.copyOf(array, array.length);
    }

    public static short[] clone(short[] array) {
        return Arrays.copyOf(array, array.length);
    }

    public static short[] mult(final short[] input, double fact) {
        short[] out = clone(input);
        for(short i = 0 ; i < input.length ; i++) {
            out[i] *= fact;
        }
        return out;
    }

    public static int[] clone(int[] array) {
        return Arrays.copyOf(array, array.length);
    }

    public static int[] mult(final int[] input, double fact) {
        int[] out = clone(input);
        for(short i = 0 ; i < input.length ; i++) {
            out[i] *= fact;
        }
        return out;
    }

    public static byte[] clone(byte[] array) {
        return Arrays.copyOf(array, array.length);
    }

    public static byte[] mult(final byte[] input, double fact) {
        byte[] out = clone(input);
        for(short i = 0 ; i < input.length ; i++) {
            out[i] *= fact;
        }
        return out;
    }

    public static long[] clone(long[] array) {
        return Arrays.copyOf(array, array.length);
    }

    public static long[] mult(final long[] input, double fact) {
        long[] out = clone(input);
        for(short i = 0 ; i < input.length ; i++) {
            out[i] *= fact;
        }
        return out;
    }

    public static double[] clone(double[] array) {
        return Arrays.copyOf(array, array.length);
    }

    public static double[] mult(final double[] input, double fact) {
        double[] out = clone(input);
        for(short i = 0 ; i < input.length ; i++) {
            out[i] *= fact;
        }
        return out;
    }

    public static <T> T[][] clone(T[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static short[][] clone(short[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static short[][] mult(short[][] matrix, double fact) {
        return Arrays.stream(matrix)
                .map(arr -> mult(arr, fact))
                .toArray(s -> matrix.clone());
    }

    public static short[][][] mult(short[][][] tensor, double fact) {
        return Arrays.stream(tensor)
                .map(arr -> mult(arr, fact))
                .toArray(s -> tensor.clone());
    }

    public static byte[][] clone(byte[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static byte[][][] clone(byte[][][] tensor) {
        return Arrays.stream(tensor)
                .map(arr -> arr.clone())
                .toArray(s -> tensor.clone());
    }

    public static byte[][] mult(byte[][] matrix, double fact) {
        return Arrays.stream(matrix)
                .map(arr -> mult(arr, fact))
                .toArray(s -> matrix.clone());
    }

    public static int[][] clone(int[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static int[][] mult(int[][] matrix, double fact) {
        return Arrays.stream(matrix)
                .map(arr -> mult(arr, fact))
                .toArray(s -> matrix.clone());
    }

    public static int[][][] mult(int[][][] matrix, double fact) {
        return Arrays.stream(matrix)
                .map(arr -> mult(arr, fact))
                .toArray(s -> matrix.clone());
    }

    public static boolean[][] clone(boolean[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static double[][] clone(double[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static double[][] mult(double[][] matrix, double fact) {
        return Arrays.stream(matrix)
                .map(arr -> mult(arr, fact))
                .toArray(s -> matrix.clone());
    }

    public static double[][][] mult(double[][][] tensor, double fact) {
        return Arrays.stream(tensor)
                .map(arr -> mult(arr, fact))
                .toArray(s -> tensor.clone());
    }

    public static long[][] clone(long[][] matrix) {
        return Arrays.stream(matrix)
                .map(arr -> arr.clone())
                .toArray(s -> matrix.clone());
    }

    public static long[][] mult(long[][] matrix, double fact) {
        return Arrays.stream(matrix)
                .map(arr -> mult(arr, fact))
                .toArray(s -> matrix.clone());
    }

    public static long[][][] mult(long[][][] tensor, double fact) {
        return Arrays.stream(tensor)
                .map(arr -> mult(arr, fact))
                .toArray(s -> tensor.clone());
    }

}
