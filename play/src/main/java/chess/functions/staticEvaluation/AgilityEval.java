package chess.functions.staticEvaluation;

public class AgilityEval {
    public static int betterForBoth(boolean mated, boolean incheck, boolean endgamePhase, boolean hasCastled, Weights aw) {
       if(mated) {
            return -aw.agility_penaltyMated;
        }
        int score = 0;
            if(incheck) {
            score -= aw.agility_penaltyInCheck;
            if(endgamePhase) {
                score -= aw.agility_penaltyInCheckEndgameExtra;
            }
        }
            if(hasCastled) {
            score += aw.agility_rewardForConsumingCastlingPrivilege;
        }
            return score;
    }

}
