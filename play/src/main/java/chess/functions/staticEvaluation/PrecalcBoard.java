package chess.functions.staticEvaluation;

import chess.board.Board;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.Piece;
import chess.functions.isendgame.DetermineEndgameStatus;
import chess.operations.Getters;
import chess.rules.Castling;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class PrecalcBoard {

    public State state = null;
    public byte nrPiecesOnBoard;
    public short Score;
    public boolean staleMate;
    public short repeatedMove;
    public boolean blackMate;
    public boolean whiteMate;
    public boolean blackInCheck;
    public boolean endGamePhase;
    public boolean whiteInCheck;
    public boolean blackCastled;
    public boolean whiteCastled;
    public boolean whoseMove;
    public boolean insufficientMaterial;
    public boolean whiteCanCastle;
    public boolean blackCanCastle;
    public HashMap<Square, PrecalcPiece> lookup = new HashMap<>();

    public static PrecalcBoard create(State state) {
        return new PrecalcBoard(state);
    }

    public PrecalcBoard(State state) {
        this.state = state;
    }

    public static void init(PrecalcBoard b, Weights aw) {
        b.staleMate = !b.state.check && b.state.getLegalMoves().size() == 0;
        b.repeatedMove = (short)b.state.repetitions;
        b.blackInCheck = b.state.check && !b.state.turn;
        b.blackMate = b.blackInCheck && b.state.getLegalMoves().size() == 0;
        b.whiteInCheck = b.state.check && b.state.turn;
        b.whiteMate = b.whiteInCheck && b.state.getLegalMoves().size() == 0;
        b.whoseMove = b.state.turn;
        b.whiteCanCastle = Castling.canCastleKingside(b.state, Board.WHITE) || Castling.canCastleQueenside(b.state, Board.WHITE);
        b.blackCanCastle = Castling.canCastleKingside(b.state, Board.BLACK) || Castling.canCastleQueenside(b.state, Board.BLACK);
        b.whiteCastled = b.state.whiteHasCastled;
        b.blackCastled = b.state.blackHasCastled;
        b.endGamePhase = DetermineEndgameStatus.isEndgame1(b.state);

        for(byte s = 0; s < Board.ranks*Board.files ; s++) {
            Square pos = Getters.getSquare(s);
            Piece piece = Getters.getPiece(b.state, pos);
            if(piece != null) {
                PrecalcPiece ep = new PrecalcPiece(b.state, piece, pos, aw, b);
                b.lookup.put(pos, ep);
                b.nrPiecesOnBoard++;
                ep.relativeRank = pos.rank;
                ep.relativeFile = pos.file;
                if(ep.pieceColor == Board.BLACK) {
                    ep.relativeRank = (byte) (Board.ranks-1 - ep.relativeRank);
                    ep.relativeFile = (byte) (Board.files-1 - ep.relativeFile);
                }
            }
        }
        /*
            Square position = ePiece.square;//Getters.getSquare(positionStraightWhite);
            //Piece piece = Getters.getPiece(board.state, position);

            byte relativeRank = position.rank;//(byte) (positionStraightWhite / Board.files);
            byte relativeFile = position.file;//(byte) (positionStraightWhite % Board.files);
            byte tableIndex = (byte) (Board.ranks*(Board.ranks-1-relativeRank)+relativeFile);
            byte relativeTableIndex = tableIndex;

            if(ePiece.pieceColor == Board.BLACK) {
                relativeRank = (byte) (Board.ranks-1 - relativeRank);
                relativeFile = (byte) (Board.files-1 - relativeFile);
                relativeTableIndex = (byte) (Board.ranks*(Board.ranks-1-relativeRank)+relativeFile);
            }
         */

        for(PrecalcPiece piece : b.lookup.values()) {
            piece.nrValidMoves++;
            ArrayList<Square> reacedOccupied = piece.piece.getReachedOccupiedSquaresDisregardingTurn(piece.square, b.state);
            for(Square square : reacedOccupied) {
                PrecalcPiece reached = b.lookup.get(square);
                addReaching(piece, reached, aw);
            }
        }
    }

    public static void init(PrecalcBoard b, Weights aw, List<String> trace) {
        b.staleMate = !b.state.check && b.state.getLegalMoves().size() == 0;
        b.repeatedMove = (short)b.state.repetitions;
        b.blackInCheck = b.state.check && !b.state.turn;
        b.blackMate = b.blackInCheck && b.state.getLegalMoves().size() == 0;
        b.whiteInCheck = b.state.check && b.state.turn;
        b.whiteMate = b.whiteInCheck && b.state.getLegalMoves().size() == 0;
        b.whoseMove = b.state.turn;
        b.whiteCanCastle = Castling.canCastleKingside(b.state, Board.WHITE) || Castling.canCastleQueenside(b.state, Board.WHITE);
        b.blackCanCastle = Castling.canCastleKingside(b.state, Board.BLACK) || Castling.canCastleQueenside(b.state, Board.BLACK);
        b.whiteCastled = b.state.whiteHasCastled;
        b.blackCastled = b.state.blackHasCastled;
        b.endGamePhase = DetermineEndgameStatus.isEndgame1(b.state);

        for(byte s = 0; s < Board.ranks*Board.files ; s++) {
            Square pos = Getters.getSquare(s);
            Piece piece = Getters.getPiece(b.state, pos);
            if(piece != null) {
                PrecalcPiece ep = new PrecalcPiece(b.state, piece, pos, aw, b);
                b.lookup.put(pos, ep);
                b.nrPiecesOnBoard++;
                ep.relativeRank = pos.rank;
                ep.relativeFile = pos.file;
                if(ep.pieceColor == Board.BLACK) {
                    ep.relativeRank = (byte) (Board.ranks-1 - ep.relativeRank);
                    ep.relativeFile = (byte) (Board.files-1 - ep.relativeFile);
                }
            }
        }

        for(PrecalcPiece piece : b.lookup.values()) {
            piece.nrValidMoves++;
            ArrayList<Square> reacedOccupied = piece.piece.getReachedOccupiedSquaresDisregardingTurn(piece.square, b.state);
            for(Square square : reacedOccupied) {
                PrecalcPiece reached = b.lookup.get(square);
                addReaching(piece, reached, aw, trace);
            }
        }
    }

    public static void addReaching(PrecalcPiece reaching, PrecalcPiece reached, Weights aw) {
        if(reached.pieceColor == reaching.pieceColor) {
            short contrib = aw.defendingValue[reaching.piece.index()][reached.piece.index()];
            reaching.defendingValue += contrib;
            reaching.noDefendRelations++;
            if(reached.lowestDefending == null) {
                reached.lowestDefending = reaching;
            }
            else {
                if(reached.lowestDefending.pieceMaterialValue > reaching.pieceMaterialValue) {
                    reached.lowestDefending = reaching;
                }
            }
        }
        else {
            short contrib = aw.attackingValue[reaching.piece.index()][reached.piece.index()];
            reaching.attackingValue += contrib;
            reaching.noAttackRelations++;
            if(reached.highestAttacking == null) {
                reached.highestAttacking = reaching;
            }
            else {
                if(reached.highestAttacking.pieceMaterialValue < reaching.pieceMaterialValue) {
                    reached.highestAttacking = reaching;
                }
            }
            if(reached.lowestAttacking == null) {
                //System.out.println("Ingen där sedan tidigare. Nu "+movingPiece);
                reached.lowestAttacking = reaching;
            }
            else {
                if(reached.lowestAttacking.pieceMaterialValue > reaching.pieceMaterialValue) {
                    //System.out.println("Förra var dyrare. Tar bort "+attackedPiece.lowestAttacker);
                    reached.lowestAttacking = reaching;
                }
            }
        }
    }

    public static void addReaching(PrecalcPiece reaching, PrecalcPiece reached, Weights aw, List<String> trace) {
        trace.add("* "+reaching.piece+" på "+reaching.square+" -> "+reached.piece+" på "+reached.square);
        if(reached.pieceColor == reaching.pieceColor) {
            short contrib = aw.defendingValue[reaching.piece.index()][reached.piece.index()];
            reaching.defendingValue += contrib;
            reaching.noDefendRelations++;
            if(reached.lowestDefending == null) {
                reached.lowestDefending = reaching;
            }
            else {
                if(reached.lowestDefending.pieceMaterialValue > reaching.pieceMaterialValue) {
                    reached.lowestDefending = reaching;
                }
            }
        }
        else {
            short contrib = aw.attackingValue[reaching.piece.index()][reached.piece.index()];
            reaching.attackingValue += contrib;
            if(reaching.pieceMaterialValue < reached.pieceMaterialValue) {
                reaching.strikesOnBetter++;
            }
            reaching.noAttackRelations++;
            if(reached.highestAttacking == null) {
                reached.highestAttacking = reaching;
            }
            else {
                if(reached.highestAttacking.pieceMaterialValue < reaching.pieceMaterialValue) {
                    reached.highestAttacking = reaching;
                }
            }
            if(reached.lowestAttacking == null) {
                //System.out.println("Ingen där sedan tidigare. Nu "+movingPiece);
                reached.lowestAttacking = reaching;
            }
            else {
                if(reached.lowestAttacking.pieceMaterialValue > reaching.pieceMaterialValue) {
                    //System.out.println("Förra var dyrare. Tar bort "+attackedPiece.lowestAttacker);
                    reached.lowestAttacking = reaching;
                }
            }
        }
    }

    public Collection<PrecalcPiece> getPieces() {
        return lookup.values();
    }

    public String toString() {
        return "PrecalcPiece{Score="+Score+", "+
        "StaleMate="+ staleMate +", "+
        "EndGamePhase="+ endGamePhase +", "+
        "RepeatedMove="+ repeatedMove +", "+
        "WhiteMate="+ whiteMate +", "+
        "BlackMate="+ blackMate +", "+
        "WhiteInCheck="+ whiteInCheck +", "+
        "BlackInCheck="+ blackInCheck +", "+
        "WhiteCastled="+ whiteCastled +", "+
        "BlackCastled="+ blackCastled +", "+
        "WhoseMove="+ whoseMove +", "+
        "InsufficientMaterial="+ insufficientMaterial +", "+
        "WhiteCanCastle="+ whiteCanCastle +", "+
        "BlackCanCastle="+ blackCanCastle +"}";
    }

}
