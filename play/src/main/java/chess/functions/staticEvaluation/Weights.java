package chess.functions.staticEvaluation;

import chess.functions.staticEvaluation.positional.DownloadedPositionRewarder;
import chess.functions.staticEvaluation.positional.PositionRewarder;
import chess.functions.staticEvaluation.positional.TablesAsDownloaded;

public class Weights {

    public int penaltyDoublePawn = 15;
    public short[] pieceValues = new short[] { 100, 315, 330, 500, 975, Short.MAX_VALUE };
    public short[] pieceValueEndGameExtra = new short[] { 3, -10, 10, 0, 0, 0 };

    public short agility_penaltyMated = Short.MAX_VALUE;
    public short agility_penaltyInCheck = 70;
    public short agility_penaltyInCheckEndgameExtra = 10;
    public short agility_rewardForConsumingCastlingPrivilege = 50;

    public short[] penaltyIsolatedPawn = new short[] { 22, 18, 16, 15, 15, 16, 18, 22 };
    public short[] rewardPassedPawn = new short[] { 34, 38, 41, 42, 42, 41, 38, 34 };

    /*
    public short[][] posValue = new short[][] {
            Tables.PawnTable8x8, Tables.KnightTable8x8, Tables.BishopTable8x8, ZeroTable8x8, ZeroTable8x8, Tables.KingTable8x8, ZeroTable8x8, ZeroTable8x8
    } ;
    public short[][] posValueEndgame = new short[][] {
            ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, Tables.KingTableEndGame8x8, ZeroTable8x8, ZeroTable8x8
    };
     */
    public PositionRewarder positionRewarder;

    public short[][] attackingValue = TablesAsDownloaded.attacValue6x6;
    public short[][] defendingValue = TablesAsDownloaded.defendValue6;
    public short[][] moveability = TablesAsDownloaded.moveability6;

    /*
    public double mayVeryWellBe = 0.82;
    public double someStability = 0.11;
    public double someImmobilisation = 0.16;
    public double mayNotBeSaved = 0.1;
     */

    public double mayVeryWellBe = 0.8;
    public double someStability = 0.0102;
    public double someImmobilisation = 0.1;
    public double mayNotBeSaved = 0.0;
    public double multipleStrike = 1.0;

    public static short[] getVector(int length, short value) {
        short[] out = new short[length];
        for(short i = 0 ; i < length ; i++) {
            out[i] = value;
        }
        return out;
    }

    public static final short[] ZeroTable8x8 = getVector(64, (short)0);

    public static final short[] ZeroTable10x8 = getVector(80, (short)0);

    public static final short[] ZeroTable10x10 = getVector(100, (short)0);

    public static Weights aw8x8 = new Weights();
    public static Weights aw10x8 = new Weights();
    public static Weights aw10x10 = new Weights();

    public static Weights[] aws = new Weights[] { aw8x8, aw10x8, aw10x10 };


    static {
        aw8x8.penaltyDoublePawn = 15;
        //aw8x8.pieceValues = new short[] { 100, 315, 330, 500, 975, 0 };
        aw8x8.pieceValues = new short[] { 100, 315, 303, 500, 975, 0 };
        aw8x8.pieceValueEndGameExtra = new short[] { 3, -10, 10, 0, 0, 0 };
        aw8x8.agility_penaltyMated = Short.MAX_VALUE;
        aw8x8.agility_penaltyInCheck = 0;
        aw8x8.agility_penaltyInCheckEndgameExtra = 0;
        aw8x8.penaltyIsolatedPawn = new short[] { 22, 18, 16, 15, 15, 16, 18, 22 };
        aw8x8.rewardPassedPawn = new short[] { 34, 38, 41, 42, 42, 41, 38, 34 };
        aw8x8.positionRewarder = new DownloadedPositionRewarder();

        /*
        aw8x8.posValue = new short[][] {
                Tables.PawnTable8x8, Tables.KnightTable8x8, Tables.BishopTable8x8, ZeroTable8x8, ZeroTable8x8, Tables.KingTable8x8
        };
        aw8x8.posValueEndgame = new short[][] {
                ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, ZeroTable8x8, Tables.KingTableEndGame8x8
        };
         */

        aw8x8.attackingValue = TablesAsDownloaded.attacValue6x6;
        aw8x8.defendingValue = TablesAsDownloaded.defendValue6;
        aw8x8.moveability = TablesAsDownloaded.moveability6;


        aw10x8.penaltyDoublePawn = 15;
        aw10x8.pieceValues = new short[] { 100, 315, 330, 500, 975, 0, 923, 886 };
        aw10x8.pieceValueEndGameExtra = new short[] { 3, -10, 10, 0, 0, 0, 0, 0 };
        aw10x8.agility_penaltyMated = Short.MAX_VALUE;
        aw10x8.agility_penaltyInCheck = 0;
        aw10x8.agility_penaltyInCheckEndgameExtra = 0;
        aw10x8.penaltyIsolatedPawn = new short[] { 22, 18, 16, 15, 15, 15, 15, 16, 18, 22 };
        aw10x8.rewardPassedPawn = new short[] { 34, 38, 41, 42, 42, 42, 42, 41, 38, 34 };
        aw10x8.positionRewarder = new DownloadedPositionRewarder();

        /*
        aw10x8.posValue = new short[][] {
                Tables.PawnTable10x8, Tables.KnightTable10x8, Tables.BishopTable10x8, ZeroTable10x8, ZeroTable10x8, Tables.KingTable10x8, ZeroTable10x8, ZeroTable10x8
        };
        aw10x8.posValueEndgame = new short[][] {
                ZeroTable10x8, ZeroTable10x8, ZeroTable10x8, ZeroTable10x8, ZeroTable10x8, Tables.KingTableEndGame10x8, ZeroTable10x8, ZeroTable10x8
        };
         */

        aw10x8.attackingValue = TablesAsDownloaded.attacValue8;
        aw10x8.defendingValue = TablesAsDownloaded.defendValue8x8;
        aw10x8.moveability = TablesAsDownloaded.moveability8;



        aw10x10.penaltyDoublePawn = 15;
        aw10x10.pieceValues = new short[] { 100, 315, 330, 500, 975, 0, 923, 886 };
        aw10x10.pieceValueEndGameExtra = new short[] { 3, -10, 10, 0, 0, 0, 0, 0 };
        aw10x10.agility_penaltyMated = Short.MAX_VALUE;
        aw10x10.agility_penaltyInCheck = 0;
        aw10x10.agility_penaltyInCheckEndgameExtra = 0;
        aw10x10.penaltyIsolatedPawn = new short[] { 22, 18, 16, 15, 15, 15, 15, 16, 18, 22 };
        aw10x10.rewardPassedPawn = new short[] { 34, 38, 41, 42, 42, 42, 42, 41, 38, 34 };
        aw10x10.positionRewarder = new DownloadedPositionRewarder();

        /*
        aw10x10.posValue = new short[][] {
                Tables.PawnTable10x10, Tables.KnightTable10x10, Tables.BishopTable10x10, ZeroTable10x10, ZeroTable10x10, Tables.KingTable10x10, ZeroTable10x10, ZeroTable10x10
        };
        aw10x10.posValueEndgame = new short[][] {
                ZeroTable10x10, ZeroTable10x10, ZeroTable10x10, ZeroTable10x10, ZeroTable10x10, Tables.KingTableEndGame10x10, ZeroTable10x10, ZeroTable10x10
        };
         */

        aw10x10.attackingValue = TablesAsDownloaded.attacValue8;
        aw10x10.defendingValue = TablesAsDownloaded.defendValue8x8;
        aw10x10.moveability = TablesAsDownloaded.moveability8;
    }

    /*
    public double mayVeryWellBe = 0.82;
    public double someStability = 0.11;
    public double someImmobilisation = 0.16;
    public double mayNotBeSaved = 0.1;
     */

    public Weights copy() {
        Weights out = new Weights();
        out.mayVeryWellBe = this.mayVeryWellBe;
        out.someStability = this.someStability;
        out.someImmobilisation = this.someImmobilisation;
        out.mayNotBeSaved = this.mayNotBeSaved;

        out.agility_penaltyInCheck = this.agility_penaltyInCheck;
        out.agility_penaltyInCheckEndgameExtra = this.agility_penaltyInCheckEndgameExtra;
        out.agility_penaltyMated = this.agility_penaltyMated;
        out.agility_rewardForConsumingCastlingPrivilege = this.agility_rewardForConsumingCastlingPrivilege;
        out.penaltyDoublePawn = this.penaltyDoublePawn;
        out.attackingValue = Cloning.clone(this.attackingValue);
        out.defendingValue = Cloning.clone(this.defendingValue);
        out.moveability = Cloning.clone(this.moveability);
        out.pieceValueEndGameExtra = Cloning.clone(this.pieceValueEndGameExtra);
        out.penaltyIsolatedPawn = Cloning.clone(this.penaltyIsolatedPawn);
        out.pieceValues = Cloning.clone(this.pieceValues);
        //out.posValue = Cloning.clone(this.posValue);
        out.rewardPassedPawn = Cloning.clone(this.rewardPassedPawn);
        //out.posValueEndgame = Cloning.clone(this.posValueEndgame);
        out.positionRewarder = this.positionRewarder;

        return out;
    }
}
