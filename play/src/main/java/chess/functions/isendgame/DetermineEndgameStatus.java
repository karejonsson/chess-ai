package chess.functions.isendgame;

import chess.board.State;

public class DetermineEndgameStatus {

    public static int countPieces(State state) {
        int sum = 0;
        for(int i = 0 ; i < state.wPieces.length ; i++) {
            sum += (state.wPieces[i] + state.bPieces[i]);
        }
        return sum;
    }

    public static boolean isEndgame1(State state) {
        return countPieces(state) <= 9;
    }

}
