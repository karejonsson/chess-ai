package chess.functions.isendgame;

import chess.board.State;

public interface EndgameDeterminator {
    boolean isEndgame(State state);
}
