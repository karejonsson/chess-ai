package chess.functions;

import chess.board.State;
import chess.board.pieces.*;

public class Measures {

    // Calculates the values of all pieces
    public static int whitesPieceAdvantage(State state) {
        int sum = 0;
        for(int i = 0 ; i < state.wPieces.length ; i++) {
            sum += state.wPieces[i]*BruteForceFunctions.values[i] -
                    state.bPieces[i]*BruteForceFunctions.values[i];
        }
        return sum;
    }

    public static boolean possiblyEnoughForMate(byte[] pieces) {
        if(certainlyEnoughForMate(pieces)) {
            return true;
        }
        if(pieces[Pawn.idx] > 0) {
            return true;
        }
        return false;
    }

    private static boolean certainlyEnoughForMate(byte[] pieces) {
        if(pieces[Pawn.idx] > 0 && (pieces[Bishop.idx] + pieces[Knight.idx] > 0)) {
            return true;
        }
        if(pieces[Bishop.idx] > 1) {
            return true; // Two bishops
        }
        if(pieces[Knight.idx] > 0 && pieces[Bishop.idx] > 0) {
            return true; // One knight, one bishop
        }
        if(pieces[Rook.idx] > 0) {
            return true; // One rook
        }
        if(pieces[Queen.idx] > 0) {
            return true; // One queen
        }
        return false;
    }

    public static boolean isAhead(State state, boolean color) {
        int wa = Measures.whitesPieceAdvantage(state);
        return color ? wa > 0 : wa < 0;
    }

}
