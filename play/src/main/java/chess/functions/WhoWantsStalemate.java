package chess.functions;

import chess.board.State;

public interface WhoWantsStalemate {
    Boolean eval(State state);
}
