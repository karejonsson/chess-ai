package chess.functions;

import chess.board.State;

public interface StaticStateEvaluation {
    int eval(State state);
}
