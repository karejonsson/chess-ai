package chess.functions;

import chess.board.State;

public interface MovesValueWithStateEvaluation {
    int eval(int movesValue, State state);
}
