package chess.functions;

import chess.board.Board;
import chess.board.Move;
import chess.operations.Moving;
import chess.rules.Check;
import chess.board.State;
import chess.board.BidirectionalTree;

import java.util.HashMap;
import java.util.Map;

public class MinimaxCalcule {

    private StaticStateEvaluation se = null;
    private WhoWantsStalemate wws = null;
    private HashMap<Integer, Integer>[] maxDepthHashWhite = null;
    private HashMap<Integer, Integer>[] maxDepthHashBlack = null;
    private final byte maxdepth;
    private int countAll = 0;
    private int countRecall = 0;
    private int countMinimPrunes = 0;
    private int countMaximPrunes = 0;
    private Map<Integer, Byte> countRepetitions = null;

    public MinimaxCalcule(StaticStateEvaluation se, WhoWantsStalemate wws, byte maxdepth, Map<Integer, Byte> countRepetitions) {
        this.se = se;
        this.wws = wws;
        this.maxdepth = maxdepth;
        this.countRepetitions = countRepetitions;
        maxDepthHashWhite = new HashMap[maxdepth+1];
        maxDepthHashBlack = new HashMap[maxdepth+1];
        for(int i = 0 ; i <= maxdepth ; i++) {
            maxDepthHashWhite[i] = new HashMap<>();
            maxDepthHashBlack[i] = new HashMap<>();
        }
    }

    public String getPerformanceMsg() {
        return "Alla: "+countAll+", återvunna "+countRecall+", minimeringsbeskär "+ countMinimPrunes +", maximeringsbeskär "+ countMaximPrunes;
    }

    /*
    public BidirectionalTree<State> buildAnalysis(State state, byte depth) {
        //System.out.println("MinimaxCalcule: depth="+depth);
        BidirectionalTree<State> root = new BidirectionalTree();
        root.current = state;
        root.value = minimax(root, depth, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return root;
    }
     */

    public int eval(State state, byte depth) {
        return minimax(state, depth, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public int eval(BidirectionalTree<State> root, byte depth) {
        return minimax(root, depth, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static BidirectionalTree<State> next(BidirectionalTree<State> root, State state) {
        BidirectionalTree<State> out = new BidirectionalTree<>();
        out.previous = root;
        root.successors.add(out);
        out.current = state;
        return out;
    }

    public static BidirectionalTree<State> next(BidirectionalTree<State> root) {
        BidirectionalTree<State> out = new BidirectionalTree<>();
        out.previous = root;
        root.successors.add(out);
        return out;
    }

    private int minimax(State state, byte depth, int alpha, int beta) {
        countAll++;

        if(Check.isMate(state)) {
            // Nödvändigt med snabb matt. Annars blir matt i 2 lika bra som matt direkt och
            // då vet den inte vilken den skall välja.
            return !state.turn ? Integer.MAX_VALUE-100+depth : Integer.MIN_VALUE+100-depth;
        }

        if(Check.isStaleMate(state) || state.repetitions == Board.maxrep) {
            Boolean favourable = wws.eval(state);
            if(favourable == null) {
                // Oklart vem som står bäst så patt är dåligt
                return state.turn ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            if(favourable.equals(state.turn) ) {
                // Spelaren står för bra för patt så patt är dåligt
                return state.turn ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            // Spelaren står dåligt så patt är bra
            return state.turn ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        }

        int zhash = state.zobrist.hashCode();

        HashMap<Integer, Integer> hasher = (state.turn ? maxDepthHashWhite : maxDepthHashBlack)[depth];
        if(depth == 0) {
            countRecall++;
            Integer staticScore = null;//hasher.get(zhash);
            if(staticScore != null) {
                //System.out.println("Stat: Återanvändning, count="+count);
                return staticScore;
            }
            staticScore = se.eval(state);
            hasher.put(zhash, staticScore);
            return staticScore;
        }

        Integer score = hasher.get(zhash);
        if(score != null) {
            //System.out.println("Återanvändning av värde. depth="+depth+", spelare "+(state.turn ? "VIT" : "SVART"));
            return score;
        }

        if(state.turn) {
            int maxEval = Integer.MIN_VALUE;
            for(Move move : state.getLegalMoves()) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                int eval = minimax(nextState, (byte)(depth-1), alpha, beta);
                nextState.clearLegalMoves();
                maxEval = Math.max(maxEval, eval);
                alpha = Math.max(alpha, eval);
                if(beta <= alpha) {
                    countMaximPrunes++;
                    break;
                }
            }
            hasher.put(state.zobrist.hashCode(), maxEval);
            return maxEval;
        }
        else {
            int minEval = Integer.MAX_VALUE;
            for(Move move : state.getLegalMoves()) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                int eval = minimax(nextState, (byte)(depth-1), alpha, beta);
                nextState.clearLegalMoves();
                minEval = Math.min(minEval, eval);
                beta = Math.min(beta, eval);
                if(beta <= alpha) {
                    countMinimPrunes++;
                    break;
                }
            }
            hasher.put(state.zobrist.hashCode(), minEval);
            return minEval;
        }
    }

    private int minimax(BidirectionalTree<State> root, byte depth, int alpha, int beta) {
        State state = root.current;
        countAll++;

        if(Check.isMate(state)) {
            return !state.turn ? Integer.MAX_VALUE-100+depth : Integer.MIN_VALUE+100-depth;
        }

        if(Check.isStaleMate(state) || state.repetitions == Board.maxrep) {
            Boolean favourable = wws.eval(state);
            if(favourable == null) {
                // Oklart vem som står bäst så patt är dåligt
                return state.turn ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            if(favourable.equals(state.turn) ) {
                // Spelaren står för bra för patt så patt är dåligt
                return state.turn ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            // Spelaren står dåligt så patt är bra
            return state.turn ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        }

        int zhash = state.zobrist.hashCode();

        HashMap<Integer, Integer> hasher = (state.turn ? maxDepthHashWhite : maxDepthHashBlack)[depth];
        if(depth == 0) {
            countRecall++;
            Integer staticScore = null;//hasher.get(zhash);
            if(staticScore != null) {
                //System.out.println("Stat: Återanvändning, count="+count);
                return staticScore;
            }
            staticScore = se.eval(state);
            hasher.put(zhash, staticScore);
            return staticScore;
        }

        Integer score = null;//hasher.get(zhash);
        if(score != null) {
            //System.out.println("Återanvändning av värde. depth="+depth+", spelare "+(state.turn ? "VIT" : "SVART"));
            return score;
        }

        if(state.turn) {
            int maxEval = Integer.MIN_VALUE;
            for(Move move : state.getLegalMoves()) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                BidirectionalTree<State> branch = MinimaxCalcule.next(root, nextState);
                int eval = minimax(branch, (byte)(depth-1), alpha, beta);
                branch.value = eval;
                nextState.clearLegalMoves();
                maxEval = Math.max(maxEval, eval);
                alpha = Math.max(alpha, eval);
                if(beta <= alpha) {
                    countMaximPrunes++;
                    break;
                }
            }
            hasher.put(state.zobrist.hashCode(), maxEval);
            return maxEval;
        }
        else {
            int minEval = Integer.MAX_VALUE;
            for(Move move : state.getLegalMoves()) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                BidirectionalTree<State> branch = MinimaxCalcule.next(root, nextState);
                int eval = minimax(branch, (byte)(depth-1), alpha, beta);
                branch.value = eval;
                nextState.clearLegalMoves();
                minEval = Math.min(minEval, eval);
                beta = Math.min(beta, eval);
                if(beta <= alpha) {
                    countMinimPrunes++;
                    break;
                }
            }
            hasher.put(state.zobrist.hashCode(), minEval);
            return minEval;
        }
    }

    public int staticEval(State state) {
        return se.eval(state);
    }

}
