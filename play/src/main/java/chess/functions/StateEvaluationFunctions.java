package chess.functions;

import chess.board.Move;
import chess.board.State;

public class StateEvaluationFunctions {

    public static int accumulatingMoveValues(State state, StaticMoveEvaluation sme, MovesValueWithStateEvaluation mvse) {
        int eval = 0;
        if(state.turn) {
            // Vit drar
            for(Move move : state.getLegalMoves()) {
                eval += sme.eval(state, move);
                //countEvaluations++;
            }
        }
        else {
            // Svart drar
            for(Move move : state.getLegalMoves()) {
                eval -= sme.eval(state, move);
                //countEvaluations++;
            }
        }
        return mvse.eval(eval, state);//eval+300*Measures.whitesPieceAdvantage(state);
    }

    public static int maximizingMoveValues(State state, StaticMoveEvaluation sme, MovesValueWithStateEvaluation mvse) {
        int eval = 0;
        if(state.turn) {
            // Vit drar
            eval = Integer.MIN_VALUE;
            for(Move move : state.getLegalMoves()) {
                eval = Math.max(eval, sme.eval(state, move));
                //countEvaluations++;
            }
        }
        else {
            // Svart drar
            eval = Integer.MAX_VALUE;
            for(Move move : state.getLegalMoves()) {
                eval = Math.min(eval, sme.eval(state, move));
                //countEvaluations++;
            }
        }
        return mvse.eval(eval, state);//eval+300*Measures.whitesPieceAdvantage(state);
    }
}
