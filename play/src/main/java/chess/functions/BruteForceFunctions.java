package chess.functions;

import chess.board.*;
import chess.operations.Getters;
import chess.board.pieces.*;
import chess.operations.Moving;
import chess.rules.Check;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BruteForceFunctions {

    private static long lastprint = System.currentTimeMillis();

    /*
    static {
        Thread t = new Thread(() -> {
            while(true) {
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                }
                if (System.currentTimeMillis() - lastprint > 9999) {
                    printCount("---");
                }
            }
        });
        t.start();
    }
     */

    private static int[] count = new int[Board.pieces];
    public static void clearCount() {
        count = new int[Board.pieces];
    }

    public static void printCount(String first) {
        lastprint = System.currentTimeMillis();
        System.out.print(first+count[0]);
        for(int i = 1 ; i < count.length ; i++) {
            System.out.print(", "+count[i]);
        }
        System.out.print("\n");
    }

    public static ArrayList<Move> getBestMoves(State state, byte depth, Map<Integer, Byte> countRepetitions) {
        int ratings[];
        ratings = new int[state.getLegalMoves().size()];
        int index = 0;
        int max = -100000;  // value of best move
        for(Move m : state.getLegalMoves()) {
            ratings[index] = rateMove(state, m, depth, countRepetitions);
            if (ratings[index] > max) {
                max = ratings[index];
            }
            index++;
        }

        // Make a list with the optimal moves for return
        ArrayList<Move> v = new ArrayList<>();
        for (int i = 0 ; i < index ; i++) {
            if (ratings[i] == max) {
                v.add(state.getLegalMoves().get(i));
            }
        }

        return v;
    }

    public static final int[] values = new int[] { 1, 3, 3, 5, 9, Integer.MAX_VALUE };

    public static int evalMove(final State state, final Move m) {
        int value = 0;

        if (m.promotion) {
            // promotion is worth a new Queen
            value += values[Queen.idx]*10;
        }
        if (Getters.isOccupied(state, m.to)) {
            // Add value for the piece captured
            value += 10 * values[Getters.getPiece(state, m.to).index()];
        }
        else if (m.enpKill != null) {
            // en Passant is special case
            value += 10;
        }
        else if (m.castle != (char) 0) {
            // castling is worth an arbitrary 50
            value += values[Rook.idx]*10;
        }
        else if (Getters.getPiece(state, m.from).index() == King.idx) {
            // take away a bit for moving king
            value -= 10;
        }

        // Attach an small value to center squares
        double r = Board.ranks-Math.abs(middleR-m.to.rank)-Math.abs(middleR-m.to.file);
        value += r;

        return value;
    }

    public static void evalMoveDebug(final State state, final Move m, boolean color) {
        boolean turn = state.turn;
        state.turn = color;
        int value = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("EVAL: "+m.toString()+": ");
        if (m.promotion) {
            // promotion is worth a new Queen
            int r = values[Queen.idx]*10;
            sb.append(" Promo "+r);
            value += r;
        }
        if (Getters.isOccupied(state, m.to)) {
            // Add value for the piece captured
            int r = 10 * values[Getters.getPiece(state, m.to).index()];
            sb.append(" Capt "+r);
            value += r;
        }
        else if (m.enpKill != null) {
            // en Passant is special case
            int r = 10;
            sb.append(" Passa "+r);
            value += r;
        }
        else if (m.castle != (char) 0) {
            // castling is worth an arbitrary 50
            int r = values[Rook.idx]*10;
            sb.append(" Cast "+r);
            value += r;
        }
        else if (Getters.getPiece(state, m.from).index() == King.idx) {
            // take away a bit for moving king
            int r = -10;
            sb.append(" movK "+r);
            value -= r;
        }

        // Attach an small value to center squares
        double r = Board.ranks-Math.abs(middleR-m.to.rank)-Math.abs(middleR-m.to.file);
        sb.append(" Midd "+r);
        value += r;

        sb.append(" Tot "+value);
        System.out.println(sb.toString());
        state.turn = turn;
    }

    public final static double middleR = (Board.ranks-1)/2;

    public static void rateMoveDebug(final State state, final Move m, final int depth, Map<Integer, Byte> countRepetitions) {
        int value = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("RATE: "+m.toString()+": ");

        if(depth == 0) {
            // base case
            int r = evalMove(state, m);
            sb.append(" Eval "+r);
            return;
        }

        // Get the rating for just this move
        int nominal = evalMove(state, m);
        sb.append(" Nomi "+nominal);
        // Make the actual move
        State after = Moving.attempted(state, m, countRepetitions);
        Match copy = new Match(after);
        //copy.makeMove(m);

        // find all recourses
        //List<Move> legalMoves = MixOfGameStuff.legalMoves(state);
        List<Move> legalMoves = copy.getAllMoves();

        if (legalMoves.size() == 0) {
            // If there are no legal moves + check...
            if (Check.isInCheck(copy.state, copy.state.turn, Getters.getLastMove(state))) {
                int r = 1000+depth;
                sb.append(" Check "+r);
                System.out.println(sb.toString());
                return; // Mate in two is better than mate in 1
            }
            else {
                int r = Measures.isAhead(copy.state, state.turn) ? -100 : 100;
                sb.append(" Ahead "+r);
                System.out.println(sb.toString());
                return;
            }
        }
        else {
            // Otherwise go through all legal moves
            int maxRating = -1000;
            for(Move move : legalMoves) {
                int rating = rateMove(copy.state, move, depth-1, countRepetitions);
                maxRating = Math.max(maxRating, rating);
            }
            sb.append(" maxRa "+maxRating);
            int r = nominal - maxRating;
            sb.append(" out "+r);
            System.out.println(sb.toString());
            return;
        }
    }

    public static int rateMove(final State state, final Move m, final int depth, Map<Integer, Byte> countRepetitions) {
        //count[depth]++;
        int value = 0;

        if(depth == 0) {
            // base case
            return evalMove(state, m);
        }

        // Get the rating for just this move
        int nominal = evalMove(state, m);

        // Make the actual move
        State after = Moving.attempted(state, m, countRepetitions);
        Match copy = new Match(after);
        //copy.makeMove(m);

        // find all recourses
        List<Move> legalMoves = copy.getAllMoves();

        if (legalMoves.size() == 0) {
            // If there are no legal moves + check...
            if (Check.isInCheck(copy.state, copy.state.turn, Getters.getLastMove(state))) {
                return 1000+depth; // Mate in two is better than mate in 1
            }
            else {
                return Measures.isAhead(copy.state, state.turn) ? -100 : 100;
            }
        }
        else {
            // Otherwise go through all legal moves
            int maxRating = -1000;
            int rating;
            //Enumeration<Move> _enum = copy.getAllLegalMoves().elements();
            List<Move> moves = copy.getAllMoves();
            //while(_enum.hasMoreElements()) {
            for(Move move : moves) {
                rating = rateMove(copy.state, move, depth-1, countRepetitions);//copy.rateMove(move, depth-1);
                if (rating > maxRating) {
                    maxRating = rating;
                }
            }

            return (nominal - maxRating);
        }
    }

}
