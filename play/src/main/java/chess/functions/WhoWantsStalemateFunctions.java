package chess.functions;

import chess.board.State;

public class WhoWantsStalemateFunctions {

    public static Boolean whoIsToGoodForStalemate(State state) {
        int wa = Measures.whitesPieceAdvantage(state);
        if(Math.abs(wa) <= 2) {
            return null;
        }
        return wa > 0;
    }

}
