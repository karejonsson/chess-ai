package chess.functions;

import chess.board.Move;
import chess.board.State;

public interface StaticMoveEvaluation {
    int eval(State state, Move move);
}
