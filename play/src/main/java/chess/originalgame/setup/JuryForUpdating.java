package chess.originalgame.setup;

import chess.wrappers.ResultWrapper;

public interface JuryForUpdating {
    boolean shallUpdate(ResultWrapper res);
}
