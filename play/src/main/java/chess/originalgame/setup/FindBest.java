package chess.originalgame.setup;

import chess.execution.CompetersEvaluator;
import chess.board.Player;
import chess.reuse.MutatingNetworkPlayer;
import chess.wrappers.ResultWrapper;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

public class FindBest {
/*
    public static ResultWrapper runTwoSides(int nr, CompetersEvaluator player, String filename_template, JuryForUpdating jury) throws Exception {
        String filename = filename_template.replaceAll("<NR>", ""+nr);
        FileInputStream fis = new FileInputStream(filename);
        MutationNetwork master = MutationNetwork.deserialize(fis, StandardCharsets.UTF_8);
        MutationNetwork[] challengers = SimpleMutators.createMutations(master, 0.2, 0.4);

        Player pm = new MutatingNetworkPlayer(master);

        ResultWrapper m1 = Permutation.permuteForGeneralBestTwoSides(pm, new MutatingNetworkPlayer(challengers[0]), player);

        if(m1 != null && jury.shallUpdate(m1)) {
            nr++;
            filename = filename_template.replaceAll("<NR>", ""+nr);
            challengers[0].serialize(new FileOutputStream(filename), StandardCharsets.UTF_8);
            //System.out.println("m1="+m1);
            return m1;
        }

        ResultWrapper m2 = Permutation.permuteForGeneralBestTwoSides(pm, new MutatingNetworkPlayer(challengers[1]), player);

        if(m2 != null && jury.shallUpdate(m2)) {
            nr++;
            filename = filename_template.replaceAll("<NR>", ""+nr);
            challengers[1].serialize(new FileOutputStream(filename), StandardCharsets.UTF_8);
            return m2;
        }
        //System.out.println("m1="+m1+", m2="+m2);
        return null;
    }

    public static ResultWrapper runOneSide(int nr, CompetersEvaluator player, String filename_template, JuryForUpdating jury) throws Exception {
        String filename = filename_template.replaceAll("<NR>", ""+nr);
        FileInputStream fis = new FileInputStream(filename);
        MutationNetwork master = MutationNetwork.deserialize(fis, StandardCharsets.UTF_8);
        MutationNetwork[] challengers = SimpleMutators.createMutations(master, 0.3, 0.7);

        Player pm = new MutatingNetworkPlayer(master);

        ResultWrapper m1 = Permutation.permuteForGeneralBest(pm, new MutatingNetworkPlayer(challengers[0]), player);

        if(m1 != null && jury.shallUpdate(m1)) {
            nr++;
            filename = filename_template.replaceAll("<NR>", ""+nr);
            challengers[0].serialize(new FileOutputStream(filename), StandardCharsets.UTF_8);
            //System.out.println("m1="+m1);
            return m1;
        }

        return null;
    }
*/

}
