package chess.originalgame.setup;

import chess.board.Move;
import chess.board.State;
import chess.execution.CompetersEvaluator;
import chess.operations.Initiation;
import chess.board.Player;
import chess.wrappers.ResultWrapper;

import java.util.List;

public class Permutation {

    public static ResultWrapper permuteForGeneralBestTwoSides(Player master, Player challenger, CompetersEvaluator gameDriver) throws Exception {
        gameDriver.init();
        List<Move> moves = gameDriver.getMoves();//MoveManipulation.findMoves(board.state);
        ResultWrapper res = new ResultWrapper();
        State initialState = gameDriver.getState();//Initiation.clone(board.state);

        for(Move move : moves) {
            gameDriver.init(initialState);
            //System.out.println("Move "+move);
            State stateOneMove = gameDriver.move(move);//MoveManipulation.makeMove(board.state, move);
            ResultWrapper or = decideIfMasterIsBetterOnePosTwoSides(
                    master,
                    challenger,
                    stateOneMove,
                    gameDriver,
                    "Move "+move);
            res = res.add(or);
            //break;
        }
        return res;
    }

    public static ResultWrapper permuteForGeneralBest(Player master, Player challenger, CompetersEvaluator gameDriver) throws Exception {
        gameDriver.init();
        List<Move> moves = gameDriver.getMoves();//MoveManipulation.findMoves(board.state);
        ResultWrapper res = new ResultWrapper();
        State initialState = gameDriver.getState();//Initiation.clone(board.state);

        for(Move move : moves) {
            gameDriver.init(initialState);
            //System.out.println("Move "+move);
            State stateOneMove = gameDriver.move(move);//MoveManipulation.makeMove(board.state, move);
            ResultWrapper or = decideIfMasterIsBetterOnePos(
                    master,
                    challenger,
                    stateOneMove,
                    gameDriver,
                    "Move "+move);
            res = res.add(or);
            //break;
        }
        return res;
    }

    public static ResultWrapper decideIfMasterIsBetterOnePos(Player master, Player challenger, State state, CompetersEvaluator player, String descriptionOfState) throws Exception {
        //System.out.println("1: "+Serialization.getString(state));

        player.init(Initiation.clone(state));
        Boolean r = player.eval(master, challenger, descriptionOfState);
        ResultWrapper res1 = forMaster(r == null ? 0.5 : (r ? 1.0 : 0.0));

        return res1;
    }

    public static ResultWrapper forMaster(double resMaster) {
        ResultWrapper out = new ResultWrapper();
        out.master = resMaster;
        out.challenger = 1.0-resMaster;
        return out;
    }

    public static ResultWrapper decideIfMasterIsBetterOnePosTwoSides(Player master, Player challenger, State state, CompetersEvaluator player, String descriptionOfState) throws Exception {
        //System.out.println("1: "+Serialization.getString(state));

        player.init(Initiation.clone(state));
        Boolean r = player.eval(master, challenger, descriptionOfState);
        ResultWrapper res1 = forMaster(r == null ? 0.5 : (r ? 1.0 : 0.0));

        player.init(Initiation.clone(state));
        r = player.eval(challenger, master, descriptionOfState);
        ResultWrapper res2 = forMaster(r == null ? 0.5 : (r ? 0.0 : 1.0));

        return res1.add(res2);
    }

}
