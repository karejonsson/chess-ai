package chess.player;

import chess.board.*;
import chess.functions.StaticStateEvaluation;
import chess.functions.WhoWantsStalemate;
import chess.functions.staticEvaluation.EvalMain;
import chess.operations.Moving;
import chess.functions.MinimaxCalcule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BruteForceDepthPlayerMinimax implements Player {

    private byte depth = 0;
    private MinimaxCalcule mm = null;
    private Map<Integer, Byte> countRepetitions = null;

    public BruteForceDepthPlayerMinimax(byte depth, StaticStateEvaluation se, WhoWantsStalemate wws, Map<Integer, Byte> countRepetitions) {
        this.depth = depth;
        this.countRepetitions = countRepetitions;
        mm = new MinimaxCalcule(se, wws, depth, countRepetitions);
    }

    @Override
    public Move findMove(Match board) {
        List<Move> nextMoves = board.getAllMoves();
        Map<Integer, Byte> countRepetitions = board.countRepetitions;
        State state = board.state;
        int value;

        Move nextBestMove = nextMoves.get(0);
        if(state.turn) {
            // Maximerande spelare, dvs motspelaren minimerar
            value = Integer.MIN_VALUE;
            // Logiken är 'minimera godhet för motståndaren'
            for(Move move : nextMoves) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                int nextMoveValue = mm.eval(nextState, depth);
                if(nextMoveValue > value) {
                    value = nextMoveValue;
                    nextBestMove = move;
                }
            }
        }
        else {
            // Minimerande spelare, dvs motspelaren maximerar
            value = Integer.MAX_VALUE;
            // Logiken är 'maximera dålighet för motståndaren'
            for(Move move : nextMoves) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                int nextMoveValue = mm.eval(nextState, depth);
                if(nextMoveValue < value) {
                    value = nextMoveValue;
                    nextBestMove = move;
                }
            }
        }

        return nextBestMove;
    }

    @Override
    public BidirectionalTree<State> analysis(State state) {
        if(state.turn) {
            System.out.println("Vit spelar, maximerar");
        }
        else {
            System.out.println("Svart spelar, minimerar");
        }
        BidirectionalTree<State> root = new BidirectionalTree();
        Map<Integer, Byte> countRepetitions = new HashMap<>();
        List<Move> nextMoves = state.getLegalMoves();
        int value;

        Move nextBestMove = nextMoves.get(0);
        if(state.turn) {
            // Maximerande spelare, dvs motspelaren minimerar
            value = Integer.MIN_VALUE;
            // Logiken är 'minimera godhet för motståndaren'
            for(Move move : nextMoves) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                BidirectionalTree<State> branch = MinimaxCalcule.next(root, nextState);
                int nextMoveValue = mm.eval(branch, depth);
                branch.value = nextMoveValue;
                if(nextMoveValue > value) {
                    value = nextMoveValue;
                    nextBestMove = move;
                }
            }
        }
        else {
            // Minimerande spelare, dvs motspelaren maximerar
            value = Integer.MAX_VALUE;
            // Logiken är 'maximera dålighet för motståndaren'
            for(Move move : nextMoves) {
                State nextState = Moving.attempted(state, move, countRepetitions);
                BidirectionalTree<State> branch = MinimaxCalcule.next(root, nextState);
                int nextMoveValue = mm.eval(branch, depth);
                branch.value = nextMoveValue;
                if(nextMoveValue < value) {
                    value = nextMoveValue;
                    nextBestMove = move;
                }
            }
        }

        root.current = nextBestMove.state;
        root.value = value;
        return root;
    }

    @Override
    public String getName() {
        return "BruteForceDepthPlayerMinimax("+depth+")";
    }

    @Override
    public int staticEval(State state, List<String> trace) throws Exception {
        state.clearLegalMoves();
        state.prepareForStaticAnalysis();
        int out = EvalMain.evaluateBoardScore(state, trace);
        int internal = mm.staticEval(state);
        trace.add("Internt värde: "+internal);
        return internal;
    }

}