package chess.player;

import chess.board.*;
import chess.functions.BruteForceFunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BruteForceDepthPlayerAsDownloaded implements Player {

    private byte depth = 0;
    private Map<Integer, Byte> countRepetitions = null;

    public BruteForceDepthPlayerAsDownloaded(byte depth, Map<Integer, Byte> countRepetitions) {
        this.depth = depth;
        this.countRepetitions = countRepetitions;
    }

    @Override
    public Move findMove(Match board) {
        BruteForceFunctions.clearCount();
        ArrayList<Move> v = BruteForceFunctions.getBestMoves(board.state, depth, countRepetitions);
        BruteForceFunctions.printCount("");
        Move m = v.get((int)(Math.random()*v.size()));
        return m;
    }

    @Override
    public BidirectionalTree<State> analysis(State state) {
        return null;
    }

    @Override
    public String getName() {
        return "BruteForceDepthPlayerAsDownloaded("+depth+")";
    }

    @Override
    public int staticEval(State state, List<String> trace) throws Exception {
        throw new Exception("Ej implementerat i ursprungliga spelaren");
    }

}
