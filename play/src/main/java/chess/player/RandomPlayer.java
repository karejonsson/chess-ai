package chess.player;

import chess.board.*;

import java.util.List;

public class RandomPlayer implements Player {

    @Override
    public Move findMove(Match board) {
        List<Move> moves = board.getAllMoves();
        Move m = moves.get((int)(Math.random()*moves.size()));
        return m;
    }

    @Override
    public BidirectionalTree<State> analysis(State state) {
        return null;
    }

    @Override
    public String getName() {
        return "RandomPlayer";
    }

    @Override
    public int staticEval(State state, List<String> trace) throws Exception {
        throw new Exception("Ej implementerat i slump-spelaren");
    }

}
