package chess.wrappers;

public class ResultWrapper {
    public double master = 0;
    public double challenger = 0;
    public ResultWrapper add(ResultWrapper other) {
        ResultWrapper out = new ResultWrapper();
        out.master = other.master + master;
        out.challenger = other.challenger + challenger;
        return out;
    }
    public String toString() {
        return "Master: "+master+" - Challenger: "+challenger;
    }
}


