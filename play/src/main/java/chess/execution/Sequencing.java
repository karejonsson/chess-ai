package chess.execution;

import chess.board.*;
import chess.board.Player;

public class Sequencing {

    public static String timePretty(long l) {
        long tenths = l/100;
        long t = tenths % 10;
        long secs_tot = tenths/10;
        long secs = secs_tot % 60;
        long mins_tot = secs_tot/60;
        if(mins_tot < 1) {
            return ""+secs+"."+t;
        }
        long mins = mins_tot % 60;
        long hours_tot = mins/60;
        if(hours_tot < 1) {
            return ""+mins+":"+(secs<=9?"0":"")+secs+"."+t;
        }
        return ""+hours_tot+":"+(mins<=9?"0":"")+mins+":"+(secs<=9?"0":"")+secs+"."+t;
    }

    public static void runSingleThread(MatchNotifying board, Player white, Player black) {
        //board.init();
        board.setPlayerW(white);
        board.setPlayerB(black);
        while(board.continues()) {
            board.surveillor.onWhite("White: "+ timePretty(board.timeWhite));
            board.surveillor.onBlack("Black: "+ timePretty(board.timeBlack));
            boolean player = board.state.turn;
            try {
                PlayUpdateTime.playOneMove(board, white, black);
            }
            catch(Exception e) {
                e.printStackTrace();
                break;
            }
        }
    }

    public static void runSingleThreadCountedPiecesAheadWins(MatchNotifying board, Player white, Player black, int maxMoves) throws Exception {
        //board.init();
        int ctr = 0;
        while(board.continues()) {
            board.surveillor.onWhite("White: "+ timePretty(board.timeWhite));
            board.surveillor.onBlack("Black: "+ timePretty(board.timeBlack));
            boolean player = board.state.turn;
            ctr++;
            if(ctr > maxMoves) {
                board.surveillor.onEnd();
                return;
            }
            PlayUpdateTime.playOneMove(board, white, black);
        }
    }

    public static void runSingleThreadCounted(MatchNotifying board, Player whitePlayer, Player blackPlayer, int maxMoves) throws Exception {
        //board.init();
        int ctr = 0;
        while(board.continues()) {
            board.surveillor.onWhite("White: "+ timePretty(board.timeWhite));
            board.surveillor.onBlack("Black: "+ timePretty(board.timeBlack));
            boolean player = board.state.turn;
            ctr++;
            if(ctr > maxMoves) {
                board.surveillor.onEnd();
                return;
            }
            PlayUpdateTime.playOneMove(board, whitePlayer, blackPlayer);
        }
    }


}
