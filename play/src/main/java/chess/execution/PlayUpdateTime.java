package chess.execution;

import chess.board.Match;
import chess.board.Move;
import chess.operations.Moving;
import chess.board.Player;

public class PlayUpdateTime {

    public static void playOneMove(Match board, Player white, Player black) {
        playOneMove(board, board.state.turn ? white : black);
    }

    public static void playOneMove(Match board, Player player) {
        long before = System.currentTimeMillis();
        Move m = player.findMove(board);
        if(m == null) {
            System.out.println("Fick null som drag av "+player);
        }
        long after = System.currentTimeMillis();
        if(board.state.turn) {
            board.timeWhite += (after-before);
        }
        else {
            board.timeBlack += (after-before);
        }
        Moving.real(board, m);
        board.state.timeWhite = board.timeWhite;
        board.state.timeBlack = board.timeBlack;
        board.readyNextMove();
    }

}
