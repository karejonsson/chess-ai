package chess.execution;

import chess.board.Move;
import chess.board.State;
import chess.board.Player;

import java.util.List;

public interface CompetersEvaluator {

    void init();
    void init(State state);
    State move(Move m);
    Boolean eval(Player white, Player black, String descriptionOfState) throws Exception;
    List<Move> getMoves();
    State getState();

}
