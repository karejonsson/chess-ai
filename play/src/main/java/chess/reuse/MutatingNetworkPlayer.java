package chess.reuse;

import chess.board.*;
import chess.operations.Moving;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class MutatingNetworkPlayer implements Player {
    @Override
    public Move findMove(Match board) {
        return null;
    }

    @Override
    public BidirectionalTree<State> analysis(State state) {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int staticEval(State state, List<String> trace) throws Exception {
        return 0;
    }
/*
    private MutationNetwork mn = null;

    public MutatingNetworkPlayer(MutationNetwork mn) throws Exception {
        this.mn = mn;
    }

    public MutatingNetworkPlayer(String filename) throws Exception {
        FileInputStream fis = new FileInputStream(filename);
        mn = MutationNetwork.deserialize(fis, StandardCharsets.UTF_8);
        name = "MutatingNetwork(File: '"+filename+"')";
    }

    @Override
    public Move findMove(Match board) {
        double bestResult = -100000000.0;
        Move bestMove = null;
        List<Move> moves = board.getAllMoves();
        try {
            for(Move move : moves) {
                State state = Moving.attempted(board.state, move, board.countRepetitions);
                double[] encoded = EncodeBoard.squareTo6(state, state.turn);
                double[] out = mn.eval(encoded);
                double res = out[0];
                if(res > bestResult) {
                    bestResult = res;
                    bestMove = move;
                }
            }
            return bestMove;
        }
        catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public BidirectionalTree<State> analysis(State state) {
        return null;
    }

    private String name = "MutatingNetwork(NET)";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int staticEval(State state, List<String> trace) throws Exception {
        throw new Exception("Ej implementerat i muterande nät-spelaren");
    }
*/
}
