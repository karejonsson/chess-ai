package chess.reuse;

import chess.board.Board;
import chess.operations.Getters;
import chess.board.State;
import chess.board.pieces.Piece;

public class EncodeBoard {

    public static final double[][] own = new double[][] {
        new double[] { 1.0, 0, 0, 0, 0, 0 }, // Pawn
        new double[] { 0, 1.0, 0, 0, 0, 0 }, // Bishop
        new double[] { 0, 0, 1.0, 0, 0, 0 }, // Knight
        new double[] { 0, 0, 0, 1.0, 0, 0 }, // Rook
        new double[] { 0, 0, 0, 0, 1.0, 0 }, // Queen
        new double[] { 0, 0, 0, 0, 0, 1.0 }, // King
    };

    public static final double[][] other = new double[][] {
        new double[] { -1.0, 0, 0, 0, 0, 0 }, // Pawn
        new double[] { 0, -1.0, 0, 0, 0, 0 }, // Bishop
        new double[] { 0, 0, -1.0, 0, 0, 0 }, // Knight
        new double[] { 0, 0, 0, -1.0, 0, 0 }, // Rook
        new double[] { 0, 0, 0, 0, -1.0, 0 }, // Queen
        new double[] { 0, 0, 0, 0, 0, -1.0 }, // King
    };

    public static final double[] empty = new double[] { 0, 0, 0, 0, 0, 0 };

    public static double[] squareTo6White(State state) {
        double[] out = new double[384];
        int ctr = 0;
        for(int r = 0; r < Board.ranks ; r++) {
            for(int f = 0; f < Board.files ; f++) {
                Piece p = Getters.getPiece(state, r, f);
                if(p == null) {
                    System.arraycopy(empty, 0, out, ctr*6, 6);
                }
                else if(p.getColor()) {
                    System.arraycopy(own[p.index()], 0, out, ctr*6, 6);
                }
                else {
                    System.arraycopy(other[p.index()], 0, out, ctr*6, 6);
                }
                ctr++;
            }
        }
        return out;
    }

    public static double[] squareTo6Black(State state) {
        double[] out = new double[384];
        int ctr = 0;
        for(int r = Board.ranks-1; r >= 0 ; r--) {
            for(int f = Board.files-1; f >= 0 ; f--) {
                Piece p = Getters.getPiece(state, r, f);
                if(p == null) {
                    System.arraycopy(empty, 0, out, ctr*6, 6);
                }
                else if(!p.getColor()) {
                    System.arraycopy(own[p.index()], 0, out, ctr*6, 6);
                }
                else {
                    System.arraycopy(other[p.index()], 0, out, ctr*6, 6);
                }
                ctr++;
            }
        }
        return out;
    }

    public static double[] squareTo6(State state, boolean player) {
        if(player) {
            return squareTo6White(state);
        }
        return squareTo6Black(state);
    }

}
