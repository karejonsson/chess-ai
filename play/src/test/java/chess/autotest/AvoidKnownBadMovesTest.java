package chess.autotest;

import chess.board.Match;
import chess.board.Move;
import chess.board.Player;
import chess.board.State;
import chess.debuganalysis.Reuse;
import chess.functions.WhoWantsStalemateFunctions;
import chess.functions.staticEvaluation.EvalMain;
import chess.operations.Serialization;
import chess.player.BruteForceDepthPlayerMinimax;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class AvoidKnownBadMovesTest {

    public static final byte levels = 2;

    public static String getMove(String pos) {
        State state = Serialization.getState(pos);
        Map<Integer, Byte> countReps = new HashMap<>();
        Player p =
                new BruteForceDepthPlayerMinimax(levels,
                        s -> EvalMain.evaluateBoardScore(s),
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);
        Match match = new Match(state, countReps);
        Move move = p.findMove(match);
        return move.toString();
    }

    public static boolean is(String pos, String move) {
        String m = getMove(pos).trim();
        System.out.println("Det erhållna draget var "+m);
        return move.equals(m);
    }

    @Test
    public void test01() {
        Assert.assertTrue(is(
                "______________K___B____P_P_____k_____Q__________________________:TTFTTTFTTF:H5H4F____:0110",
                "Bc3-e1+"));
    }

    @Test
    public void test02() {
        Assert.assertFalse(is(
                "R__QK__R_PP_BPPPP__P_________B____Nb______n_____ppp_ppppr_kq_b_r:TFFFFTFFFF:D7C8F____:0110",
                "Nc5*b7"));
    }

    @Test
    public void test03() {
        Assert.assertTrue(is(
                "_K______PP_____P_________B_______p_____Q_____k____R_____________:TTFTTTTTTF:A1B1F____:0110",
                "Bb4-c3+"));
    }

    @Test
    public void test04() {
        Assert.assertFalse(is(
                "_R___R_K__Q___PP__b__P____P____pP_______pBb________r_pp_k___rq__:FTFTTTTTFF:A2C2F____:0110",
                "Bc3-a1"));
    }

    @Test
    public void test05() {
        Assert.assertTrue(is(
                "______________PP_____________P__P___K________Q____R_______b_k___:TTTTTTTTFF:D7C8F____:0110",
                "Qf6-e7+"));
    }

    @Test
    public void test06() {
        Assert.assertFalse(is(
                "R__QK__R_P___PbPP__P______P__BB___N_______n_____ppp_ppppr_kq_b_r:FFFFFTFFFT:E2G4F____:0110",
                "f7-f5"));
    }

    @Test
    public void test07() {
        Assert.assertFalse(is(
                "R_BQKBNRPPPP_PPP__N_P______________p____________ppp_pppprnbqkbnr:FFFFFFFFFF:B1C3F____:0110",
                "Bc8-g4"));
    }

    @Test
    public void test08() {
        Assert.assertFalse(is(
                "RNB__RK_PPPPN_PP___BP__Q_n___P_____p________pq__ppp__pppr_b_kbnr:TTFFTFFFFF:C6B4F____:0110",
                "c2-c4"));
    }

    @Test
    public void test09() {
        Assert.assertFalse(is(
                "R_BQ_RK_PPPP_PPP_____N___n__P____B_Np______q____pppp_pppr_b_kbnr:FTFFTFFFFF:E1G1F_K__:0110",
                "Nb4*c2"));
    }

    @Test
    public void test10() {
        Assert.assertTrue(is(
                "______K__P____PP______P__P____p_R________knQ_______p______R_____:TTFTTTTTFF:A6B6F____:0110",
                "Qd6-c7+"));
    }

    @Test
    public void test11() {
        Assert.assertFalse(is(
                "R_B__RK_PPQP_PPP_____N______P____B_Np______q____pppp_pppr_b_kbnr:FTFFTFFFFF:D1C2F____:0110",
                "Ng8-e7"));
    }

    //@Test
    public void test12() {
        // Kanske kan göra något åt att låsas av löparen
        Assert.assertFalse(is(
                "__R__RK__P___PPPP_________Q____B_B__N____p_q___pp__pn_p__k___b_r:FTFTTTFTFF:F3E5F____:0110",
                "Qd6*e5"));
    }

    //@Test
    public void test13() {
        // Springargaffel kan beräknas. Hotar man flera av högre värde är det bra!
        Assert.assertFalse(is(
                "___R_RK_PPP___P___N_________P_nP____P_p_p_p______pp__pp___k__r_r:TTFTTTFFTF:F6G4F____:0110",
                "h4*g5"));
    }

    //@Test
    public void test14() {
        Assert.assertFalse(is(
                "__KR__NRPP____BP________p__P_Bp______P_p__p_Nn___p_np_p__rb_kb_r:FTFFTFFTFF:E3F4F____:0110",
                "b7-b5"));
        // Svart låter vit ta ett torn på B8 som kan flyttas iväg.
    }

}
