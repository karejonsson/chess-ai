package chess.debuganalysis;

import chess.functions.staticEvaluation.EvalMain;

public class ShouldSaveBishop {

    public static void main(String[] args) {
        Reuse.calculateOneMove(
                "R__QK_NRPPP__PPP__NB____p___P_____rP__B________p_pppppp__nbqkbnr:TFFFFFFTFF:H7H6F____",
                s -> EvalMain.evaluateBoardScore(s),
                2);
    }

    /*
    2 : Sc3*a4
    3 : Dd1-d2
    4 : Dd1-f3 (efter 20 minuter minst)
     */

}
