package chess.debuganalysis;

import chess.functions.Measures;

public class TestingPlace {

    public static void main(String[] args) {
        Reuse.calculateOneMove(
                "R_B_KB_RPPQPPPPPN_P____Np________p________ppb_______pppprn_qkbnr:TFFTFFFFFF:D5E6F____",
                s -> Measures.whitesPieceAdvantage(s),
                2);
    }

}

/*
R_B_KB_R__QPPPPPNPP__________N___pp________p____q__bpppprn__kbnr:FFFTFFFFFF:B1A1F____
R_B_KB_R__QPPPPPNPP__________N__qpp________p_______bpppprn__kbnr:TFFTFFFFFF:A7A5F____
R_B_KB_R__QPPPPP_PP__________N__qNp________p_______bpppprn__kbnr:FFFTFFFFFF:A3B5F____
q_B_KB_R__QPPPPP_PP__________N___Np________p_______bpppprn__kbnr:TFFTFFFFFF:A5A1F____

 */