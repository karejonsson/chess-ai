package chess.debuganalysis;

import chess.functions.Measures;

public class WhyBishopSacrifice {

    public static void main(String[] args) {
        Reuse.calculateOneMove(
                "R_Q_KBNRPP_BPPPPN_PP____p________p_b______pp________pppprn_qkbnr:FFFTFFFFFF:B1C1F____",
                s -> Measures.whitesPieceAdvantage(s),
                2);
    }

}
