package chess.debuganalysis;

import chess.board.*;
import chess.execution.Sequencing;
import chess.functions.StaticStateEvaluation;
import chess.functions.WhoWantsStalemateFunctions;
import chess.gui.AnalysisTableOfMoves;
import chess.gui.MatchIllustration;
import chess.operations.Serialization;
import chess.player.BruteForceDepthPlayerMinimax;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reuse {

    public static void calculateOneMove(String position, StaticStateEvaluation eval, int levels) {
        State state = Serialization.getState(position);
        Map<Integer, Byte> countReps = new HashMap<>();
        Player p =
                new BruteForceDepthPlayerMinimax((byte)levels, eval,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);
        Match match = new Match(state, countReps);
        calculateOneMove(match, p);
    }

    public static void calculateOneMove(Match match, Player p) {
        final MatchIllustration canvas = new MatchIllustration(match, 2.0);
        final JFrame f = new JFrame("Schackbräde");
        f.setLocation(0, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

        Move m = p.findMove(match);
        System.out.println(m.toString());
    }

    public static void initByProtocol(byte depth, StaticStateEvaluation se1, StaticStateEvaluation se2, String protocolFile) throws IOException {
        Map<Integer, Byte> countReps = new HashMap<>();
        Player p1 =
                new BruteForceDepthPlayerMinimax(depth, se1,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);
        Player p2 =
                new BruteForceDepthPlayerMinimax(depth, se2,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);


        MatchNotifying mn = new MatchNotifying(p1, p2);
        List<State> list = MatchNotifying.readList(new FileInputStream(protocolFile));
        list.remove(0);

        AnalysisTableOfMoves atom = new AnalysisTableOfMoves(mn);
        for(State s : list) {
            atom.addMove(s);
        }
    }

    public static void runVisuallyFromPosition(byte depth, StaticStateEvaluation se1, StaticStateEvaluation se2, String position) {
        Map<Integer, Byte> countReps = new HashMap<>();
        Player pw =
                new BruteForceDepthPlayerMinimax(depth, se1,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);
        Player pb =
                new BruteForceDepthPlayerMinimax(depth, se2,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);

        meet(pw, pb, position);
    }

    public static void meet(StaticStateEvaluation se1, StaticStateEvaluation se2, int depth) {
        Map<Integer, Byte> countReps = new HashMap<>();
        Player p1 =
                new BruteForceDepthPlayerMinimax((byte)(depth), se1,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);
        Player p2 =
                new BruteForceDepthPlayerMinimax((byte)(depth), se2,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);
        Reuse.meet(p1, p2);
    }

    public static void meet(Player pw, Player pb, String position) {
        State state = Serialization.getState(position);
        MatchNotifying board = new MatchNotifying();
        board.init(state);
        meet(pw, pb, board);
    }

    public static void meet(Player pw, Player pb) {
        MatchNotifying board = new MatchNotifying();
        board.init();
        meet(pw, pb, board);
    }

    public static void meet(Player pw, Player pb, MatchNotifying board) {
        final MatchIllustration canvas = new MatchIllustration(board, 3.0);
        final JFrame f = new JFrame("Schackbräde");
        f.setLocation(0, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

        AnalysisTableOfMoves protocol = new AnalysisTableOfMoves(board);

        board.surveillor.addMoveListener(protocol, m -> {
            protocol.addMove(m.state);
            canvas.repaint();
            //System.out.println(Serialization.getStateString(m.state));
        });

        Thread t = new Thread(() -> {
            try {
                Sequencing.runSingleThread(board, pw, pb);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        });
        t.start();
    }

}
