package chess.debuganalysis;

import chess.functions.Measures;

public class WhiteLeavesPawn {

    public static void main(String[] args) {
        Reuse.calculateOneMove(
                "_RB_KB_RPPQPPPPPN______NpP________p_p______pb________ppprn_qkbnr:TFFTFFFFFF:C6C5F____",
                s -> Measures.whitesPieceAdvantage(s),
                2);
    }

}
