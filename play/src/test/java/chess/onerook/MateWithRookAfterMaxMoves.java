package chess.onerook;

import chess.board.MatchNotifying;
import chess.board.Move;
import chess.operations.Moving;
import chess.board.State;
import chess.operations.Initiation;
import chess.execution.CompetersEvaluator;
import chess.execution.Sequencing;
import chess.board.Player;
import chess.gui.MatchIllustration;
import chess.gui.TableOfMoves;

import java.util.List;

public class MateWithRookAfterMaxMoves /* implements CompetersEvaluator */ {

    /*
    private MatchNotifying board;
    private TableOfMoves protocol;
    private MatchIllustration canvas;
    private int maxMoves = 0;

    public MateWithRookAfterMaxMoves(MatchNotifying board, TableOfMoves protocol, MatchIllustration canvas, int maxMoves) {
        this.board = board;
        this.protocol = protocol;
        this.canvas = canvas;
        this.maxMoves = maxMoves;
    }

    @Override
    public void init() {
        board.init(OneRookMateing.create());
        if(protocol != null) {
            protocol.init();
        }
    }

    @Override
    public void init(State state) {
        board.init(state);
        if(protocol != null) {
            protocol.init();
        }
    }

    public State move(Move m) {
        //System.out.println("PiecesAheadWins.move: m="+m);
        State out = Moving.attempted(board.state, m, board.countRepetitions);
        if(protocol != null) {
            protocol.addMove(out);
        }
        return out;
    }

    public List<Move> getMoves() {
        return board.state.getLegalMoves();
    }

    @Override
    public State getState() {
        return Initiation.clone(board.state);
    }

    @Override
    public Boolean eval(Player white, Player black, String descriptionOfState) throws Exception {
        board.surveillor.onWhite(white.getName());
        board.surveillor.onBlack(black.getName());
        board.surveillor.onGame(descriptionOfState);
        Sequencing.runSingleThreadCounted(
            board,
            white,
            black,
            maxMoves
        );
        return board.winner();
    }
*/
}
