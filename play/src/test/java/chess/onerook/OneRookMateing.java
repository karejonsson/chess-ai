package chess.onerook;

import chess.board.*;
import chess.gui.MatchIllustration;
import chess.gui.TableOfMoves;
import chess.board.pieces.*;
import chess.gui.paint.Painter;
import chess.originalgame.setup.FindBest;
import chess.wrappers.ResultWrapper;

import java.awt.*;

public class OneRookMateing {
/*
    public static void setupBoardDefaultPositions(Piece[][] contents) {
        int i;
        for (int rank = 0; rank < Board.ranks ; rank++) {
            for (int file = 0; file < Board.files ; file ++) {
                contents[rank][file] = null;
            }
        }
        contents[0][0] = new Rook(Board.WHITE);
        contents[0][Board.kingFile] = new King(Board.WHITE);
        contents[Board.ranks-1][Board.kingFile] = new King(Board.BLACK);

    }

    public static State create() {
        State state = new State();
        state.board = new Piece[Board.ranks][Board.files]; // Sammanblandning?
        setupBoardDefaultPositions(state.board);
        state.turn = Board.WHITE;
        state.whiteKing = new Square((byte)(0), (byte)(Board.kingFile));
        state.blackKing = new Square((byte)(Board.ranks-1), (byte)(Board.kingFile));

        return state;
    }

    public static void main(String[] args) throws Exception {
        TableOfMoves protocol = null;//TableOfMoves.get();
        //protocol.init();
        MatchNotifying board = new MatchNotifying();
        board.init(create());

        MatchIllustration canvas = new MatchIllustration(board, 1.0);

        if(canvas != null) {
            Frame f = new Frame("Schackbräde");
            f.setLocation(500, 0);
            f.add(canvas);
            f.pack();
            f.setVisible(true);
        }

        MateWithRookAfterMaxMoves player = new MateWithRookAfterMaxMoves(board, protocol, canvas, 140);

        int nr = 1;
        for(int i = 0 ; i < 10000 ; i++) {
            ResultWrapper res = FindBest.runOneSide(
                    nr,
                    player,
                    "tmp/sor<NR>.ser",
                    rw -> rw.challenger - rw.master >= 1.0);
            if(i % 10 == 0) {
                System.out.println("i="+i+", nr="+nr+", ");
            }
            if(res == null) {
                //System.out.println("Oavgjort genom NULL "+(new Date()));
                continue;
            }
            if(res.challenger > res.master) {
                nr++;
                System.out.println("Ny bästa "+nr+" på varv "+i+" med sittande "+res.master+", mutationen "+res.challenger);
                continue;
            }
            if(res.challenger < res.master) {
                System.out.println("Mästaren sitter kvar med "+res.master+", mutationen "+res.challenger);
                continue;
            }
            System.out.println("Oavgjort");
        }
    }
*/
}
