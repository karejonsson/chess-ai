package chess.permute.rewards;

import chess.board.Board;
import chess.board.MatchNotifying;
import chess.board.Player;
import chess.execution.PiecesAheadAfterMaxMovesWins;
import chess.functions.StaticStateEvaluation;
import chess.functions.WhoWantsStalemateFunctions;
import chess.functions.staticEvaluation.EvalMain;
import chess.functions.staticEvaluation.Weights;
import chess.functions.staticEvaluation.positional.DownloadedPositionRewarder;
import chess.functions.staticEvaluation.positional.FormulaPositionRewarder;
import chess.gui.TableOfMoves;
import chess.originalgame.setup.Permutation;
import chess.player.BruteForceDepthPlayerMinimax;
import chess.wrappers.ResultWrapper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Attempt1 {

    public static Weights getDownloaded() {
        Weights out = Weights.aws[Board.chessStyle].copy();
        out.positionRewarder = new DownloadedPositionRewarder();
        //out.pieceValues[Rook.idx] *= d;
        //out.someStability *= d;
        //out.someImmobilisation *= d;
        //out.mayNotBeSaved *= d;
        //out.multipleStrike *= d;
        return out;
    }
    // 129, 112, 116, 66, 45, 132
    public static short pawn = 129;
    public static short knight = 112;
    public static short bishop = 116;
    public static short rook = 66;
    public static short queen = 45;
    public static short king = 132;

    public static short[] weights = new short[] { pawn, knight, bishop, rook, queen, king };

    public static Weights getImproved() {
        Weights out = Weights.aws[Board.chessStyle].copy();
        out.positionRewarder = new FormulaPositionRewarder(new short[] { pawn, knight, bishop, rook, queen, king } );
        return out;
    }

    //public static final double[] facts = new double[] {-0.5, -0.35, -0.2, -0.1, 0, 0.05, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.65, 0.80, 1.0, 1.2, 1.4, 1.7, 2.0,2.5, 3.0};
    //public static final double[] facts = new double[] { 0.6, 0.7, 0.85, 1.0, 1.15, 1.3, 1.5, 1.7};
    //public static final double[] facts = new double[] { 0.87, 0.92, 0.96, 1.01, 1.05, 1.1, 1.17};
    public static final double[] facts = new double[] { 0.8, 0.9, 1.0, 1.1, 1.2};
    //public static final double[] facts = new double[] { 0.76, 0.83, 0.87, 0.92, 0.96, 1.01, 1.05, 1.1, 1.17, 1.26};
    //public static final double[] facts = new double[] { 0.83, 0.86, 0.89, 0.92, 0.94, 0.96, 0.98, 1.0, 1.03 };
    //public static final double[] facts = new double[] { 0.96, 0.98, 1.0, 1.03, 1.06, 1.09, 1.12 };

    public static void main(String[] args) throws Exception {


        MatchNotifying board = new MatchNotifying();
        board.init();

        TableOfMoves protocol = new TableOfMoves(board.surveillor);
        protocol.init();

        /*
        for(int i = 0 ; i < facts.length-1 ; i++) {
            for(int j = i+1 ; j < facts.length ; j++) {
                final Weights sw = getDownloaded(facts[i]);
                final Weights gw = getImproved(facts[j]);
                StaticStateEvaluation ses = state -> EvalMain.evaluateBoardScore(state, sw);
                StaticStateEvaluation seg = state -> EvalMain.evaluateBoardScore(state, gw);
                System.out.print(
                        //"----------------------------------\n"+
                        "s="+facts[i]+", g="+facts[j]+" "
                );
                test(ses, seg, protocol, board);
            }
        }
         */

        /*
        for(int i = 0 ; i < facts.length ; i++) {
            final Weights sw = getDownloaded();
            final Weights gw = getImproved();
            StaticStateEvaluation ses = state -> EvalMain.evaluateBoardScore(state, sw);
            StaticStateEvaluation seg = state -> EvalMain.evaluateBoardScore(state, gw);
            System.out.print(
                    //"----------------------------------\n"+
                    "faktor="+facts[i]+" "//+", g="+facts[j]+" "
            );
            test(ses, seg, protocol, board);
        }
        */

        byte rot = 2;

        for(int i = 0 ; i < 100 ; i++) {
            rot++;
            rot %= Board.pieces;
            short current = weights[rot];
            double best = -0.1;
            int bestIdx = 0;
            for(int idx = 0 ; idx < facts.length ; idx++) {
                short candidate = (short)(current*facts[idx]);
                weights[rot] = candidate;

                final Weights sw = getDownloaded();
                final Weights gw = getImproved();
                StaticStateEvaluation ses = state -> EvalMain.evaluateBoardScore(state, sw);
                StaticStateEvaluation seg = state -> EvalMain.evaluateBoardScore(state, gw);

                double won = test(ses, seg, protocol, board);
                if(won > best) {
                    bestIdx = idx;
                    best = won;
                    System.out.println("Förbättring "+ Arrays.toString(weights)+" -> "+won);
                }
                else {
                    System.out.println("Försämring "+ Arrays.toString(weights)+" -> "+won);
                }
            }
            short candidate = (short)(current*facts[bestIdx]);
            weights[rot] = candidate;
            System.out.println("#### Bästa "+ Arrays.toString(weights)+" -> "+best+", från varv "+i+" och pjäsindex "+rot);
        }

    }

    public static double test(StaticStateEvaluation ses, StaticStateEvaluation seg, TableOfMoves protocol, MatchNotifying board) throws Exception {

        Map<Integer, Byte> countReps = new HashMap<>();

        Player pw =
                new BruteForceDepthPlayerMinimax((byte)1, ses,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);

        Player pb =
                new BruteForceDepthPlayerMinimax((byte)1, seg,
                        s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                        countReps);

        PiecesAheadAfterMaxMovesWins player = new PiecesAheadAfterMaxMovesWins(board, protocol, 180);

        ResultWrapper res = Permutation.permuteForGeneralBestTwoSides(pw, pb, player);

        //System.out.println(res.toString());
        return res.challenger/40;
    }

}
