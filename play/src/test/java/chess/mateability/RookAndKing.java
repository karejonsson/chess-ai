package chess.mateability;

import chess.debuganalysis.Reuse;
import chess.functions.staticEvaluation.EvalMain;

public class RookAndKing {

    public static final String pos = "R____K_________________________k________________________________:TTFTTTFTTF:H5H4F____";

    public static void main(String[] args) {

        Reuse.runVisuallyFromPosition((byte)5, s -> EvalMain.evaluateBoardScore(s),
                s -> EvalMain.evaluateBoardScore(s), pos);


        //Reuse.calculateOneMove(pos2, s -> AmbitiousFunction.evaluateBoardScore(s), 3);
    }

}
