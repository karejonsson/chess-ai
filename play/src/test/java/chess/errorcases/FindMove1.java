package chess.errorcases;

import chess.debuganalysis.Reuse;
import chess.functions.staticEvaluation.EvalMain;

public class FindMove1 {

    // Varför hittar den inte g7-g5? Konstaterat med djup 2&3.
    public static final String pos1 = "R__Q_RK_PPPB_PPP_B__P______P_N_q___p______nbp___pppb_pppr____rk_:FTFFTTFFTF:E2F4F____";

    // Varför hittar den inte Dg4 eller Le1 matt? Konstaterat med djup 2.
    public static final String pos2 = "______________K___B____P_P_____k_____Q__________________________:TTFTTTFTTF:H5H4F____";

    public static void main(String[] args) {

        Reuse.runVisuallyFromPosition((byte)3, s -> EvalMain.evaluateBoardScore(s),
                s -> EvalMain.evaluateBoardScore(s), pos1);


        //Reuse.calculateOneMove(pos2, s -> AmbitiousFunction.evaluateBoardScore(s), 3);
    }

}
