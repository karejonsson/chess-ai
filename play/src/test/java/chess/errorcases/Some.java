package chess.errorcases;

import chess.board.State;
import chess.functions.staticEvaluation.EvalMain;
import chess.gui.MatchIllustration;
import chess.operations.Serialization;

import javax.swing.*;
import java.util.ArrayList;

public class Some {
    /*
    _K_______p____P_________________________________k________Q______:FTTTFTTTFT:E8B8F____
    Genereras öht rätt drag. Kolla speciellt fel spelare
    Svar: För fel spelare genereras inte de drag som lämnar rätt spelare i schack.
     */

    /*
     _K_______p____P_________________________________k________Q______:FTTTFTTTFT:E8B8F____
     Svart borde få värde av att damen står i slag för kungen

     Då borde vit undvika att sätta bort damen ifrån
     _K_______p____P_________________________________k___________Q___:TTTTFTTTFF:B8A7F____
     */

    /*
    RNBQK_NRPPP__PPP___________Pp_____________B_____ppp_ppppr_bqkbnr:FFFFFFFFFT:B5C6F____
    Svart borde få värde i bonden på B7 eftersom den kan slå löparen.

    Då kanske vit inte slår springaren med löparen ifrån
    RNBQK_NRPPP__PPP___________Pp____B________n_____ppp_ppppr_bqkbnr:TFFFFFFFFF:B8C6F____
     */

    /*
    RNBQKBNRPPP__PPP___________PP______p____________ppp_pppprnbqkbnr:FFFFFFFFFF:E2E4F____
    Svarts bonde på D5 borde få värde av att den kan slå vits E4.

    Då borde inte vit välja E2-E4 ifrån
    RNBQKBNRPPP_PPPP___________P_______p____________ppp_pppprnbqkbnr:TFFFFFFFFF:D7D5F____
     */

    //public static final String pos = "_K_______p____P_________________________________k________Q______:FTTTFTTTFT:E8B8F____";
    //public static final String pos = "RNB_K_NRPbPP_PPP___B____________________________pppp_p_prnbqk___:TFFFFFTFFF:H8B2F____";
    public static final String pos = "RNB_KBNRPPPP_PPP_____Q______P________________n__pppppppprnbqkb_r:FFFFFFFFFF:D1F3F____";

    public static void main(String[] args) {
        State state = Serialization.getState(pos);
        state.prepareForStaticAnalysis();

        final MatchIllustration canvas = new MatchIllustration(state, 1.5);
        final JFrame f = new JFrame("Schackbräde");
        f.setLocation(0, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

        /*
        System.out.println("Rätt spelare");
        for(Move m : state.getLegalMoves()) {
            System.out.println(m.toString());
        }
        System.out.println("Motståndaren");
        for(Move m : state.getOpponentsMoves()) {
            System.out.println(m.toString());
        }
         */
        ArrayList<String> trace = new ArrayList<>();
        EvalMain.evaluateBoardScore(state, trace);
        for(String s : trace) {
            System.out.println(s);
        }
        System.out.println("Annars "+ EvalMain.evaluateBoardScore(state));

    }

}
