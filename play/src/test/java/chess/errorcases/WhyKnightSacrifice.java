package chess.errorcases;

import chess.debuganalysis.Reuse;
import chess.functions.staticEvaluation.EvalMain;

public class WhyKnightSacrifice {

    public static final String pos = "R__QK__R_PP_BPPPP__P_________B____Nb______n_____ppp_ppppr_kq_b_r:TFFFFTFFFF:D7C8F____";

    public static void main(String[] args) {
        Reuse.runVisuallyFromPosition((byte)2, s -> EvalMain.evaluateBoardScore(s),
                s -> EvalMain.evaluateBoardScore(s), pos);
    }

}
