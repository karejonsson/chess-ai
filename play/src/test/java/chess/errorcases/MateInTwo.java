package chess.errorcases;

import chess.debuganalysis.Reuse;
import chess.functions.staticEvaluation.EvalMain;

public class MateInTwo {

    // Vit borde hitta en matt på två. Lc3+, Ke6, Dd5
    public static final String pos = "_K______PP_____P_________B_______p_____Q_____k____R_____________:TTFTTTTTTF:A1B1F____";

    public static void main(String[] args) {

        Reuse.runVisuallyFromPosition((byte)2, s -> EvalMain.evaluateBoardScore(s),
                s -> EvalMain.evaluateBoardScore(s), pos);
    }

}
