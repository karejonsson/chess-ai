package chess.errorcases;

import chess.debuganalysis.Reuse;
import chess.functions.staticEvaluation.EvalMain;

public class WhyMoveKing {

    public static final String pos = "R__QKB_R_PP_PPPPP_NP_N_______B____________npbn__ppp_ppppr__qkb_r:FFFFFFFFFF:G1F3F____";

    public static void main(String[] args) {
        Reuse.runVisuallyFromPosition((byte)2, s -> EvalMain.evaluateBoardScore(s),
                s -> EvalMain.evaluateBoardScore(s), pos);
    }

}
