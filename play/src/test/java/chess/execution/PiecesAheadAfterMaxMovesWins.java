package chess.execution;

import chess.board.MatchNotifying;
import chess.board.Move;
import chess.operations.Moving;
import chess.board.State;
import chess.operations.Initiation;
import chess.board.Player;
import chess.gui.TableOfMoves;

import java.util.List;

public class PiecesAheadAfterMaxMovesWins implements CompetersEvaluator {

    private MatchNotifying board;
    private TableOfMoves protocol;
    private int maxMoves = 0;

    public PiecesAheadAfterMaxMovesWins(MatchNotifying board, TableOfMoves protocol, int maxMoves) {
        this.board = board;
        this.protocol = protocol;
        this.maxMoves = maxMoves;
    }

    public State move(Move m) {
        State out = Moving.attempted(board.state, m, board.countRepetitions);
        if(protocol != null) {
            protocol.addMove(out);
        }
        return out;
    }

    public List<Move> getMoves() {
        return board.state.getLegalMoves();
    }

    @Override
    public State getState() {
        return Initiation.clone(board.state);
    }

    @Override
    public void init() {
        board.init();
        if(protocol != null) {
            protocol.init();
        }
    }

    @Override
    public void init(State state) {
        board.init(state);
        if(protocol != null) {
            protocol.init();
        }
    }

    @Override
    public Boolean eval(Player white, Player black, String descriptionOfState) throws Exception {
        board.surveillor.onWhite(white.getName());
        board.surveillor.onBlack(black.getName());
        board.surveillor.onGame(descriptionOfState);
        board.setPlayerW(white);
        board.setPlayerB(black);
        Sequencing.runSingleThreadCountedPiecesAheadWins(
                board,
                white,
                black,
                maxMoves
        );
        return board.winner();
    }

}
