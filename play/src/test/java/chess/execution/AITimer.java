package chess.execution;

/* This timer sends a signal to the AI to play */
public class AITimer extends Thread {

    private ChessAsDownloaded parent;

    public AITimer(ChessAsDownloaded c) {
        parent = c;
    }

    public void run() {
        while (true) {
            try {
                // delay two seconds
                sleep(1);
            } catch (InterruptedException e) {
                // No need to catch it
                System.out.println(e.toString());
            } finally {
                //parent.timerHandler();
            }
        }
    }

}
