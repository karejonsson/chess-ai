package chess.execution;

import chess.board.MatchNotifying;
import chess.board.Move;
import chess.player.BruteForceDepthPlayerAsDownloaded;
import chess.player.RandomPlayer;
import chess.board.Player;
import chess.gui.MatchIllustration;
import chess.gui.TableOfMoves;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class StrategiesExternalized extends JPanel {

    private final MatchNotifying board = new MatchNotifying();
    private Canvas canvas = null;
    private TableOfMoves protocol = null;
    private Button btnNewGame;
    private Button btnTakeBack;
    private Choice blackCh;
    private Choice whiteCh;
    private Player blackPlayer = null;
    private Player whitePlayer = null;

    /* For getting promotions */
    private Move tempMove;
    public Label messageWhiteLbl;
    public Label messageBlackLbl;
    public Label messageGameLbl;

    public StrategiesExternalized() {
    }

    public void init() {
        setBackground(Color.gray);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        btnNewGame = new Button("Start");
        add(btnNewGame);
        btnNewGame.addActionListener(e -> this.newGame());

        whiteCh = new Choice();
        whiteCh.add("Random");
        whiteCh.add("1 move");
        whiteCh.add("2 moves");
        whiteCh.add("3 moves");
        whiteCh.add("4 moves");
        whiteCh.add("5 moves");
        //whiteCh.setForeground(Color.white);
        whiteCh.setBackground(Color.gray);

        blackCh = new Choice();
        blackCh.add("Random");
        blackCh.add("1 move");
        blackCh.add("2 moves");
        blackCh.add("3 moves");
        blackCh.add("4 moves");
        blackCh.add("5 moves");
        //blackCh.setForeground(Color.black);
        blackCh.setBackground(Color.gray);

        add(whiteCh);
        add(blackCh);

        messageWhiteLbl = new Label(".");
        messageBlackLbl = new Label(".");
        messageGameLbl = new Label("Ready to play");

        add(messageWhiteLbl);
        add(messageBlackLbl);
        add(messageGameLbl);

        board.surveillor.addWhiteListener(s -> messageWhiteLbl.setText(s));
        board.surveillor.addBlackListener(s -> messageBlackLbl.setText(s));
        board.surveillor.addGameListener(s -> messageGameLbl.setText(s));
        board.surveillor.addEndListener(() -> System.out.println("----------------"));

        messageWhiteLbl.setBounds(30, 320, 0, 0);
        messageBlackLbl.setBounds(30, 320, 0, 0);
        messageGameLbl.setBounds(30, 320, 0, 0);

        messageWhiteLbl.setSize(120, 20);
        messageBlackLbl.setSize(120, 20);
        messageGameLbl.setSize(120, 20);
    }

    private boolean executing = false;

    public void newGame() {
        if(executing) {
            return;
        }
        executing = true;
        if(canvas == null) {
            canvas = new MatchIllustration(board, 1.0);
            board.surveillor.addMoveListener(canvas, m -> canvas.repaint());
            Frame f = new Frame("Schackbräde");
            //f.addWindowListener(e -> { });
            //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setLocation(500, 0);
            f.setSize(800, 800);
            f.add(canvas);
            f.pack();
            f.setVisible(true);
        }

        if(protocol == null) {
            protocol = new TableOfMoves(board.surveillor);
        }

        messageGameLbl.setText("");
        repaint();
        int wPos = whiteCh.getSelectedIndex();
        int bPos = blackCh.getSelectedIndex();

        Map<Integer, Byte> countReps = new HashMap<>();

        whitePlayer = wPos == 0 ? new RandomPlayer() : new BruteForceDepthPlayerAsDownloaded((byte)(wPos-1), countReps);
        blackPlayer = bPos == 0 ? new RandomPlayer() : new BruteForceDepthPlayerAsDownloaded((byte)(bPos-1), countReps);

        board.setPlayerW(whitePlayer);
        board.setPlayerB(blackPlayer);

        board.init();

        Thread t = new Thread(() -> {
            Sequencing.runSingleThread(
                    board,
                    whitePlayer,
                    blackPlayer
            );
            protocol.repaint();
            executing = false;
        });
        t.start();

        /*
        Sequencing.runWithTimers(
            board,
            whitePlayer,
            blackPlayer
        );
         */
    }

    public static void main(String[] args) {
        Frame f = new Frame("GUI program");
        f.setLocation(0, 0);
        f.setSize(160, 190);
        StrategiesExternalized p = new StrategiesExternalized();
        p.init();
        f.add(p);
        f.setVisible(true);
    }

}
