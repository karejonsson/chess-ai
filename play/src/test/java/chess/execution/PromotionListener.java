package chess.execution;

import chess.execution.ChessAsDownloaded;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PromotionListener implements KeyListener {
    ChessAsDownloaded parent;

    public PromotionListener(ChessAsDownloaded c) {
        parent = c;
    }

    public void keyTyped(KeyEvent e) {
        char c = Character.toUpperCase(e.getKeyChar());
        parent.messageLbl.setText("typed: " + c);
        if (c == 'Q' || c == 'N' || c == 'R' || c == 'B') {
            parent.canvas.promotionHandler(c);
        }
    }

    /* Unused KeyListener methods */
    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

}
