package chess.execution;

import chess.board.*;
import chess.operations.Moving;
import chess.functions.BruteForceFunctions;
import chess.gui.MatchGUIMoving;
import chess.gui.paint.PainterForWhiteDown;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChessAsDownloaded extends JPanel {

    //public static final boolean WHITE = true;
    //public static final boolean BLACK = false;
    private MatchNotifying board = new MatchNotifying();
    public MatchGUIMoving canvas;
    private JPanel controllers = null;
    private Button btnNewGame;
    private Button btnTakeBack;
    private Choice blackCh;
    private Choice whiteCh;
    private Map<Integer, Byte> countReps = new HashMap<>();
    //private AITimer timer;

    /* used to make sure the timer doesn't start
       two threads for the same move */
    private boolean thinking = false;

    /* For getting promotions */
    private Move tempMove;
    public Label messageLbl;
    private boolean isPromoting;

    public ChessAsDownloaded() {
        board.surveillor.addGameListener(s -> messageLbl.setText(s));
        board.surveillor.addMoveListener(this, m -> {
            System.out.println("On any move "+m);
            canvas.repaint();
        });
        board.surveillor.addWhiteMoveListener(this, m -> {
            System.out.println("On white move "+m);
        });
        board.surveillor.addBlackMoveListener(this, m -> {
            System.out.println("On black move "+m);
        });
        board.surveillor.addWhiteToMoveListener(this, () -> {
            System.out.println("On white to move ");
        });
        board.surveillor.addBlackToMoveListener(this, () -> {
            System.out.println("On black to move ");
        });
    }

    public void init() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        canvas = new MatchGUIMoving(board, new PainterForWhiteDown());
        add(canvas);

        this.setBackground(Color.red);
        canvas.repaint();

        controllers = new JPanel();
        controllers.setLayout(new BoxLayout(controllers, BoxLayout.Y_AXIS));
        add(controllers);

        btnNewGame = new Button("New Game");
        controllers.add(btnNewGame);
        btnNewGame.addActionListener(e -> this.newGame());

        btnTakeBack = new Button("Take Back");
        controllers.add(btnTakeBack);
        btnTakeBack.addActionListener(e -> this.takeBack());

        whiteCh = new Choice();
        whiteCh.add("Human");
        whiteCh.add("Random");
        whiteCh.add("1 move");
        whiteCh.add("2 moves");
        whiteCh.add("3 moves");
        whiteCh.add("4 moves");
        whiteCh.add("5 moves");
        //whiteCh.setForeground(Color.white);
        whiteCh.setBackground(Color.gray);

        blackCh = new Choice();
        blackCh.add("Human");
        blackCh.add("Random");
        blackCh.add("1 move");
        blackCh.add("2 moves");
        blackCh.add("3 moves");
        blackCh.add("4 moves");
        blackCh.add("5 moves");
        //blackCh.setForeground(Color.black);
        blackCh.setBackground(Color.gray);

        controllers.add(whiteCh);
        controllers.add(blackCh);

        whiteCh.addItemListener(e -> this.choiceHandler());
        blackCh.addItemListener(e -> this.choiceHandler());

        messageLbl = new Label("Ready to play");

        controllers.add(messageLbl);
        messageLbl.setBounds(30,320,0,0);

        messageLbl.setSize(120,20);

        canvas.addKeyListener(new PromotionListener(this));
        addKeyListener(new PromotionListener(this));

        /*
        timer = new AITimer(this, (w, b, g) -> messageLbl.setText(g));
        timer.start();
        timer.suspend();

         */
        newGame();
    }

    public void newGame() {
        board.init();
        messageLbl.setText("");
        repaint();
        canvas.repaint();
        board.surveillor.reset();

    }

    public void takeBack() {
        board.takeBackMove();
        board.takeBackMove();
        board.readyNextMove();
    }

    public void choiceHandler() {
        board.surveillor.removeWhiteToMoveListener(this);
        board.surveillor.removeBlackToMoveListener(this);
        if(whiteCh.getSelectedItem().equals("Human")) {
            board.surveillor.addWhiteToMoveListener(this, () -> {
                System.out.println("On white to move ");
            });
        }
        else {
            board.surveillor.addWhiteToMoveListener(this, () -> {
                playAI();
            });
            if(board.state.turn && board.continues()) {
                playAI();
            }
        }
        if(blackCh.getSelectedItem().equals("Human")) {
            board.surveillor.addBlackToMoveListener(this, () -> {
                System.out.println("On black to move ");
            });
        }
        else {
            board.surveillor.addBlackToMoveListener(this, () -> {
                playAI();
            });
            if(!board.state.turn && board.continues()) {
                playAI();
            }
        }

        /*
        System.out.println("ChessAsDownloaded.choiceHandler");
        if (isHuman(WHITE) && isHuman(BLACK)) {
            System.out.println("ChessAsDownloaded.choiceHandler suspend");
            timer.suspend();
        }
        else {
            System.out.println("ChessAsDownloaded.choiceHandler resume");
            timer.resume();
        }

         */
    }

    /* Once it is determined that is time for the AI to make a move */
    private void playAI() {
        thinking = true;
        System.out.println("ChessAsDownloaded.playAI");
        //ArrayList<Move> moves = board.getAllMoves();
        ArrayList<Move> v = null;
        // Random Play
        if ((board.state.turn && whiteCh.getSelectedItem().equals("Random"))
                || (!board.state.turn && blackCh.getSelectedItem().equals("Random")) ) {
            v = board.state.getLegalMoves();
        }
        // Search moves to depth
        else if (board.state.turn && whiteCh.getSelectedIndex() > 1) {
            v = BruteForceFunctions.getBestMoves(board.state, (byte)(whiteCh.getSelectedIndex() - 2), countReps);
            //v = board.getBestMoves(whiteCh.getSelectedIndex() - 2);
        }
        else if (!board.state.turn && blackCh.getSelectedIndex() > 1) {
            v = BruteForceFunctions.getBestMoves(board.state, (byte)(blackCh.getSelectedIndex() - 2), countReps);
            //v = board.getBestMoves(blackCh.getSelectedIndex()-2);
        }
        else {
            return;
        }

        Move m = v.get((int)(Math.random()*v.size()));
        /* Automatic promotion to queen */

        boolean mover = board.state.turn;
        Moving.real(board, m);
        board.readyNextMove();
        thinking = false;
    }

    public static void main(String[] args) {
        Frame f = new Frame("GUI program");
        //f.addWindowListener(e -> { });
        //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLocation(30,30);
        f.setSize(800, 800);
        ChessAsDownloaded p = new ChessAsDownloaded();
        p.init();
        f.add(p);
        f.pack();
        f.setVisible(true);
    }

}
