package chess.tests;

import chess.board.MatchNotifying;
import chess.originalgame.setup.Permutation;
import chess.reuse.MutatingNetworkPlayer;
import chess.execution.PiecesAheadAfterMaxMovesWins;
import chess.player.BruteForceDepthPlayerAsDownloaded;
import chess.board.Player;
import chess.gui.MatchIllustration;
import chess.gui.TableOfMoves;
import chess.wrappers.ResultWrapper;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class RunSomething {

    public static void main(String[] args) throws Exception {
        MatchNotifying board = new MatchNotifying();
        board.init();

        TableOfMoves protocol = new TableOfMoves(board.surveillor);
        protocol.init();

        MatchIllustration canvas = new MatchIllustration(board, 1.0);

        Frame f = new Frame("Schackbräde");
        f.setLocation(500, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

        Map<Integer, Byte> countReps = new HashMap<>();

        Player master = new BruteForceDepthPlayerAsDownloaded((byte)1, countReps);
        //Player master = new RandomPlayer();
        Player challenger = null;//new MutatingNetworkPlayer("tmp/1.ser");

        PiecesAheadAfterMaxMovesWins player = new PiecesAheadAfterMaxMovesWins(board, protocol, 140);

        ResultWrapper res = Permutation.permuteForGeneralBestTwoSides(master, challenger, player);

        System.out.println(res.toString());
    }

}
