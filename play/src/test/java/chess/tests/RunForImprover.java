package chess.tests;

import chess.board.MatchNotifying;
import chess.execution.PiecesAheadAfterMaxMovesWins;
import chess.gui.MatchIllustration;
import chess.gui.TableOfMoves;
import chess.gui.paint.Painter;
import chess.originalgame.setup.FindBest;
import chess.wrappers.ResultWrapper;

import java.awt.*;
import java.util.Date;

public class RunForImprover {

    /*
    public static void main(String[] args) throws Exception {
        TableOfMoves protocol = null;//TableOfMoves.get();
        //protocol.init();
        MatchNotifying board = new MatchNotifying();
        board.init();

        MatchIllustration canvas = new MatchIllustration(board, 1.0);

            Frame f = new Frame("Schackbräde");
            f.setLocation(500, 0);
            f.add(canvas);
            f.pack();
            f.setVisible(true);

        PiecesAheadAfterMaxMovesWins player = new PiecesAheadAfterMaxMovesWins(board, protocol, 140);

        int nr = 1;
        for(int i = 0 ; i < 10000 ; i++) {
            ResultWrapper res = FindBest.runTwoSides(
                    nr,
                    player,
                    "tmp/<NR>.ser",
                    rw -> rw.challenger - rw.master >= 3.0);
            if(res == null) {
                System.out.println("Oavgjort genom NULL "+(new Date()));
                continue;
            }
            if(res.challenger > res.master) {
                nr++;
                System.out.println("Ny bästa "+nr+" på varv "+i+" med sittande "+res.master+", mutationen "+res.challenger);
                continue;
            }
            if(res.challenger < res.master) {
                System.out.println("Mästaren sitter kvar med "+res.master+", mutationen "+res.challenger);
                continue;
            }
            System.out.println("Oavgjort");
        }
    }
    */

}
