package chess.tests;

import chess.board.*;
import chess.execution.PlayUpdateTime;
import chess.functions.Measures;
import chess.functions.WhoWantsStalemateFunctions;
import chess.functions.staticEvaluation.EvalMain;
import chess.gui.AnalysisTableOfMoves;
import chess.operations.Moving;
import chess.operations.Serialization;
import chess.player.BruteForceDepthPlayerMinimax;
import chess.board.Player;
import chess.gui.MatchGUIMoving;
import chess.gui.paint.Painter;
import chess.gui.paint.PainterForBlackDown;
import chess.gui.paint.PainterForWhiteDown;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class HumanAgainstBot {

    public static void main(String[] args) throws Exception {
        int botNr = 1;
        Map<Integer, Byte> countReps = new HashMap<>();
        Player bot = new BruteForceDepthPlayerMinimax(
                (byte)(0),
                s -> EvalMain.evaluateBoardScore(s),
                s -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s),
                countReps);
        humanAgainstBot(bot, Board.WHITE);
    }

    public static void dumpScoresCurrent(MatchNotifying board) {
        System.out.println("Current: " + EvalMain.evaluateBoardScore(board.state)+" : "+Measures.whitesPieceAdvantage(board.state));
        System.out.println("-");
    }

    public static void dumpScoresCurrentAndNext(MatchNotifying board) {
        System.out.println("Current: " + EvalMain.evaluateBoardScore(board.state)+" : "+Measures.whitesPieceAdvantage(board.state));
        for (Move move : board.state.getLegalMoves()) {
            State nextState = Moving.attempted(board.state, move, board.countRepetitions);
            System.out.println(move.toString() + " -> " + EvalMain.evaluateBoardScore(nextState)+" : "+Measures.whitesPieceAdvantage(nextState));
        }
        System.out.println("-");
    }

    public static void humanAgainstBot(Player bot, boolean humanColor) throws Exception {
        final MatchNotifying board = new MatchNotifying();
        board.init();
        Painter painter = humanColor ? new PainterForWhiteDown() : new PainterForBlackDown();
        AnalysisTableOfMoves protocol = new AnalysisTableOfMoves(board);
        protocol.addAction("EVAL", () -> dumpScoresCurrentAndNext(board));
        final MatchGUIMoving canvas = new MatchGUIMoving(board, painter);
        Runnable onMove = () -> {
            Thread t = new Thread(() -> {
                playOneMove(board, bot);
                canvas.repaint();
            });
            t.start();
        };
        if(humanColor) {
            board.surveillor.addBlackToMoveListener(HumanAgainstBot.class, onMove);
        }
        else {
            board.surveillor.addWhiteToMoveListener(HumanAgainstBot.class, onMove);
        }

        canvas.addKeyListener(canvas);

        Frame f = new Frame("Schackbräde");
        canvas.addKeyListener(f);
        f.setLocation(500, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

        if(!humanColor) {
            // Människan har svart, botten börjar
            onMove.run();
        }
    }

    public static void playOneMove(Match board, Player bot) {
        //protocol.addMove(board.state);
        try {
            PlayUpdateTime.playOneMove(board, bot);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void serialize(MatchNotifying board) {
        System.out.println(Serialization.getStateString(board.state));
    }

}
