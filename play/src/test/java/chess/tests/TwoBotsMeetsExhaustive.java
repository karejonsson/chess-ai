package chess.tests;

import chess.board.MatchNotifying;
import chess.execution.PiecesAheadAfterMaxMovesWins;
import chess.functions.BruteForceFunctions;
import chess.functions.Measures;
import chess.functions.StateEvaluationFunctions;
import chess.functions.WhoWantsStalemateFunctions;
import chess.originalgame.setup.Permutation;
import chess.player.BruteForceDepthPlayerMinimax;
import chess.board.Player;
import chess.gui.MatchIllustration;
import chess.gui.TableOfMoves;
import chess.reuse.MutatingNetworkPlayer;
import chess.wrappers.ResultWrapper;
//import se.prv.ai.mutating.net.MutationNetwork;

import javax.swing.*;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class TwoBotsMeetsExhaustive {

    public static void main(String[] args) throws Exception {
        Map<Integer, Byte> countReps = new HashMap<>();
        meet(
                new BruteForceDepthPlayerMinimax(
                        (byte)(2),
                        s1 -> StateEvaluationFunctions.accumulatingMoveValues(
                                s1,
                                (s2, m) -> BruteForceFunctions.evalMove(s2, m),
                                (e, s4) -> e+300* Measures.whitesPieceAdvantage(s4)),
                        s3 -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s3),
                        countReps)
                ,
                new BruteForceDepthPlayerMinimax(
                        (byte)(2),
                        s1 -> StateEvaluationFunctions.accumulatingMoveValues(
                                s1,
                                (s2, m) -> BruteForceFunctions.evalMove(s2, m),
                                (e, s4) -> e+300*Measures.whitesPieceAdvantage(s4)),
                        s3 -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s3),
                        countReps)
        );
    }

    public static void meet(byte masterIdx, byte challengerIdx) throws Exception {
        /*
        FileInputStream fis = new FileInputStream("tmp/" + masterIdx + ".ser");
        MutationNetwork master = MutationNetwork.deserialize(fis, StandardCharsets.UTF_8);
        fis.close();
        fis = new FileInputStream("tmp/" + challengerIdx + ".ser");
        MutationNetwork challenger = MutationNetwork.deserialize(fis, StandardCharsets.UTF_8);
        fis.close();
        Player pm = new MutatingNetworkPlayer(master);
        Player pc = new MutatingNetworkPlayer(challenger);

        meet(pm, pc);
        //meet(pm, pc);

         */
    }

    public static void meet(Player pm, Player pc) {
        MatchNotifying board = new MatchNotifying();
        board.init();

        MatchIllustration canvas = new MatchIllustration(board, 1.0);
        JFrame f = new JFrame("Schackbräde");
        f.setLocation(500, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

        TableOfMoves protocol = new TableOfMoves(board.surveillor);

        PiecesAheadAfterMaxMovesWins gameDriver = new PiecesAheadAfterMaxMovesWins(board, protocol, 140);

        board.surveillor.addMoveListener(gameDriver, e -> canvas.repaint());

        Thread t = new Thread(() -> {
            try {
                ResultWrapper w = Permutation.permuteForGeneralBestTwoSides(pm, pc, gameDriver);
                System.out.println(pm.getName()+": "+w.master+", "+pc.getName()+" "+w.challenger);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        });
        t.start();
    }

}
