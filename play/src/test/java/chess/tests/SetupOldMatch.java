package chess.tests;

import chess.debuganalysis.Reuse;
import chess.functions.StaticStateEvaluation;
import chess.functions.staticEvaluation.EvalMain;

import java.io.IOException;

public class SetupOldMatch {

    public static void main(String[] args) throws IOException {
        byte depth = 2;
        StaticStateEvaluation se1 = s -> EvalMain.evaluateBoardScore(s);
        StaticStateEvaluation se2 = se1;//s -> Measures.whitesPieceAdvantage(s);
        String protocolFile = "1639065688716.chessprot";
        Reuse.initByProtocol(depth, se1, se2, protocolFile);
    }

}
