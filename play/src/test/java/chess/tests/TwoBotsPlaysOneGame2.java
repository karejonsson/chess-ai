package chess.tests;

import chess.debuganalysis.Reuse;
import chess.functions.*;
import chess.functions.staticEvaluation.EvalMain;

public class TwoBotsPlaysOneGame2 {

    public static void main(String[] args) {
        StaticStateEvaluation se1 = s -> EvalMain.evaluateBoardScore(s);
        StaticStateEvaluation se2 = s -> Measures.whitesPieceAdvantage(s);
        Reuse.meet(se1, se1, 2);
    }

}

/*
R__QK_NRPPP__PPP__NB____p___P_____rP__B________p_pppppp__nbqkbnr:TFFFFFFTFF:H7H6F____

Vit räddar inte sin löpare. Varför?
Svar: För slaget på a4 sätter springaren att hota tornet

    public static void main(String[] args) throws Exception {
        StaticStateEvaluation se1 = s -> AmbitiousFunction.evaluateBoardScore(s);
        StaticStateEvaluation se2 = s -> Measures.whitesPieceAdvantage(s);
        TwoBotsPlaysOneGame1.meet(se1, se2, 2);
    }

 */

/*
R_Q_KBNRPP_BPPPPN_PP____p________p_b______pp________pppprn_qkbnr:FFFTFFFFFF:B1C1F____

Svart ställer sin löpare i slag. Varför?
(Följande iakttogs: R_Q_KBNRbP_BPPPPN_PP____p________p________pp________pppprn_qkbnr:TFFTFFFFFF:D5A2F____)

    public static void main(String[] args) throws Exception {
        StaticStateEvaluation se1 = s -> AmbitiousFunction.evaluateBoardScore(s);
        StaticStateEvaluation se2 = s -> Measures.whitesPieceAdvantage(s);
        TwoBotsPlaysOneGame1.meet(se2, se2, 2);
    }

 */