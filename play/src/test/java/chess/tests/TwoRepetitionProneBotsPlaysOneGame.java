package chess.tests;

import chess.debuganalysis.Reuse;
import chess.functions.Measures;
import chess.functions.StaticStateEvaluation;

public class TwoRepetitionProneBotsPlaysOneGame {

    public static void main(String[] args) throws Exception {
        StaticStateEvaluation se1 = s -> Measures.whitesPieceAdvantage(s);
        StaticStateEvaluation se2 = s -> Measures.whitesPieceAdvantage(s);
        Reuse.meet(se2, se1, 1);
    }

}
