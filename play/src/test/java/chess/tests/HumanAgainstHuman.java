package chess.tests;

import chess.board.*;
import chess.gui.AnalysisTableOfMoves;
import chess.gui.MatchGUIMoving;
import chess.gui.paint.Painter;
import chess.gui.paint.PainterForBlackDown;
import chess.gui.paint.PainterForWhiteDown;

import java.awt.*;

public class HumanAgainstHuman {

    public static void main(String[] args) throws Exception {
        int botNr = 1;
        /*
        FileInputStream fis = new FileInputStream("tmp/sor"+botNr+".ser");
        MutationNetwork nn = MutationNetwork.deserialize(fis, StandardCharsets.UTF_8);
        fis.close();
        Player bot = new MutatingNetworkPlayer(nn);
        */
        /*
        Player bot = new BruteForceDepthPlayerMinimax(
            0,
            s1 -> StateEvaluationFunctions.accumulatingMoveValues(
                s1,
                (s2, m) -> BruteForceFunctions.evalMove(s2, m),
                (e, s4) -> e+300* Measures.whitesPieceAdvantage(s4)),
            s3 -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s3));
         */
        humanAgainstBot(Board.WHITE);
    }

    public static void humanAgainstBot(boolean colorDown) throws Exception {
        final MatchNotifying board = new MatchNotifying();
        board.init();
        Painter painter = colorDown ? new PainterForWhiteDown() : new PainterForBlackDown();
        AnalysisTableOfMoves protocol = new AnalysisTableOfMoves(board);
        protocol.addAction("EVAL C+1", () -> HumanAgainstBot.dumpScoresCurrentAndNext(board));
        protocol.addAction("EVAL C", () -> HumanAgainstBot.dumpScoresCurrent(board));
        protocol.addAction("Sträng", () -> HumanAgainstBot.serialize(board));
        final MatchGUIMoving canvas = new MatchGUIMoving(board, painter);

        canvas.addKeyListener(canvas);

        Frame f = new Frame("Schackbräde");
        canvas.addKeyListener(f);
        f.setLocation(500, 0);
        f.add(canvas);
        f.pack();
        f.setVisible(true);

    }

}
