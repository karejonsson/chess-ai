package chess.tests;

import chess.board.MatchNotifying;
import chess.debuganalysis.Reuse;
import chess.execution.Sequencing;
import chess.functions.*;
import chess.gui.AnalysisTableOfMoves;
import chess.gui.MatchIllustration;
import chess.player.BruteForceDepthPlayerMinimax;
import chess.board.Player;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public class TwoBotsPlaysOneGame1 {

    public static void main(String[] args) throws Exception {
        Map<Integer, Byte> countReps = new HashMap<>();
        Player p1 =
            new BruteForceDepthPlayerMinimax(
                    (byte)(1),
                s1 -> StateEvaluationFunctions.maximizingMoveValues(
                        s1,
                        (s2, m) -> BruteForceFunctions.evalMove(s2, m),
                        (e, s4) -> e+300*Measures.whitesPieceAdvantage(s4)),
                s3 -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s3),
                    countReps);
        Player p2 =
            new BruteForceDepthPlayerMinimax(
                    (byte)(1),
                s1 -> StateEvaluationFunctions.accumulatingMoveValues(
                        s1,
                        (s2, m) -> (s2.turn ? 1 : -1)*BruteForceFunctions.evalMove(s2, m),
                        (e, s4) -> e+300*Measures.whitesPieceAdvantage(s4)),
                s3 -> WhoWantsStalemateFunctions.whoIsToGoodForStalemate(s3),
                    countReps);
        Reuse.meet(p1, p2);
    }

}
