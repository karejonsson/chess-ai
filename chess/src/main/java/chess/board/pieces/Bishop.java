package chess.board.pieces;

import chess.board.Board;
import chess.board.Move;
import chess.board.State;
import chess.operations.Getters;
import chess.board.Square;

import java.util.*;

public class Bishop extends Piece {

    public static final byte idx = 2;

    public Bishop(boolean color) {
        super(color);
    }

    public String toString() {
        return "Löpare: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Bishop(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state)  {
        ArrayList<Move> out = new ArrayList<>();
        if(getColor() == state.turn) {
            addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state)  {
        ArrayList<Move> out = new ArrayList<>();
        addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public static void addMovesRegardingFunctionality(ArrayList<Move> out, Square from, State state, boolean color)  {
        Square tempSquare = new Square();

        // Ovan vänster
        for(byte dist = 1 ; dist <= Math.min(Board.ranks-from.rank, from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank + dist);
            tempSquare.file = (byte)(from.file - dist);
            if(!addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

        // Nedan vänster
        for(byte dist = 1 ; dist <= Math.min(from.rank, from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank - dist);
            tempSquare.file = (byte)(from.file - dist);
            if(!addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

        // Nedan höger
        for(byte dist = 1 ; dist <= Math.min(from.rank, Board.files-from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank - dist);
            tempSquare.file = (byte)(from.file + dist);
            if(!addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

        // Ovan höger
        for(byte dist = 1 ; dist <= Math.min(Board.ranks-from.rank, Board.files-from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank + dist);
            tempSquare.file = (byte)(from.file + dist);
            if(!addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }
    }

    public static boolean addMoveRegardingFunctionality(ArrayList<Move> out, State state, Square from, Square tempSquare, boolean color) {
        //System.out.println("Från "+from+" till "+tempSquare);
        if(!tempSquare.onBoard()) {
            //System.out.println("Utanför - NEJ, sluta leta");
            return false; // Borde inte ske men sluta iaf att leta
        }
        Piece piece = Getters.getPiece(state, tempSquare.rank, tempSquare.file);
        if(piece == null) {
            //System.out.println("Ingen pjäs där - OK, fortsätt leta");
            // Om rutan är tom är det ett drag utan slag
            out.add(new Move(from, tempSquare)); // Move-konstruktorn klonar rutan
            return true; // Fortsätt leta
        }
        if(piece.getColor() != color) {
            //System.out.println("Annan pjäs annan färg där - OK, sluta leta");
            // Om det är andra färgen som står här finns draget i form av ett slag
            out.add(new Move(from, tempSquare)); // Move-konstruktorn klonar rutan
            return false; // Sluta leta
        }
        //System.out.println("Annan pjäs samma färg där - NEJ, sluta leta");
        // Om det är en pjäs av samma färg finns inte draget
        return false; // Sluta leta
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        if(getColor() == state.turn) {
            addReachedOccupiedSquares(out, from, state);
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        addReachedOccupiedSquares(out, from, state);
        return out;
    }

    public static void addReachedOccupiedSquares(ArrayList<Square> out, Square from, State state) {
        Square tempSquare = new Square();

        // Ovan vänster
        for(byte dist = 1 ; dist <= Math.min(Board.ranks-from.rank, from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank + dist);
            tempSquare.file = (byte)(from.file - dist);
            if(!addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }

        // Nedan vänster
        for(byte dist = 1 ; dist <= Math.min(from.rank, from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank - dist);
            tempSquare.file = (byte)(from.file - dist);
            if(!addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }

        // Nedan höger
        for(byte dist = 1 ; dist <= Math.min(from.rank, Board.files-from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank - dist);
            tempSquare.file = (byte)(from.file + dist);
            if(!addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }

        // Ovan höger
        for(byte dist = 1 ; dist <= Math.min(Board.ranks-from.rank, Board.files-from.file) ; dist++) {
            tempSquare.rank = (byte)(from.rank + dist);
            tempSquare.file = (byte)(from.file + dist);
            if(!addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }
    }

    public static boolean addReachedOccupiedSquare(ArrayList<Square> out, State state, Square from, Square tempSquare) {
        if(!tempSquare.onBoard()) {
            // Utanför brädet
            return false; // Sluta leta
        }
        if(Getters.isOccupied(state, tempSquare)) {
            // En nådd ruta vare sig det är en gardering eller ett slag
            out.add(tempSquare.clone());
            return false; // Sluta leta
        }
        return true; // Fortsätt leta
    }

    @Override
    public byte index() {
        return idx;
    }

}

