package chess.board.pieces;

import chess.board.Move;
import chess.board.Square;
import chess.board.State;

import java.util.ArrayList;

public class Archbishop extends Piece {

    public static final byte idx = 7;

    public Archbishop(boolean color) {
        super(color);
    }

    public String toString() {
        return "Ärkebiskop: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Archbishop(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        if (getColor() == state.turn) {
            Knight.addMovesRegardingFunctionality(out, from, state, color);
            Bishop.addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        Knight.addMovesRegardingFunctionality(out, from, state, color);
        Bishop.addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        if (getColor() == state.turn) {
            Knight.addReachedOccupiedSquares(out, from, state);
            Bishop.addReachedOccupiedSquares(out, from, state);
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Knight.addReachedOccupiedSquares(out, from, state);
        Bishop.addReachedOccupiedSquares(out, from, state);
        return out;
    }

    @Override
    public byte index() {
        return idx;
    }

}

