package chess.board.pieces;

import chess.board.Move;
import chess.board.State;
import chess.operations.Getters;
import chess.board.Square;

import java.util.*;

public class Knight extends Piece {

    public static final byte idx = 1;

    public Knight(boolean c) {
        super(c);
    }

    public String toString() {
        return "Springare: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Knight(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        if(color == state.turn) {
            addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public static void addMovesRegardingFunctionality(ArrayList<Move> out, Square from, State state, boolean color)  {
        Square tempSquare = new Square();
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-1, (byte)-2, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-2, (byte)-1, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-2, (byte)1, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-1, (byte)2, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)1, (byte)2, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)2, (byte)1, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)2, (byte)-1, color);
        addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)1, (byte)-2, color);
    }

    public static void addMoveRegardingFunctionality(ArrayList<Move> out, State state, Square from, Square tempSquare, byte dx, byte dy, boolean color) {
        tempSquare.file = (byte)(from.file + dx);
        tempSquare.rank = (byte)(from.rank + dy);

        if(!tempSquare.onBoard()) {
            return;
        }
        Piece piece = Getters.getPiece(state, tempSquare);
        if(piece == null || piece.getColor() != color) {
            out.add(new Move(from, tempSquare)); // Move-konstruktorn klonar rutan
        }
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        if(getColor() == state.turn) {
            addReachedOccupiedSquares(out, from, state);
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        addReachedOccupiedSquares(out, from, state);
        return out;
    }

    public static void addReachedOccupiedSquares(ArrayList<Square> moves, Square from, State state) {
        Square tempSquare = new Square();
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)-1, (byte)-2);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)-2, (byte)-1);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)-2, (byte)1);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)-1, (byte)2);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)1, (byte)2);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)2, (byte)1);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)2, (byte)-1);
        addReachedOccupiedSquare(moves, state, from, tempSquare, (byte)1, (byte)-2);
    }

    public static void addReachedOccupiedSquare(ArrayList<Square> out, State state, Square from, Square tempSquare, byte dx, byte dy) {
        tempSquare.file = (byte)(from.file + dx);
        tempSquare.rank = (byte)(from.rank + dy);

        if(tempSquare.onBoard() && Getters.isOccupied(state, tempSquare)) {
            out.add(tempSquare.clone());
        }
    }

    @Override
    public byte index() {
        return idx;
    }

}
