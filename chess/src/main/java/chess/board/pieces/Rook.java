package chess.board.pieces;

import chess.board.Board;
import chess.board.Move;
import chess.board.State;
import chess.operations.Getters;
import chess.board.Square;

import java.util.*;

public class Rook extends Piece {

    public static final byte idx = 3;

    public Rook(boolean c) {
        super(c);
    }

    public String toString() {
        return "Torn: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Rook(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) {
        ArrayList<Move> out = new ArrayList();
        if(getColor() == state.turn) {
            addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) {
        ArrayList<Move> out = new ArrayList();
        addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public static void addMovesRegardingFunctionality(ArrayList<Move> out, Square from, State state, boolean color)  {
        Square tempSquare = new Square();

        // Ovan
        tempSquare.file = from.file;
        for(byte r = (byte)(from.rank+1) ; r < Board.ranks ; r++) {
            tempSquare.rank = r;
            if(!Bishop.addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

        // Nedan
        for(byte r = (byte)(from.rank-1) ; r >= 0 ; r--) {
            tempSquare.rank = r;
            if(!Bishop.addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

        // Vänster
        tempSquare.rank = from.rank;
        for(byte f = (byte)(from.file-1) ; f >= 0 ; f--) {
            tempSquare.file = f;
            if(!Bishop.addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

        // Höger
        for(byte f = (byte)(from.file+1) ; f < Board.files ; f++) {
            tempSquare.file = f;
            if(!Bishop.addMoveRegardingFunctionality(out, state, from, tempSquare, color)) {
                break;
            }
        }

    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        if (getColor() == state.turn) {
            addReachedOccupiedSquares(out, from, state);
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        addReachedOccupiedSquares(out, from, state);
        return out;
    }

    public static void addReachedOccupiedSquares(ArrayList<Square> out, Square from, State state) {
        Square tempSquare = new Square();

        // Ovan
        tempSquare.file = from.file;
        for(byte r = (byte)(from.rank+1) ; r < Board.ranks ; r++) {
            tempSquare.rank = r;
            if(!Bishop.addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }

        // Nedan
        for(byte r = (byte)(from.rank-1) ; r >= 0 ; r--) {
            tempSquare.rank = r;
            if(!Bishop.addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }

        // Vänster
        tempSquare.rank = from.rank;
        for(byte f = (byte)(from.file-1) ; f >= 0 ; f--) {
            tempSquare.file = f;
            if(!Bishop.addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }

        // Höger
        for(byte f = (byte)(from.file+1) ; f < Board.files ; f++) {
            tempSquare.file = f;
            if(!Bishop.addReachedOccupiedSquare(out, state, from, tempSquare)) {
                break;
            }
        }
    }

    @Override
    public byte index() {
        return idx;
    }

}
