package chess.board.pieces;

import chess.board.Move;
import chess.board.State;
import chess.board.Square;

import java.util.*;

public class Queen extends Piece {

    public static final byte idx = 4;

    public Queen(boolean c) {
        super(c);
    }

    public String toString() {
        return "Dam: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Queen(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        if(getColor() == state.turn) {
            Rook.addMovesRegardingFunctionality(out, from, state, color);
            Bishop.addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        Rook.addMovesRegardingFunctionality(out, from, state, color);
        Bishop.addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        if(getColor() == state.turn) {
            Rook.addReachedOccupiedSquares(out, from, state);
            Bishop.addReachedOccupiedSquares(out, from, state);
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Rook.addReachedOccupiedSquares(out, from, state);
        Bishop.addReachedOccupiedSquares(out, from, state);
        return out;
    }

    @Override
    public byte index() {
        return idx;
    }

}
