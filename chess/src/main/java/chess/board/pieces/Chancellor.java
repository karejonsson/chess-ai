package chess.board.pieces;

import chess.board.Move;
import chess.board.Square;
import chess.board.State;

import java.util.ArrayList;

public class Chancellor extends Piece {

    public static final byte idx = 6;

    public Chancellor(boolean c) {
        super(c);
    }

    public String toString() {
        return "Kansler: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Chancellor(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        if (getColor() == state.turn) {
            Knight.addMovesRegardingFunctionality(out, from, state, color);
            Rook.addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) {
        ArrayList<Move> out = new ArrayList<>();
        Knight.addMovesRegardingFunctionality(out, from, state, color);
        Rook.addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        if (getColor() == state.turn) {
            Knight.addReachedOccupiedSquares(out, from, state);
            Rook.addReachedOccupiedSquares(out, from, state);
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Knight.addReachedOccupiedSquares(out, from, state);
        Rook.addReachedOccupiedSquares(out, from, state);
        return out;
    }

    @Override
    public byte index() {
        return idx;
    }

}
