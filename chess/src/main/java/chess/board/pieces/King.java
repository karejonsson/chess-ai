package chess.board.pieces;

import chess.board.*;
import chess.operations.Getters;
import chess.rules.Castling;

import java.util.*;

public class King extends Piece {

    public static final byte idx = 5;

    public King(boolean c) {
        super(c);
    }

    public String toString() {
        return "Kung: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new King(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecks(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State b, Move lastMove)  {
        return getMovesDisregardingChecksAndTurn(from, b);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) {
        ArrayList<Move> out = new ArrayList();
        if(getColor() == state.turn) {
            addMovesRegardingFunctionality(out, from, state, color);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) {
        ArrayList<Move> out = new ArrayList();
        addMovesRegardingFunctionality(out, from, state, color);
        return out;
    }

    public static void addMovesRegardingFunctionality(ArrayList<Move> out, Square from, State state, boolean color)  {
        // Check for castling
        if (Castling.canCastle(state, state.turn, 'Q')) {
            out.add(new Move(from,new Square(from.rank,(byte)(Board.kingFile-Board.castlingKingDistance)),'Q'));
        }

        if (Castling.canCastle(state, state.turn, 'K')) {
            out.add(new Move(from, new Square(from.rank, (byte)(Board.kingFile+Board.castlingKingDistance)),'K'));
        }

        Square tempSquare = new Square();

        // Check normal moves
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-1, (byte)-1, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-1, (byte)0, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)-1, (byte)1, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)0, (byte)-1, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)0, (byte)1, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)1, (byte)-1, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)1, (byte)0, color);
        Knight.addMoveRegardingFunctionality(out, state, from, tempSquare, (byte)1, (byte)1, color);
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Square tempSquare = new Square();
        // Castling is not a reaching move

        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)-1, (byte)-1);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)-1, (byte)0);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)-1, (byte)1);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)0, (byte)-1);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)0, (byte)1);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)1, (byte)-1);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)1, (byte)0);
        Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte)1, (byte)1);
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Square tempSquare = new Square();
        // Castling is not a reaching move
        if(getColor() == state.turn) {
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) -1, (byte) -1);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) -1, (byte) 0);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) -1, (byte) 1);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) 0, (byte) -1);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) 0, (byte) 1);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) 1, (byte) -1);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) 1, (byte) 0);
            Knight.addReachedOccupiedSquare(out, state, from, tempSquare, (byte) 1, (byte) 1);
        }
        return out;
    }

    @Override
    public byte index() {
        return idx;
    }

}

