package chess.board.pieces;

import chess.board.Board;
import chess.board.Move;
import chess.board.State;
import chess.operations.Getters;
import chess.board.Square;

import java.util.*;

public class Pawn extends Piece {

    public static final byte idx = 0;

    public Pawn(boolean c) {
        super(c);
    }

    public String toString() {
        return "Bonde: "+(color ? "VIT" : "SVART");
    }

    public Piece copyToColor(boolean color) {
        return new Pawn(color);
    }

    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state, Move lastMove) {
        ArrayList<Move> out = new ArrayList<>();
        if (getColor() == state.turn) {
            addMovesDisregardingChecks(out, from, state, lastMove);
        }
        return out;
    }

    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state, Move lastMove) {
        ArrayList<Move> out = new ArrayList<>();
        addMovesDisregardingChecks(out, from, state, lastMove);
        return out;
    }

    public void addMovesDisregardingChecks(ArrayList<Move> out, Square from, State state, Move lastMove) {
        byte dx,dy;
        Square tempSquare = new Square();

        // Find the rank-vise direction for the pawn
        dy = state.turn == Board.WHITE ? (byte)1 : (byte)-1;

        // Check for straight ahead move
        tempSquare.file = from.file;
        tempSquare.rank = (byte)(from.rank + dy);

        if (!(Getters.isOccupied(state, tempSquare))) {
            // forward promotion
            if (tempSquare.rank == 0 || tempSquare.rank == (Board.ranks-1)) {
                out.add(new Move(from, tempSquare, true, 'Q'));
                out.add(new Move(from, tempSquare, true, 'R'));
                out.add(new Move(from, tempSquare, true, 'B'));
                out.add(new Move(from, tempSquare, true, 'N'));
            }
            else {
                out.add(new Move(from, tempSquare));
            }

            // Also check for double move
            tempSquare.rank = (byte)(from.rank + 2*dy);
            tempSquare.file = from.file;
            if (tempSquare.onBoard() && !(Getters.isOccupied(state, tempSquare))
                    && (from.rank == 1 || from.rank == (Board.ranks-2))) {
                out.add(new Move(from, tempSquare));
            }
        }

        // Check for attack
        for (dx = -1; dx <=1; dx+=2) {
            tempSquare.file = (byte)(from.file + dx);
            tempSquare.rank = (byte)(from.rank + dy);

            if (tempSquare.onBoard() && Getters.isOccupied(state, tempSquare)
                    && Getters.getPiece(state, tempSquare).getColor() != getColor()) {
                if (tempSquare.rank > 0 && tempSquare.rank < Board.ranks-1) {
                    out.add(new Move(from, tempSquare));
                }
                else {
                    // capture promotion
                    out.add(new Move(from, tempSquare, true, 'Q'));
                    out.add(new Move(from, tempSquare, true, 'R'));
                    out.add(new Move(from, tempSquare, true, 'B'));
                    out.add(new Move(from, tempSquare, true, 'N'));
                }
            }
            // Check for en passant
            else if (getColor() == Board.BLACK && tempSquare.rank == 2) {
                Square capSquare = new Square((byte)3, tempSquare.file);
                //Move lastMove = b.getLastMove();
                if (tempSquare.onBoard() && Getters.isOccupied(state, capSquare)
                        && Getters.getPiece(state, capSquare).getColor() == Board.WHITE
                        && Getters.getPiece(state, capSquare).index() == Pawn.idx
                        && lastMove.equals(new Move(new Square((byte)1, tempSquare.file),
                        capSquare)))
                    out.add(new Move(from, tempSquare, capSquare));
            }
            else if (getColor() == Board.WHITE && lastMove != null && tempSquare.rank == (Board.ranks-3))
            {
                Square capSquare = new Square((byte)(Board.ranks-4), tempSquare.file);
                //Move lastMove = b.getLastMove();
                if (tempSquare.onBoard() && Getters.isOccupied(state, capSquare)
                        && Getters.getPiece(state, capSquare).getColor() == Board.BLACK
                        && Getters.getPiece(state, capSquare).index() == Pawn.idx
                        && lastMove.equals(new Move(new Square((byte)(Board.ranks-2), tempSquare.file),
                        capSquare)))
                    out.add(new Move(from, tempSquare, capSquare));
            }
        }
    }

    @Override
    public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) throws Exception {
        throw new Exception("Illegal call. Pawn.getMovesDisregardingChecks(Square from, State state)");
    }

    @Override
    public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) throws Exception {
        throw new Exception("Illegal call. Pawn.getMovesDisregardingChecks(Square from, State state)");
    }

    public ArrayList<Square> getReachedOccupiedSquares(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Square tempSquare = new Square();

        if (getColor() == state.turn) {
            // Find the direction for the pawn
            tempSquare.rank = (byte)(from.rank + (state.turn == Board.WHITE ? 1 : -1));
            tempSquare.file = (byte)(from.file + -1);
            if(tempSquare.onBoard() && Getters.isOccupied(state, tempSquare)) {
                out.add(tempSquare.clone());
            }

            tempSquare.file = (byte)(from.file + 1);
            if(tempSquare.onBoard() && Getters.isOccupied(state, tempSquare)) {
                out.add(tempSquare.clone());
            }
        }
        return out;
    }

    public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state) {
        ArrayList<Square> out = new ArrayList<>();
        Square tempSquare = new Square();

        // Find the direction for the pawn
        tempSquare.rank = (byte)(from.rank + (color == Board.WHITE ? 1 : -1));
        tempSquare.file = (byte)(from.file + -1);
        if(tempSquare.onBoard() && Getters.isOccupied(state, tempSquare)) {
            out.add(tempSquare.clone());
        }

        tempSquare.file = (byte)(from.file + 1);
        if(tempSquare.onBoard() && Getters.isOccupied(state, tempSquare)) {
            out.add(tempSquare.clone());
        }
        return out;
    }

    @Override
    public byte index() {
        return idx;
    }

}

