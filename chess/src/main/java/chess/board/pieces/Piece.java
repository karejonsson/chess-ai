package chess.board.pieces;

import chess.board.Board;
import chess.board.Move;
import chess.board.Square;
import chess.board.State;
import chess.rules.Check;
import chess.rules.Moves;

import java.util.*;

abstract public class Piece {
    public byte moves = 0;

    // https://www.mcs.anl.gov/~sarich/chess/Piece.java

    protected boolean color;

    public Piece(boolean c) {
        color = c;
    }

    abstract public ArrayList<Move> getMovesDisregardingChecks(Square from, State state, Move previous);
    abstract public ArrayList<Move> getMovesDisregardingChecks(Square from, State state) throws Exception;
    abstract public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state, Move previous);
    abstract public ArrayList<Move> getMovesDisregardingChecksAndTurn(Square from, State state) throws Exception;
    abstract public byte index();
    public boolean getColor() {
        return color;
    }
    abstract public Piece copyToColor(boolean color);
    abstract public ArrayList<Square> getReachedOccupiedSquares(Square from, State state);
    abstract public ArrayList<Square> getReachedOccupiedSquaresDisregardingTurn(Square from, State state);

    public List<Move> getValidMoves(Square from, State state) {
        Move previous = state.previous == null ? null : state.previous.lastMove;
        if(previous != null) {
            return getValidMoves(from, state, previous);
        }
        else {
            // It must be the first two moves.
            List<Move> out = new ArrayList<>();
            for(Move m : state.getLegalMoves()) {
                if(m.from.equals(from)) {
                    out.add(m);
                }
            }
            return out;
        }
    }

    public List<Move> getValidMoves(Square from, State state, Move previous) {
        List<Move> out = new ArrayList<>();
        for(Move m : state.getLegalMoves()) {
            if(m.from.equals(from) && Check.isCheckLegal(state, m, previous)) {
                out.add(m);
            }
        }
        return out;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Piece) {
            Piece other = (Piece) o;
            if(color != other.color) {
                return false;
            }
            if(index() != other.index()) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public final int hashCode() {
        return index()+(color ? Board.pieces : 0);
    }

}
