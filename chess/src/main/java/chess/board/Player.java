package chess.board;

import chess.board.Match;
import chess.board.Move;

import java.util.List;

public interface Player {

    Move findMove(Match board);
    BidirectionalTree<State> analysis(State state);
    String getName();
    int staticEval(State state, List<String> trace) throws Exception;
}
