package chess.board;

import chess.operations.Getters;
import chess.operations.Initiation;
import chess.operations.Serialization;
import chess.rules.Check;

import javax.swing.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Match {

    public State state = null;
    public Map<Integer, Byte> countRepetitions = new HashMap<>(); // Byt till Zobrist-hashning
    public long timeWhite = 0;
    public long timeBlack = 0;

    public Match() {
    }

    public void init() {
        init(Initiation.createStandardInitialState());
    }

    public void init(State state) {
        this.state = state;
        state.getLegalMoves();
        countRepetitions.clear();
        timeWhite = 0;
        timeBlack = 0;
    }

    public Match(State state) {
        this.state = state;
    }

    public Match(State state, Map<Integer, Byte> countRepetitions) {
        this.state = state;
        this.countRepetitions = countRepetitions;
    }

    public void takeBackMove() {
        if(Getters.isInitial(state)) {
            return;
        }
        state = state.previous;
        timeWhite = state.timeWhite;
        timeBlack = state.timeBlack;
    }

    public ArrayList<Move> getAllMoves() {
        return state.getLegalMoves();
    }

    public boolean continues() {
        return state.isWon == null;
    }

    public void readyNextMove() {
        //System.out.println("Reps "+board.state.repetitions);
        if(state.repetitions >= Board.maxrep) {
            state.isWon = false;
            return;
        }

        state.isWon = Check.isMate(state);
    }

    public Boolean winner() {
        return state.isWon != null && state.isWon ? !state.turn : null;
    }

    public interface ForEachState {
        void handle(State state) throws IOException;
    }

    public void write(ForEachState fes) throws IOException {
        State s = state;
        List<State> states = new ArrayList<>();
        while(s != null) {
            states.add(0, s);
            s = s.previous;
        }
        //System.out.println("Antal "+states.size());
        for(State state : states) {
            //String ss = Serialization.getStateString(state);
            fes.handle(state);
        }
    }

    public void write(OutputStream os) throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(os);
        write(osw);
        osw.flush();
        osw.close();
    }

    public void write(OutputStreamWriter osw) throws IOException {
        State s = state;
        List<State> states = new ArrayList<>();
        while(s != null) {
            states.add(0, s);
            s = s.previous;
        }
        //System.out.println("Antal "+states.size());
        for(State state : states) {
            //String ss = Serialization.getStateString(state);
            osw.write(Serialization.getStateString(state)+"\n");
        }
    }

    public static Match read(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        State previous = null;
        while(reader.ready()) {
            String line = reader.readLine();
            State s = Serialization.getState(line.trim());
            s.previous = previous;
            previous = s;
        }
        return new Match(previous);
    }

}
