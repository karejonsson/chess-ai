package chess.board;

import chess.operations.Getters;
import chess.board.pieces.Piece;

public class Move {

    public Square from;
    public Square to;
    public boolean promotion;
    public char promoteTo; // 'Q', 'R', 'B', or 'N'
    public Square enpKill; // The piece killed by en Passant
    public char castle;
    public String message;
    public State state = null;

    public boolean captures() {
        if(enpKill != null) {
            return true;
        }
        if(state != null) {
            if(state.previous != null && Getters.isOccupied(state.previous, to)) {
                return true;
            }
        }
        return false;
    }

    public Piece captured() {
        if(enpKill != null) {
            return Getters.getPiece(state.previous, enpKill);
        }
        if(state != null) {
            if(state.previous != null && Getters.isOccupied(state.previous, to)) {
                return Getters.getPiece(state.previous, to);
            }
        }
        return null;
    }

    public String toString() {
        if(castle == 'K') {
            return "O-O";
        }
        if(castle == 'Q') {
            return "O-O-O";
        }
        String connecting = "-";
        String pieceLetter = " ";
        String finish = "";
        if(state.isWon != null) {
            if(state.isWon) {
                finish = " Mate"; // Partiet vunnet
            }
            else {
                finish = " Stale"; // Partiet patt
            }
        }
        if(state != null) {
            if(state.previous != null && Getters.isOccupied(state.previous, to)) {
                connecting = "*";
            }
            Piece p = Getters.getPiece(state, to);
            pieceLetter = p != null ? Getters.getCharForMoveNotation(p) : " ";
        }
        if(promotion && state != null) {
            Piece p = Getters.getPiece(state, to);
            pieceLetter = p != null ? Getters.getCharForMoveNotation(p) : " ";
            return " "+from.toString().toLowerCase()+connecting+to.toString().toLowerCase()+
                    (promotion ? "->"+pieceLetter : "")+
                    (state.check ? "+" : "")+finish;
        }
        return pieceLetter+from.toString().toLowerCase()+connecting+to.toString().toLowerCase()+
                ((state != null && state.check) ? "+" : "")+finish;
    }

    public Move() {
        from = new Square();
        to = new Square();
        promotion = false;
        castle = (char)0;
    }

    public Move(Square f, Square t) {
        from = f.clone();
        to = t.clone();
        promotion = false;
        castle=(char)0;
    }

    public Move(Square f, Square t, boolean promotion, char promotionPiece) {
        from = new Square(f.rank, f.file);
        to = new Square(t.rank, t.file);
        this.promotion = promotion;
        promoteTo = promotionPiece;
        castle=(char) 0;
    }

    public Move(Square f, Square t, char c) {
        from = new Square(f.rank, f.file);
        to = new Square(t.rank, t.file);
        promotion = false;
        castle = c;
    }

    public Move(Square f, Square t, Square enp) {
        from = new Square(f.rank, f.file);
        to = new Square(t.rank, t.file);
        enpKill = new Square(enp.rank, enp.file);
        castle=(char) 0;
    }

    public boolean equals(Move m) {
        return (m != null && m.to.equals(to) && m.from.equals(from));
    }

    public void setMessage(String s)
    {
        message = s;
    }

    public String getMessage()
    {
        return message;
    }

    public char getCastle()	{ return castle; }

    public void setCastle(char c) { castle = c; }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Move) {
            return equals((Move) o);
        }
        return false;
    }

    @Override
    public final int hashCode() {
        return (Board.ranks*Board.files)*from.hashCode()+to.hashCode();
    }

}