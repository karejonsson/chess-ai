package chess.board;

import chess.board.pieces.Piece;
import chess.operations.ZobristHashing;
import chess.rules.Moves;

import java.util.ArrayList;

public class State {

    public State previous = null;
    public Move lastMove = null;

    public Piece[][] board;

    public byte[] wPieces;
    public byte[] bPieces;

    // order between players true = white
    public boolean turn;

    // check info
    public Square blackKing;
    public Square whiteKing;

    // Castling info
    public boolean whiteKingMoved;
    public boolean blackKingMoved;
    public boolean whiteKRookMoved;
    public boolean whiteQRookMoved;
    public boolean blackKRookMoved;
    public boolean blackQRookMoved;
    public boolean whiteHasCastled;
    public boolean blackHasCastled;

    // Move counting, https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation
    public int halfMoves = 0;
    public int fullMoves = 0;

    public boolean check = false;
    public byte repetitions = 0;
    public long timeWhite = 0;
    public long timeBlack = 0;

    public ZobristHashing zobrist = null;

    public Boolean isWon = null; // True -> turn lost, False -> stalemate, null = continues
    private ArrayList<Move> legalMoves = null;
    //private ArrayList<Move> opponentsMoves = null;

    public ArrayList<Move> getLegalMoves() {
        if(legalMoves != null) {
            return legalMoves;
        }
        legalMoves = Moves.findMoves(this);
        return legalMoves;
    }

    public void clearLegalMoves() {
        legalMoves = null;
        //opponentsMoves = null;
    }

    public void prepareForStaticAnalysis() {
        getLegalMoves();
        /*
        legalMoves = new ArrayList<>();
        opponentsMoves = new ArrayList<>();
        Moves.addMoves(this, legalMoves, opponentsMoves);

         */
    }

    /*
    public ArrayList<Move> getOpponentsMoves() {
        return opponentsMoves;
    }
     */

}
