package chess.board;

public class Wrapper<T> {

    public T value = null;
    public Wrapper(T value) {
        this.value = value;
    }

}
