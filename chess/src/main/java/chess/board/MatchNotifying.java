package chess.board;

import chess.operations.Getters;
import chess.operations.Initiation;
import chess.operations.Serialization;
import chess.rules.Check;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MatchNotifying extends Match {

    public MatchSurveillor surveillor = new MatchSurveillor();

    private Player pw = null;
    private Player pb = null;

    public MatchNotifying() {
        this(null, null);
    }

    public MatchNotifying(Player pw, Player pb) {
        this.pw = pw;
        this.pb = pb;
    }

    public Player getPlayer(boolean player) {
        if(player) {
            return pw;
        }
        return pb;
    }

    public void init() {
        super.init(Initiation.createStandardInitialState());
        surveillor.onInit();
    }

    public void init(State state) {
        super.init(state);
        surveillor.onInit();
    }

    public void readyNextMove() {
        //System.out.println("Reps "+board.state.repetitions);
        Move m = Getters.getLastMove(state);
        surveillor.onMove(m);
        if(state.turn) {
            surveillor.onBlackMove(m);
        }
        else {
            surveillor.onWhiteMove(m);
        }
        if(state.repetitions >= Board.maxrep) {
            surveillor.onGame("Staled, repetitions");
            state.isWon = false;
            surveillor.onEnd();
            return;
        }

        List<Move> moves = getAllMoves();

		/* I don't know why this is necessary, but the
		program keeps trying to shrink this label */
        //messageLbl.setSize(120, 20);
        //canvas.repaint(50);
        if (moves.isEmpty()) {
            if (Check.isInCheck(state, state.turn, m)) {
                surveillor.onGame("Checkmate! "+(state.turn ? "black" : "white")+" won");
                state.isWon = true;
                surveillor.onEnd();
            }
            else {
                surveillor.onGame("Stalemate! "+(state.turn ? "black" : "white")+" won");
                state.isWon = false;
                surveillor.onEnd();
            }
            return;
        } else if (Check.isInCheck(state, state.turn, m)) {
            surveillor.onGame("Check");
            if(state.turn) {
                surveillor.onWhiteToMove();
            }
            else {
                surveillor.onBlackToMove();
            }
        } else if (state.turn) {
            surveillor.onGame("white's move...");
            surveillor.onWhiteToMove();
        } else {
            surveillor.onGame("black's move...");
            surveillor.onBlackToMove();
        }
    }

    public void setPlayerW(Player whitePlayer) {
        pw = whitePlayer;
    }

    public void setPlayerB(Player blackPlayer) {
        pb = blackPlayer;
    }

    public static List<State> readList(InputStream is) throws IOException {
        List<State> out = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        State previous = null;
        while(reader.ready()) {
            String line = reader.readLine();
            State s = Serialization.getState(line.trim());
            s.previous = previous;
            out.add(s);
            previous = s;
        }
        return out;
    }

}
