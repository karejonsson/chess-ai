package chess.board;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MatchSurveillor {

    private HashMap<Object, Runnable> wtmers = new HashMap<>();

    public void reset() {
        wtmers.clear();
        btmers.clear();
        queue.stop();
        wmers.clear();
        bmers.clear();
        mers.clear();
        inits.clear();
        ends.clear();
        whites.clear();
        blacks.clear();
        games.clear();
    }

    public void addWhiteToMoveListener(Object o, Runnable whiteToMove) {
        wtmers.put(o, whiteToMove);
    }

    public void removeWhiteToMoveListener(Object o) {
        wtmers.remove(o);
    }

    public void onWhiteToMove() {
        //System.out.println("Vit skall flytta");
        for(Runnable wtmer : wtmers.values()) {
            addToQueue(wtmer);
        }
    }

    private HashMap<Object, Runnable> btmers = new HashMap<>();

    public void addBlackToMoveListener(Object o, Runnable blackToMove) {
        btmers.put(o, blackToMove);
    }

    public void removeBlackToMoveListener(Object o) {
        btmers.remove(o);
    }

    private RunnableQueue queue = new RunnableQueue(Executors.newFixedThreadPool(1));

    private void addToQueue(Runnable runner) {
        queue.enqueue(runner);
    }

    public void onBlackToMove() {
        //System.out.println("Svart skall flytta");
        for(Runnable btmer : btmers.values()) {
            addToQueue(btmer);
        }
    }

    public interface MoveEventReceiver {
        void moved(Move m);
    }

    private HashMap<Object, MoveEventReceiver> wmers = new HashMap<>();

    public void addWhiteMoveListener(Object o, MoveEventReceiver bmer) {
        wmers.put(o, bmer);
    }

    public void removeWhiteMoveListener(Object o) {
        wmers.remove(o);
    }

    public void onWhiteMove(Move m) {
        //System.out.println("Vit flyttade "+m);
        for(MoveEventReceiver bmer : wmers.values()) {
            //System.out.println("Skickar att vit flyttade");
            addToQueue(() -> bmer.moved(m));
        }
    }

    private HashMap<Object, MoveEventReceiver> bmers = new HashMap<>();

    public void addBlackMoveListener(Object o, MoveEventReceiver bmer) {
        bmers.put(o, bmer);
        //bmers.add(bmer);
    }

    public void removeBlackMoveListener(Object o) {
        bmers.remove(o);
        //bmers.add(bmer);
    }

    public void onBlackMove(Move m) {
        //System.out.println("Svart flyttade "+m);
        for(MoveEventReceiver bmer : bmers.values()) {
            //System.out.println("Skickar att svart flyttade");
            addToQueue(() -> bmer.moved(m));
        }
    }

    private HashMap<Object, MoveEventReceiver> mers = new HashMap<>();

    public void addMoveListener(Object o, MoveEventReceiver mer) {
        mers.put(o, mer);
    }

    public void onMove(Move m) {
        for(MoveEventReceiver mer : mers.values()) {
            addToQueue(() -> mer.moved(m));
        }
    }

    private List<Runnable> inits = new ArrayList<>();

    public void addInitListener(Runnable init) {
        inits.add(init);
    }

    public void onInit() {
        for(Runnable init : inits) {
            addToQueue(init);
        }
    }

    private List<Runnable> ends = new ArrayList<>();

    public void addEndListener(Runnable end) {
        ends.add(end);
    }

    public void onEnd() {
        for(Runnable end : ends) {
            addToQueue(end);
        }
    }

    public interface TextListener {
        void onText(String s);
    }

    private List<TextListener> whites = new ArrayList<>();

    public void addWhiteListener(TextListener white) {
        whites.add(white);
    }

    public void onWhite(String text) {
        for(TextListener white : whites) {
            addToQueue(() -> white.onText(text));
        }
    }

    private List<TextListener> blacks = new ArrayList<>();

    public void addBlackListener(TextListener black) {
        blacks.add(black);
    }

    public void onBlack(String text) {
        for(TextListener black : blacks) {
            addToQueue(() -> black.onText(text));
        }
    }

    private List<TextListener> games = new ArrayList<>();

    public void addGameListener(TextListener game) {
        games.add(game);
    }

    public void onGame(String text) {
        for(TextListener game : games) {
            addToQueue(() -> game.onText(text));
        }
    }

    public final class RunnableQueue {

        private final ExecutorService m_executorService;
        private final Queue<Runnable> m_runnables;
        private final Runnable m_loop;

        public void stop() {
            m_executorService.shutdown();
            m_runnables.clear();
        }

        public RunnableQueue(ExecutorService executorService) {
            m_executorService = executorService;
            m_runnables = new LinkedList<Runnable>();

            m_loop = new Runnable() {
                public void run() {

                    Runnable l_runnable = current();

                    while(l_runnable != null) {
                        l_runnable.run();
                        l_runnable = next();
                    }
                }
            };
        }

        private Runnable current() {
            synchronized (m_runnables) {
                return m_runnables.peek();
            }
        }

        private Runnable next() {
            synchronized (m_runnables) {
                m_runnables.remove();
                return m_runnables.peek();
            }
        }

        public void enqueue(Runnable runnable) {
            if(runnable != null) {
                synchronized (m_runnables) {
                    m_runnables.add(runnable);
                    if(m_runnables.size() == 1) {
                        m_executorService.execute(m_loop);
                    }
                }
            }
        }
    }

}
