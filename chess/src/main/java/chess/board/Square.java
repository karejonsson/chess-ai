package chess.board;

import chess.board.pieces.Piece;

public class Square {

    public byte file;
    public byte rank;

    public String toString() {
        //System.out.println("#HEJ "+toStringExhaustive());
        return ""+((char)('A'+file))+((char)('1'+rank));
    }

    public String toStringExhaustive() {
        return "file "+file+", rank "+rank;
    }

    public Square clone() {
        Square out = new Square();
        out.file = file;
        out.rank = rank;
        return out;
    }

    public static void main(String[] args) {
        Square p = new Square((byte)0, (byte)1);
        System.out.println("P="+p);
        System.out.println("Icke initierad = "+(new Square()));
        System.out.println("Icke initierad = "+(new Square()).toStringExhaustive());
    }

    // Bort
    public Square(byte r, byte f) {
        file = f;
        rank = r;
    }

    // Bort
    public Square(Square p) {
        file = p.file;
        rank = p.rank;
    }

    public Square() {
        file = rank = 0;
    }

    public boolean onBoard() {
        return (file >= 0 && file < Board.files && rank >= 0 && rank < Board.ranks);
    }

    public boolean equals(Square p) {
        return (p != null && p.file == file && p.rank == rank);
    }

    public boolean isOn(int rank, int file) {
        return (this.file == file && this.rank == rank);
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Square) {
            return equals((Square) o);
        }
        return false;
    }

    @Override
    public final int hashCode() {
        return Board.files*rank+file;
    }

}