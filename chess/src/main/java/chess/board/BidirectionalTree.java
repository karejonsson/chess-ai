package chess.board;

import java.util.ArrayList;
import java.util.List;

public class BidirectionalTree<T> {
    public T current;
    public int value;
    public BidirectionalTree<T> previous;
    public List<BidirectionalTree<T>> successors = new ArrayList<>();
}
