package chess.board;

import chess.board.pieces.*;

public class Board {

    public static final byte chessStyle = 0;

    public static final byte ranks = chessStyle == 2 ? 10 : 8;
    public static final byte files = chessStyle == 0 ? 8 : 10;
    public static final byte pieces = chessStyle == 0 ? 6 : 8;

    public static final boolean WHITE = true;
    public static final boolean BLACK = false;

    public static final byte kingFile = files / 2;
    public static final byte castlingKingDistance = kingFile-2;
    public static final String whiteSignatureLetters = "PNBRQKCA";
    public static final String blackSignatureLetters = "pnbrqkca";
    public static final int maxrep = 3;

    public static void setupBaseline(Piece[] line, boolean color) {
        if(chessStyle == 0) {
            setupBaseline8(line, color);
            return;
        }
        setupBaseline10(line, color);
    }

    public static void setupBaseline8(Piece[] line, boolean color) {
        line[0] = new Rook(color);
        line[1] = new Knight(color);
        line[2] = new Bishop(color);
        line[3] = new Queen(color);
        line[4] = new King(color);
        line[5] = new Bishop(color);
        line[6] = new Knight(color);
        line[7] = new Rook(color);
    }

    public static void setupBaseline10(Piece[] line, boolean color) {
        line[0] = new Rook(color);
        line[1] = new Knight(color);
        line[2] = new Archbishop(color);
        line[3] = new Bishop(color);
        line[4] = new Queen(color);
        line[5] = new King(color);
        line[6] = new Bishop(color);
        line[7] = new Knight(color);
        line[8] = new Chancellor(color);
        line[9] = new Rook(color);
    }

    public static Piece getPiece(char c) {
        int idx = whiteSignatureLetters.indexOf(c);
        boolean color = Board.WHITE;
        if(idx == -1) {
            color = Board.BLACK;
            idx = blackSignatureLetters.indexOf(c);
        }
        if(idx == Pawn.idx) {
            return new Pawn(color);
        }
        if(idx == Knight.idx) {
            return new Knight(color);
        }
        if(idx == Bishop.idx) {
            return new Bishop(color);
        }
        if(idx == Rook.idx) {
            return new Rook(color);
        }
        if(idx == Queen.idx) {
            return new Queen(color);
        }
        if(idx == King.idx) {
            return new King(color);
        }
        if(idx == Chancellor.idx) {
            return new Chancellor(color);
        }
        if(idx == Archbishop.idx) {
            return new Archbishop(color);
        }
        return null;
    }


}
