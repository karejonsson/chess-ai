package chess.operations;

import chess.board.Board;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.King;
import chess.board.pieces.Pawn;
import chess.board.pieces.Piece;
import chess.board.pieces.Queen;

public class FenSerialization {

    // https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation

    public static String getBoardString(State state) {
        StringBuilder sb = new StringBuilder();
        for (int rank = Board.ranks-1; rank >= 0 ; rank--) {
            int emptycount = 0;
            for (int file = 0; file < Board.files ; file++) {
                Piece p = state.board[rank][file];
                if(p == null) {
                    emptycount++;
                }
                else {
                    if(emptycount > 0) {
                        sb.append(""+emptycount);
                    }
                    emptycount = 0;
                    sb.append(Serialization.getCharForSerialization(p));
                }
            }
            if(emptycount > 0) {
                sb.append(""+emptycount);
            }
            if(rank > 0) {
                sb.append("/");
            }
        }
        return sb.toString();
    }

    public static String getFENString(String internalSerialization) {
        return getFENString(Serialization.getState(internalSerialization));
    }

    public static String getFENString(State state) {
        StringBuilder sb = new StringBuilder();
        sb.append(getBoardString(state));
        sb.append(" ");
        sb.append(getActiveString(state));
        sb.append(" ");
        sb.append(getCastlingString(state));
        sb.append(" ");
        sb.append(getEnPassantString(state));
        sb.append(" ");
        sb.append(getHalfmoveString(state));
        sb.append(" ");
        sb.append(getFullmoveString(state));
        return sb.toString();
    }

    private static String getHalfmoveString(State state) {
        return ""+state.halfMoves;
    }

    private static String getFullmoveString(State state) {
        return ""+state.fullMoves;
    }

    private static String getEnPassantString(State state) {
        if(state.lastMove == null) {
            return "-";
        }
        Square enPassant = state.lastMove.enpKill;
        if(enPassant == null) {
            return "-";
        }
        Square s = enPassant.clone();
        if(s.rank == Board.ranks-3) {
            s.rank = Board.ranks-(byte)2;
        }
        if(s.rank == 3) {
            s.rank = (byte)2;
        }
        return s.toString().toLowerCase();
    }

    public static char getCharForWhiteSerialization(int idx) {
        return String.valueOf(Board.whiteSignatureLetters).charAt(idx);
    }

    public static char getCharForBlackSerialization(int idx) {
        return String.valueOf(Board.blackSignatureLetters).charAt(idx);
    }

    private static String getCastlingString(State state) {
        StringBuilder sb = new StringBuilder();
        if(!state.whiteKingMoved && !state.whiteKRookMoved) {
            sb.append(getCharForWhiteSerialization(King.idx));
        }
        if(!state.whiteKingMoved && !state.whiteQRookMoved) {
            sb.append(getCharForWhiteSerialization(Queen.idx));
        }
        if(!state.blackKingMoved && !state.blackKRookMoved) {
            sb.append(getCharForBlackSerialization(King.idx));
        }
        if(!state.blackKingMoved && !state.blackQRookMoved) {
            sb.append(getCharForBlackSerialization(Queen.idx));
        }
        String out = sb.toString();
        if(out.length() == 0) {
            return "-";
        }
        return out;
    }

    private static String getActiveString(State state) {
        if(state.turn) {
            return "w";
        }
        return "b";
    }

    public static void main(String[] args) {
        State s = Initiation.createStandardInitialState();
        System.out.println("Init "+ getFENString(s));
        String internSer = "__R__RK__P___PPPP_________Q____B_B__N____p_q___pp__pn_p__k___b_r:FTFTTTFTFF:F3E5F____:0010";
        s = Serialization.getState(internSer);
        System.out.println("Some       "+ getFENString(s));
        System.out.println("Some again "+ getFENString(internSer));

    }

    // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
    // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - ? 1
}
