package chess.operations;

import chess.board.Board;
import chess.board.Move;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.Piece;

public class Getters {

    public static Square getSquare(byte straight) {
        return new Square((byte)(straight / Board.files), (byte)(straight % Board.files));
    }

    public static Piece getPiece(State state, Square sq) { // --
        return state.board[sq.rank][sq.file];
    }

    public static Piece getPiece(State state, int rank, int file) {
        return state.board[rank][file];
    }

    public static Piece getPiece(State state, int straight) {
        return state.board[straight / Board.files][straight % Board.files];
    }

    public static Square getKing(State state, boolean which) {
        return which ? state.whiteKing.clone() : state.blackKing.clone();
    }

    public static boolean isInitial(State state) {
        return state.previous == null;
    }

    public static boolean isOccupied(State state, Square p) {
        return state.board[p.rank][p.file] != null; // --
    }

    public static boolean isOccupied(State state, int rank, int file) {
        return state.board[rank][file] != null;
    }

    public static Move getLastMove(State state) {
        return state.lastMove;
    }

    public static final String whiteSignatureLetters = Board.whiteSignatureLetters.replace("P", " ");

    public static String getCharForMoveNotation(Piece piece) {
        return String.valueOf(whiteSignatureLetters.charAt(piece.index()));
    }

}
