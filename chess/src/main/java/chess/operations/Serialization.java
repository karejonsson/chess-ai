package chess.operations;

import chess.board.Board;
import chess.board.Move;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.*;

public class Serialization {

    public static char getCharForSerialization(Piece piece) {
        if(piece.getColor()) {
            return String.valueOf(Board.whiteSignatureLetters).charAt(piece.index());
        }
        return String.valueOf(Board.blackSignatureLetters).charAt(piece.index());
    }

    public static String getBoardString(State state) {
        StringBuilder sb = new StringBuilder();
        for (int rank = 0; rank < Board.ranks ; rank++) {
            for (int file = 0; file < Board.files ; file++) {
                Piece p = state.board[rank][file];
                sb.append(p == null ? "_" : getCharForSerialization(p));
            }
        }
        return sb.toString();
    }

    public static void addBoard(State state, String value) {
        state.board = new Piece[Board.ranks][Board.files];
        state.wPieces = new byte[Board.pieces];
        state.bPieces = new byte[Board.pieces];
        state.zobrist = new ZobristHashing(2*Board.pieces, Board.ranks*Board.files);
        for (byte rank = 0; rank < Board.ranks ; rank++) {
            for (byte file = 0; file < Board.files ; file++) {
                Piece p = Board.getPiece(value.charAt(rank * Board.files + file));
                state.board[rank][file] = p;
                if(p != null) {
                    if(p.getColor()) {
                        state.wPieces[p.index()]++;
                    }
                    else {
                        state.bPieces[p.index()]++;
                    }
                    if(p.index() == King.idx) {
                        if(p.getColor()) {
                            state.whiteKing = new Square(rank, file);
                        }
                        else {
                            state.blackKing = new Square(rank, file);
                        }
                    }
                }
            }
        }
    }

    public static String getCastlingString(State state) {
        StringBuilder sb = new StringBuilder();
        sb.append(getBooleanString(state.turn));
        sb.append(getBooleanString(state.whiteKingMoved));
        sb.append(getBooleanString(state.whiteKRookMoved));
        sb.append(getBooleanString(state.whiteQRookMoved));
        sb.append(getBooleanString(state.whiteHasCastled));
        sb.append(getBooleanString(state.blackKingMoved));
        sb.append(getBooleanString(state.blackKRookMoved));
        sb.append(getBooleanString(state.blackQRookMoved));
        sb.append(getBooleanString(state.blackHasCastled));
        sb.append(getBooleanString(state.check));
        return sb.toString();
    }

    public static void addCastling(State state, String value) {
        StringBuilder sb = new StringBuilder();
        state.turn = getBoolean(value.substring(0, 1));
        state.whiteKingMoved = getBoolean(value.substring(1, 2));
        state.whiteKRookMoved = getBoolean(value.substring(2, 3));
        state.whiteQRookMoved = getBoolean(value.substring(3, 4));
        state.whiteHasCastled = getBoolean(value.substring(4, 5));
        state.blackKingMoved = getBoolean(value.substring(5, 6));
        state.blackKRookMoved = getBoolean(value.substring(6, 7));
        state.blackQRookMoved = getBoolean(value.substring(7, 8));
        state.blackHasCastled = getBoolean(value.substring(8, 9));
        state.check = getBoolean(value.substring(9, 10));
    }

    public static String getBooleanString(boolean value) {
        if(value) {
            return "T";
        }
        return "F";
    }

    public static String getCharString(char value) {
        if(value == 0) {
            return "_";
        }
        return ""+value;
    }

    public static char getChar(String value) {
        if(value == null) {
            return 0;
        }
        if(value.equalsIgnoreCase("_")) {
            return 0;
        }
        return value.charAt(0);
    }

    public static boolean getBoolean(String value) {
        if(value.equalsIgnoreCase("t")) {
            return true;
        }
        return false;
    }

    public static final String hc = "0123456789ABCDEFGHIJKLMNOPQRTSUVWXYZ";

    public static String getInHex(int nr) {
        int top = nr / 36;
        int btn = nr % 36;
        return hc.substring(top, top+1)+hc.substring(btn, btn+1);
    }

    public static int getInDec(String hex) {
        return 36*hc.indexOf(hex.charAt(0))+hc.indexOf(hex.charAt(1));
    }

    public static String getStateString(State state) {
        StringBuilder sb = new StringBuilder();
        sb.append(getBoardString(state));
        sb.append(":");
        sb.append(getCastlingString(state));
        sb.append(":");
        sb.append(getMoveString(state.lastMove));
        sb.append(":");
        sb.append(getInHex(state.halfMoves));
        sb.append(getInHex(state.fullMoves));
        return sb.toString();
    }

    // R__QK_NRPPP__PPP__NB____p___P_____rP__B________p_pppppp__nbqkbnr:TFFFFFFTFF:H7H6F____
    // RNBQ___RPPP_KPPP___________Nn______pP_____p_____p_pb_pppr__qkb_r:FTFFFFFFFF:E1E2F____:0109

    public static State getState(String state) {
        State out = new State();
        String[] pieces = state.split(":");
        addBoard(out, pieces[0]);
        addCastling(out, pieces[1]);
        out.lastMove = getMove(pieces[2]);
        out.halfMoves = getInDec(pieces[3].substring(0, 2));
        out.fullMoves = getInDec(pieces[3].substring(2));
        out.lastMove.state = out;
        return out;
    }

    public static String getMoveString(Move move) {
        if(move == null) {
            return "_________";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getSquareString(move.from));
        sb.append(getSquareString(move.to));
        sb.append(getBooleanString(move.promotion));
        sb.append(getCharString(move.promoteTo));
        sb.append(getCharString(move.castle));
        sb.append(getSquareString(move.enpKill));
        return sb.toString();
    }

    public static Move getMove(String move) {
        Move m = new Move();
        m.from = getSquare(move.substring(0, 2));
        m.to = getSquare(move.substring(2, 4));
        m.promotion = getBoolean(move.substring(4, 5));
        m.promoteTo = getChar(move.substring(5, 6));
        m.castle = getChar(move.substring(6, 7));
        m.enpKill = getSquareIfValid(move.substring(7, 9));
        return m;
    }

    public static String getSquareString(Square square) {
        if(square == null) {
            return "__";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(""+((char)('A'+square.file))+((char)('1'+square.rank)));
        return sb.toString();
    }

    public static Square getSquare(String square) {
        Square out = new Square();
        out.file = (byte)(square.charAt(0)-'A');
        out.rank = (byte)(square.charAt(1)-'1');
        return out;
    }

    public static Square getSquareIfValid(String square) {
        Square out = new Square();
        out.file = (byte)(square.charAt(0)-'A');
        if(out.file < 0 || out.file >= Board.files) {
            return null;
        }
        out.rank = (byte)(square.charAt(1)-'1');
        if(out.rank < 0 || out.rank >= Board.ranks) {
            return null;
        }
        return out;
    }

    public static String getBoardAsStringWithLinebreaksAndTheWholeMidevitt(State state) {
        StringBuilder sb = new StringBuilder();
        for (int rank = Board.ranks-1; rank >= 0 ; rank--) {
            for (int file = 0; file < Board.files ; file++) {
                Piece p = state.board[rank][file];
                if(p == null) {
                    sb.append(" ");
                    continue;
                }
                boolean color = p.getColor();
                String t = Getters.getCharForMoveNotation(p);
                if(t.trim().length() == 0) {
                    t = "P";
                }
                sb.append(color ? t : t.toLowerCase());
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String state = "RNBQKBNRPPPP_PPP____________P_____p_____________pp_ppppprnbqkbnr:TFFFFFFFFF:C7C5F____:02JE";
        State s = getState(state);
        String s2 = getStateString(s);
        System.out.println(state+"\n"+s2);
        System.out.println("Lika? "+(state.equals(s2)));
        for(int i = 0 ; i < 36*36 ; i++) {
            if(i != getInDec(getInHex(i))) {
                System.out.println("FEL "+i);
            }
        }
    }

}
