package chess.operations;

import chess.board.*;
import chess.board.pieces.*;
import chess.rules.Attempt;

import java.util.Map;

public class Moving {

    public static void real(final Match board, final Move m) {
        State out = Attempt.move(board.state, m);
        updateCounts(board.state, out, m);
        Piece piece = Getters.getPiece(board.state, m.from);
        piece.moves++;
        out.getLegalMoves();
        board.state.clearLegalMoves(); // Annars släpps inget minne. Detta är framtidsriktningen
        board.state = out;

        updateRepetitions(out, board.countRepetitions);
    }

    public static void updateRepetitions(State out, Map<Integer, Byte> countRepetitions) {
        int hash = out.zobrist.hashCode();
        Byte i = countRepetitions.get(hash);
        if(i == null) {
            countRepetitions.put(hash, (byte)1);
            out.repetitions = 1;
            //System.out.println(pos+", rep 1");
            return;
        }
        i++;
        countRepetitions.put(hash, i);
        out.repetitions = i;
    }

    public static void updateCounts(State prev, State next, Move move) {
        Piece onSource = Getters.getPiece(prev, move.from);
        Piece onTarget = Getters.getPiece(prev, move.to);
        boolean reset = (onSource.index() == Pawn.idx /* pawnmove */ || onTarget != null /* capture */ );
        next.halfMoves = reset ? 0 : prev.halfMoves+1;
        next.fullMoves = prev.fullMoves+(!next.turn ? 1 : 0);
    }

    public static State attempted(State state, Move m, Map<Integer, Byte> countRepetitions) {
        //System.out.println("moveCleansed="+moveCleansed+", pieceChar="+pieceChar);
        State out = Attempt.move(state, m);
        int hash = out.zobrist.hashCode();
        Byte reps = countRepetitions.get(hash);
        if(reps == null) {
            out.repetitions = 1;
        }
        else {
            out.repetitions = (byte)(reps+1);
        }

        updateCounts(state, out, m);

        out.getLegalMoves();
        return out;
    }

    public static State _attemptedPrepareForStaticAnalysis(State state, Move m, Map<Integer, Byte> countRepetitions) {
        State out = Attempt.move(state, m);
        int hash = out.zobrist.hashCode();
        Byte reps = countRepetitions.get(hash);
        if(reps == null) {
            out.repetitions = 1;
        }
        else {
            out.repetitions = (byte)(reps+1);
        }
        out.prepareForStaticAnalysis();
        return out;
    }

    /*
    public static State attempted(State state, Move m) {
        State out = Attempt.move(state, m);
        out.getLegalMoves();
        return out;
    }
    */

}
