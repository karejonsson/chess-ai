package chess.operations;

import chess.board.State;
import chess.board.pieces.Piece;

public class Setters {

    public static void setPiece(Piece piece, State state, int rank, int file) {
        state.board[rank][file] = piece;
    }

}
