package chess.operations;

import chess.board.Board;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.*;

public class Initiation {

    private final static byte[] initCount = new byte[] { 8, 2, 2, 2, 1, 1 };

    private static void setupStandardPiececount(byte[] count) {
        System.arraycopy(initCount, 0, count, 0, initCount.length);
    }

    private static void setupBoardStandardInitialPosition(Piece[][] contents) {
        int i;
        // erase pieces from middle
        byte rank,file;
        for (rank = 2 ; rank < Board.ranks-2 ; rank++) {
            for (file = 0 ; file < Board.files ; file ++) {
                contents[rank][file] = null;
            }
        }

        // pawns
        for(i = 0 ; i < Board.files ; i++) {
            contents[1][i] = new Pawn(Board.WHITE);
            contents[Board.ranks-2][i] = new Pawn(Board.BLACK);
        }

        Board.setupBaseline(contents[0], Board.WHITE);
        Board.setupBaseline(contents[Board.ranks-1], Board.BLACK);
    }

    private static void initialCastlingState(State state) {
        state.whiteKing = new Square((byte)0, (byte)(Board.files/2));
        state.blackKing = new Square((byte)(Board.ranks-1), (byte)(Board.files/2));
        state.whiteKingMoved = false;
        state.blackKingMoved = false;
        state.whiteKRookMoved = false;
        state.whiteQRookMoved = false;
        state.blackKRookMoved = false;
        state.blackQRookMoved = false;
    }

    public static State createStandardInitialState() {
        State state = new State();
        initialCastlingState(state);
        state.turn = Board.WHITE;
        state.zobrist = new ZobristHashing(2*Board.pieces, Board.ranks*Board.files);

        // Set up the board
        state.board = new Piece[Board.ranks][Board.files]; // Sammanblandning?
        setupBoardStandardInitialPosition(state.board);
        state.wPieces = new byte[Board.pieces];
        setupStandardPiececount(state.wPieces);
        state.bPieces = new byte[Board.pieces];
        setupStandardPiececount(state.bPieces);
        return state;
    }

    public static State clone(State in) {
        State out = new State();
        out.turn = in.turn;

        out.board = new Piece[Board.ranks][Board.files];
        for (int rank = 0; rank < Board.ranks ; rank++) {
            System.arraycopy(in.board[rank], 0, out.board[rank], 0, Board.files);
        }

        out.wPieces = new byte[Board.pieces];
        System.arraycopy(in.wPieces, 0, out.wPieces, 0, Board.pieces);
        out.bPieces = new byte[Board.pieces];
        System.arraycopy(in.bPieces, 0, out.bPieces, 0, Board.pieces);

        out.whiteKing = in.whiteKing.clone();
        out.blackKing = in.blackKing.clone();

        // Castling info
        out.whiteKingMoved = in.whiteKingMoved;
        out.whiteKRookMoved = in.whiteKRookMoved;
        out.whiteQRookMoved = in.whiteQRookMoved;
        out.whiteHasCastled = in.whiteHasCastled;
        out.blackKingMoved = in.blackKingMoved;
        out.blackKRookMoved = in.blackKRookMoved;
        out.blackQRookMoved = in.blackQRookMoved;
        out.blackHasCastled = in.blackHasCastled;

        out.check = in.check;
        out.zobrist = new ZobristHashing(in.zobrist);

        return out;
    }

}
