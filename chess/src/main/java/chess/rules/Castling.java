package chess.rules;

import chess.board.Board;
import chess.board.Square;
import chess.board.State;
import chess.operations.Getters;

public class Castling {

    // Does not check for checks in the path
    public static boolean canCastleKingside(State state, boolean color) {
        boolean value = true;

        if (color == Board.WHITE) {
            if (state.whiteKingMoved || state.whiteKRookMoved
                    || Getters.isOccupied(state, new Square((byte)0,(byte)(Board.kingFile+1))) // Bredvid kungen
                    || Getters.isOccupied(state, new Square((byte)0,(byte)(Board.kingFile+2)))) // Två steg bredvid kungen
                // För Capablanca-schack behövs en kontroll till här
            {
                value = false;
            }
        }
        else if (color == Board.BLACK) {
            if (state.blackKingMoved || state.blackKRookMoved
                    || Getters.isOccupied(state, new Square((byte)(Board.ranks-1),(byte)(Board.kingFile+1))) // Bredvid kungen
                    || Getters.isOccupied(state, new Square((byte)(Board.ranks-1),(byte)(Board.kingFile+2)))) // Två steg bredvid kungen
            // För Capablanca-schack behövs en kontroll till här
            {
                value = false;
            }
        }
        return value;
    }

    public static boolean canCastleQueenside(State state, boolean color) {
        boolean value = true;

        if (color == Board.WHITE) {
            if (state.whiteKingMoved || state.whiteQRookMoved
                    || Getters.isOccupied(state, new Square((byte)0,(byte)(Board.kingFile-1)))
                    || Getters.isOccupied(state, new Square((byte)0,(byte)(Board.kingFile-2)))
                    || Getters.isOccupied(state, new Square((byte)0,(byte)(Board.kingFile-3))))
            // För Capablanca-schack behövs en kontroll till här
            {
                value = false;
            }
        }
        else if (color == Board.BLACK) {
            if (state.blackKingMoved || state.blackQRookMoved
                    || Getters.isOccupied(state, new Square((byte)(Board.ranks-1),(byte)(Board.kingFile-1)))
                    || Getters.isOccupied(state, new Square((byte)(Board.ranks-1),(byte)(Board.kingFile-2)))
                    || Getters.isOccupied(state, new Square((byte)(Board.ranks-1),(byte)(Board.kingFile-3))))
            // För Capablanca-schack behövs en kontroll till här
            {
                value = false;
            }
        }
        return value;
    }

    public static boolean canCastle(State state, boolean color, char side) {
        if(side == 'K') {
            return canCastleKingside(state, color);
        }
        return canCastleQueenside(state, color);
    }

}
