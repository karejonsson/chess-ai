package chess.rules;

import chess.board.*;
import chess.board.pieces.*;
import chess.operations.Getters;

import java.util.ArrayList;
import java.util.List;

public class Moves {

    public static ArrayList<Move> findMoves(final State state) {
        return findMoves(state, state.lastMove);
    }

    public static void addMoves(final State state, ArrayList<Move> legalMoves, ArrayList<Move> opponentMoves) {
        addBothPlayersMoves(legalMoves, opponentMoves, state, state.lastMove);
    }

    private static ArrayList<Move> findMoves(final State state, final Move previous) {
        ArrayList<Move> legalMoves = new ArrayList<>();
        addMoves(legalMoves, state, previous);
        return legalMoves;
    }

    public static void addMoves(ArrayList<Move> legalMoves, final State state, final Move previous) {
        byte rank,file;
        Square temp = new Square();

        for(rank = 0 ; rank < Board.ranks ; rank++) {
            for(file = 0; file < Board.files; file++) {
                temp.rank = rank;
                temp.file = file;
                Piece piece = Getters.getPiece(state, rank, file);
                if(piece == null) {
                    continue;
                }
                if(piece.getColor() == state.turn) {
                    List<Move> v = piece.getMovesDisregardingChecks(temp, state, previous);
                    for(Move m : v) {
                        if(Check.isCheckLegal(state, m, previous)) {
                            legalMoves.add(m);
                        }
                    }
                }
            }
        }
    }

    public static void addBothPlayersMoves(ArrayList<Move> legalMoves, ArrayList<Move> opponentMoves, final State state, final Move previous) {
        Square temp = new Square();

        for(byte rank = 0 ; rank < Board.ranks ; rank++) {
            for(byte file = 0; file < Board.files; file++) {
                temp.rank = rank;
                temp.file = file;
                Piece piece = Getters.getPiece(state, rank, file);
                if(piece == null) {
                    continue;
                }
                if(piece.getColor() == state.turn) {
                    ArrayList<Move> moves = piece.getMovesDisregardingChecks(temp, state, previous);
                    for(Move m : moves) {
                        if(Check.isCheckLegal(state, m, previous)) {
                            legalMoves.add(m);
                        }
                    }
                }
                else {
                    ArrayList<Move> moves = piece.getMovesDisregardingChecksAndTurn(temp, state, previous);
                    for(Move m : moves) {
                        if(Check.isCheckLegal(state, m, previous)) {
                            opponentMoves.add(m);
                        }
                    }
                }
            }
        }
    }

    // previous for en passant
    public static boolean isLegal(State state, Move m, Move previous) {
        boolean value = false;
        Piece piece = Getters.getPiece(state, m.from);
        List<Move> v = piece.getMovesDisregardingChecks(m.from, state, previous);
        for(Move temp : v) {
            if(value) {
                break;
            }
            if ( m.equals(temp)) {
                value = true;
                // if move is en passant, then the capture must be added
                if (temp.enpKill != null) {
                    m.enpKill = new Square(temp.enpKill);
                }

                // if move is promotion
                if (temp.promotion) {
                    m.promotion = true;
                }
                // if move is castling
                m.setCastle(temp.getCastle());
            }
        }
        return value;
    }

}
