package chess.rules;

import chess.board.Board;
import chess.board.Move;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.*;
import chess.operations.Getters;
import chess.operations.Initiation;
import chess.operations.Serialization;
import chess.operations.ZobristHashing;

public class Attempt {

    public static State move(final State in, final Move m) {
        State out = Initiation.clone(in);

        Piece piece = Getters.getPiece(in, m.from);
        boolean complexMove = false;
        out.previous = in;
        //System.out.println("m="+m);
        //System.out.println("out.board="+out.board);
        out.board[m.to.rank][m.to.file] = out.board[m.from.rank][m.from.file];
        out.zobrist.add(piece, m.to);
        // check for castling
        if (m.getCastle() != (char) 0) {
            complexMove = true;
            int homerank = m.from.rank;
            int dx = m.getCastle() == 'Q' ? -1 : 1;

            // move the king
            out.board[homerank][Board.kingFile+Board.castlingKingDistance*dx] = out.board[homerank][Board.kingFile];
            out.board[homerank][Board.kingFile] = null;

            // move the rook
            out.board[homerank][Board.kingFile+(Board.castlingKingDistance-1)*dx] = out.board[homerank][((Board.files-1)*dx+(Board.files-1))/2];
            out.board[homerank][((Board.files-1)*dx + (Board.files-1))/2] = null;

            // mark castling done
            if(in.turn) {
                out.whiteHasCastled = true;
            }
            else {
                out.blackHasCastled = true;
            }
        }

        // check for en Passant
        if (m.enpKill != null) {
            complexMove = true;
            out.board[m.enpKill.rank][m.enpKill.file] = null;
        }

        // check for promotion
        if (m.promotion) {
            complexMove = true;
            // The AI does automatic promotion to Queen
            switch(m.promoteTo) {
                case 'Q': out.board[m.to.rank][m.to.file] = new Queen(out.turn);
                    break;
                case 'B': out.board[m.to.rank][m.to.file] = new Bishop(out.turn);
                    break;
                case 'R': out.board[m.to.rank][m.to.file] = new Rook(out.turn);
                    break;
                case 'N': out.board[m.to.rank][m.to.file] = new Knight(out.turn);
                    break;
                default: out.board[m.to.rank][m.to.file] = new Queen(out.turn);
            }
        }

        // Check if the King or a Rook moved (castling purposes)
        if (m.from.equals(new Square((byte)0,(byte)Board.kingFile))) {
            out.whiteKingMoved = true;
        }
        if (m.from.equals(new Square((byte)0,(byte)0))) {
            out.whiteQRookMoved = true;
        }
        if (m.from.equals(new Square((byte)0,(byte)(Board.files-1)))) {
            out.whiteKRookMoved = true;
        }
        if (m.from.equals(new Square((byte)(Board.ranks-1),(byte)Board.kingFile))) {
            out.blackKingMoved = true;
        }
        if (m.from.equals(new Square((byte)(Board.ranks-1),(byte)0))) {
            out.blackQRookMoved = true;
        }
        if (m.from.equals(new Square((byte)(Board.ranks-1),(byte)(Board.files-1)))) {
            out.blackKRookMoved = true;
        }

        // Add the move to the list of moves
        out.lastMove = m;
        m.state = out;

        // If the king has moved, update its position
        if (m.from.equals(Getters.getKing(out, Board.WHITE))) {
            out.whiteKing.rank = m.to.rank;
            out.whiteKing.file = m.to.file;
        }
        if (m.from.equals(Getters.getKing(out, Board.BLACK))) {
            out.blackKing.rank = m.to.rank;
            out.blackKing.file = m.to.file;
        }

        // change player
        out.turn = !out.turn;

        // count down if piece captured
        if(m.captures()) {
            Piece p = m.captured();
            if(p == null) {
                System.out.println(Serialization.getBoardString(m.state.previous));
            }
            (out.turn ? out.wPieces : out.bPieces)[p.index()]--;
            out.zobrist.remove(p, m.to);
        }

        // empty the square that was moved from
        out.board[m.from.rank][m.from.file] = null;
        if(complexMove) {
            // Recalculate Zobrist hash
            out.zobrist.recalculate(out);
        }
        else {
            out.zobrist.remove(piece, m.from);
        }
        out.check = Check.isInCheck(out);
        return out;
    }

}
