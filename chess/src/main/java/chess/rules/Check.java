package chess.rules;

import chess.board.Board;
import chess.board.Move;
import chess.board.Square;
import chess.board.State;
import chess.operations.Getters;
import chess.operations.Initiation;
import chess.operations.Moving;

import java.util.Vector;

public class Check {

    public static boolean isCheckLegal(State in, Move m, Move previous) {
        boolean returnValue = false;
        State temp = Initiation.clone(in);
        // The function is simple if not castling
        if (m.getCastle() == (char) 0) {
            temp = Attempt.move(temp, m);
            if(isInCheck(temp, !temp.turn, previous)) {
                m.setMessage("Move results in Check!");
            }
            else {
                returnValue = true;
            }
        }
        else {
            // if castling, check the intermediate squares
            byte homerank = (byte)((in.turn == Board.WHITE ? 0 : (Board.ranks-1)));
            byte dx = (byte)((m.getCastle() == 'Q') ? -1 : 1);

            // If not in immediate check
            if(!isInCheck(temp, in.turn, previous)) {
                temp = Attempt.move(temp, new Move(new Square(homerank, Board.kingFile),new Square(homerank,(byte)(Board.kingFile+dx))));
                // If the middle square is not attacked
                if(!isInCheck(temp, in.turn, previous)) {
                    temp = Attempt.move(temp, new Move(new Square(homerank,(byte)(Board.kingFile+dx)),new Square(homerank,(byte)(Board.kingFile+Board.castlingKingDistance*dx))));
                    // If the final square is not attacked
                    if (!isInCheck(temp, in.turn, previous)) {
                        returnValue = true;
                    }
                }
            }
        }
        return returnValue;
    }

    public static boolean isMate(State state) {
        return isInCheck(state) && state.getLegalMoves().size() == 0;
    }

    public static boolean isStaleMate(State state) {
        return !isInCheck(state) && state.getLegalMoves().size() == 0;
    }

    public static boolean isInCheck(State state) {
        return isInCheck(state, state.turn, state.lastMove);
    }

    public static boolean isInCheck(State state, boolean t, Move previous) {
        byte rank, file;
        boolean value = false;
        Vector v;
        Square from = new Square();

        // if it is t's turn, then make it temporarily !t's turn
        if (state.turn == t) {
            state = Initiation.clone(state);
            state.turn = !t;
        }

        // Get all legal moves (without worrying about checks)
        for (rank = 0 ; rank < Board.ranks ; rank++) {
            for (file = 0 ; file < Board.files ; file++) {
                from.rank = rank;
                from.file = file;

				// for each square occupied by (turn), see if it is legal to
				// move from the square to the opponents king
                // Do not include castling as a way of capture opponents king
                if (Getters.isOccupied(state, from) && Getters.getPiece(state, from).getColor() == state.turn
                        && Moves.isLegal(state, new Move(from, Getters.getKing(state, t)), previous)) {
                    value = true;
                }
            }
        }

        return value;
    }

}
