package chess.execute;

import chess.training.NetworkGenesis;
import chess.training.Training101;
import chess.trainingdata.EvaluatePositions;
import chess.trainingdata.MakeSets;
import general.reuse.properties.HarddriveProperties;

public class Main {

    private static HarddriveProperties hp = new HarddriveProperties("en.symbol", "/Finns/ej/application.properties");

    public static void main(String[] args) throws Exception {
        String whatToDo = hp.getString("execute", null);
        if(whatToDo == null || whatToDo.trim().length() == 0) {
            System.out.println("Inget att göra!");
            return;
        }
        whatToDo = whatToDo.trim();
        if(whatToDo.toLowerCase().contains("eval")) {
            EvaluatePositions.main(args);
            return;
        }
        if(whatToDo.toLowerCase().contains("make")) {
            MakeSets.main(args);
            return;
        }
        if(whatToDo.toLowerCase().contains("genes")) {
            NetworkGenesis.main(args);
            return;
        }
        if(whatToDo.toLowerCase().contains("train")) {
            Training101.main(args);
            return;
        }
    }

}
