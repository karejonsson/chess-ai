package chess.trainingdata;

import chess.board.Move;
import chess.board.State;
import chess.operations.FenSerialization;
import chess.operations.Moving;
import chess.operations.Serialization;
import chess.rules.Attempt;
import chess.trainingdata.engines.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.UnaryOperator;

import general.reuse.properties.HarddriveProperties;

public class EvaluatePositions {

    private static HarddriveProperties hp = new HarddriveProperties("en.symbol", "/Finns/ej/evaluation.properties");

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        String infile = hp.getString("evaluation.file.in", null);
        File reproducedFile = new File(infile);
        if(!reproducedFile.exists()) {
            System.out.println("Infilen finns inte. infile="+infile);
            System.exit(-1);
        }
        File goodnessesFile = new File(hp.getString("evaluation.file.out", null));
        File stateFile = new File(hp.getString("evaluation.file.state", null));

        handle(reproducedFile, goodnessesFile, stateFile);
    }

    private static void handle(File reproducedFile, File goodnessesFile, File stateFile) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        if(!reproducedFile.exists()) {
            System.out.println("Mysko, filen "+goodnessesFile.getAbsolutePath()+" finns inte.");
            return;
        }
        if(stateFile.exists()) {
            System.out.println("Tillståndsfilen finns. Fortsätter enligt vad som står där.");
            handleFromWhereInterruptedByStatefile(reproducedFile, goodnessesFile, stateFile);
            return;
        }
        else {
            System.out.println("Tillståndsfilen finns INTE.");
        }

        if(!goodnessesFile.exists()) {
            System.out.println("Godhetsfilen saknas. Börjar från början.");
            // Filen skall bearbetas från början
            goodnessesFile.createNewFile();
            //FileOutputStream goodnessesFos = new FileOutputStream(goodnessesFile);
            FileWriter goodnessesFW = new FileWriter(goodnessesFile);
            handleFromBeginning(reproducedFile, goodnessesFW, stateFile);
            return;
        }
        else {
            System.out.println("Godhetsfilen finns sedan tidigare");
            //FileWriter fw = new FileWriter(goodnessesFile, true);
            handleFromWhereInterruptedByCreatedGoodnesses(reproducedFile, goodnessesFile, stateFile);
            return;
        }
    }

    private static void handleFromWhereInterruptedByStatefile(File reproducedFile, File goodnessesFile, File stateFile) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        FileReader stateFR = new FileReader(stateFile);
        BufferedReader stateBR = new BufferedReader(stateFR);
        String key = stateBR.readLine().trim();
        stateBR.close();
        stateFR.close();
        System.out.println("Nyckel från tillståndsfilen: key="+key);
        setFileReadingAtCorrectPlace(reproducedFile, key, goodnessesFile, stateFile);
    }

    private static void handleFromBeginning(File reproducedFile, FileWriter goodnessesFW, File stateFile) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        FileReader reproducedFR = new FileReader(reproducedFile);
        BufferedReader reproducedBR = new BufferedReader(reproducedFR);
        handleFromReadersCurrent(reproducedBR, goodnessesFW, stateFile);
        reproducedBR.close();
        reproducedFR.close();
    }

    private static void handleFromWhereInterruptedByCreatedGoodnesses(File reproducedFile, File goodnessesFile, File stateFile) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        FileReader goodnessesFR = new FileReader(goodnessesFile);
        BufferedReader goodnessesBR = new BufferedReader(goodnessesFR);
        String line = null;
        String lastValidLine = null;
        while ((line = goodnessesBR.readLine()) != null) {
            if (line != null && line.contains("|")) {
                lastValidLine = line;
            }
        }
        String[] parts = lastValidLine.split("\\|");
        String[] id = parts[0].split(":");
        String key = id[0] + ":" + id[1] + ":" + id[2];
        System.out.println("Nyckel " + key);
        setFileReadingAtCorrectPlace(reproducedFile, key, goodnessesFile, stateFile);
    }

    public static void setFileReadingAtCorrectPlace(File reproducedFile, String key, File goodnessesFile, File stateFile) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        FileReader reproducedFR = new FileReader(reproducedFile);
        BufferedReader reproducedBR = new BufferedReader(reproducedFR);
        String line = null;
        int ctr = 0;
        while((line = reproducedBR.readLine()) != null) {
            ctr++;
            // Fortsätt fram till det vi söker
            if(line.contains(key)) {
                break;
            }
        }
        System.out.println("Framme: "+line+", genomsökte "+ctr+" rader.");
        FileWriter goodnessesFW = new FileWriter(goodnessesFile, true);
        handleFromReadersCurrent(reproducedBR, goodnessesFW, stateFile);
    }

    //public static final long millisToEvaluate = 60000;

    private static void handleFromReadersCurrent(BufferedReader reproducedBR, FileWriter goodnessesFW, File stateFile) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        String line = null;
        CmdClient client = new CmdClient();
        client.start(hp.getString("evaluation.uci.exec", ""));
        long millisToEvaluate = hp.getLong("evaluation.millis", 60000l);
        String host = hp.getString("evaluation.host", "");
        String engine = hp.getString("evaluation.uci.version", "");
        // We initialise the engine to use the UCI interface
        client.command("uci", UnaryOperator.identity(), (s) -> s.startsWith("uciok"), 2000l);

        while((line = reproducedBR.readLine()) != null) {
            System.out.println("RAD '"+line+"'");
            String[] parts = line.split("\\|");
            //System.out.println("Antal delar '"+parts.length+"'");
            String[] id = parts[0].split(":");
            String colorS = id[1];
            int moveNr = Integer.parseInt(id[2]);
            if(moveNr == 0) {
                continue;
            }
            boolean turn = colorS.equalsIgnoreCase("W");
            State s = Serialization.getState(parts[1]);
            if(!turn) {
                s = ChangeColor.turn(s);
            }
            List<String> moveScores = ExternalClassifyer.evaluate(client, FenSerialization.getFENString(s), millisToEvaluate);

            String pattern = "yyyy/MM/dd-HH:mm"; // 20220323-0000
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");

            for(String moveScore : moveScores) {
                String[] moveScoreParts = moveScore.split(":");
                Move m = getMove(s.getLegalMoves(), moveScoreParts[0]);
                if(m == null) {
                    // Något är fel. Draget fanns inte.
                    System.out.println("Fel vid : "+parts[0]+":"+moveScore.toUpperCase()+":"+millisToEvaluate+"|"+Serialization.getStateString(s)+" <- ställningen innan");
                    continue;
                }
                State out = Attempt.move(s, m);
                Moving.updateCounts(s, out, m);

                String time = simpleDateFormat.format(new Date());
                goodnessesFW.write(parts[0]+":"+moveScore.toUpperCase()+":"+millisToEvaluate+":"+engine+":"+host+":"+time+"|"+Serialization.getStateString(out)+"\n");
                goodnessesFW.flush();
            }

            String key = id[0] + ":" + id[1] + ":" + id[2];

            FileWriter stateFW = new FileWriter(stateFile, false);
            stateFW.write(key);
            stateFW.close();
        }

        client.close();
    }

    public static Move getMove(List<Move> possibleMoves, String move) {
        for(Move possibleMove  : possibleMoves) {
            if(MatchMoveToNotation.isSame_TwoSquaresLower(possibleMove, move)) {
                return possibleMove;
            }
            //System.out.println(move+" != "+possibleMove);
        }
        return null;
    }

}
