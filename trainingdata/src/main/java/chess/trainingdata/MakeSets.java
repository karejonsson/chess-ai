package chess.trainingdata;

import chess.board.State;
import chess.operations.Serialization;
import chess.trainingdata.encoders.BoardEncoder;
import chess.trainingdata.encoders.BoardEncoderFactory;
import chess.trainingdata.encoders.Encode786;
import general.reuse.properties.HarddriveProperties;
import se.prv.ai.mutating.runtime.functions.Function1Arg;
import se.prv.ai.mutating.training.data.TDArray;
import se.prv.ai.mutating.training.data.TDImmutableSet;
import se.prv.ai.mutating.training.data.TDTuple;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MakeSets {

    private static HarddriveProperties hp = new HarddriveProperties("en.symbol", "/Finns/ej/makesets.properties");

    public static void main(String[] args) throws Exception {
        String input_folder = hp.getString("input.folder", null);
        if(input_folder == null) {
            System.out.println("Ingen inkatalog");
            System.exit(-1);
        }
        File inputFolder = new File(input_folder);
        if(!inputFolder.exists()) {
            System.out.println("Inkatalog en finns ej");
            System.exit(-1);
        }
        String filter = hp.getString("input.filter", null);
        String[] inputFiles = inputFolder.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(filter == null) {
                    return true;
                }
                return name.contains(filter);
            }
        });
        if(inputFiles == null || inputFiles.length == 0) {
            System.out.println("Inga infiler");
            System.exit(-1);
        }
        List<String> posits = new ArrayList<>();
        for(String inputFile : inputFiles) {
            File file = new File(inputFolder, inputFile);
            if(!file.exists()) {
                System.out.println("Trots att filen "+inputFile+" silats fram verkar den inte finnas");
                System.exit(-1);
            }
            System.out.println("Fil "+inputFile);
            FileReader fileFR = new FileReader(file);
            BufferedReader fileBR = new BufferedReader(fileFR);
            String line = null;
            while((line = fileBR.readLine()) != null) {
                if(line != null && line.contains("|")) {
                    posits.add(line);
                }
            }
            fileBR.close();
            fileFR.close();
        }
        System.out.println("Antal "+posits.size());
        boolean shuffle = hp.getBoolean("details.shuffle", false);
        if(shuffle) {
            Collections.shuffle(posits);
        }
        String output_folder = hp.getString("output.folder", null);
        if(output_folder == null) {
            System.out.println("Ingen utkatalog");
            System.exit(-1);
        }
        File outputFolder = new File(output_folder);
        if(!outputFolder.exists()) {
            System.out.println("Utkatalog en finns ej");
            System.exit(-1);
        }
        Integer output_size = hp.getInt("output.size", null);
        if(output_size == null) {
            System.out.println("Saknar uppgift om utfilernas storlek");
            System.exit(-1);
        }

        String functionS = hp.getString("transform.function", null);
        if(functionS == null) {
            System.out.println("Saknar funktion för transformering");
            System.exit(-1);
        }

        Function1Arg func = Function1Arg.deserialize(functionS);
        if(func == null) {
            System.out.println("Något blev fel när funktion för transformering skulle deserialiseras");
            System.exit(-1);
        }

        List<TDTuple> set = new ArrayList<>();
        int ctr = hp.getInt("output.first", 1);
        int nulls = 0;

        boolean printTrace = hp.getBoolean("details.trace", false);

        double max = -1.0;
        while(posits.size() > 0) {
            String originLineFromFile = posits.remove(0);
            State state = getState(originLineFromFile);//
            Double score = null;
            {
                Double cp = getScoreInCentipawn(originLineFromFile);
                if(cp == null) {
                    nulls++;
                    continue;
                }
                max = Math.max(max, Math.abs(cp));
                score = func.eval(getScoreInCentipawn(originLineFromFile));
            }
            State state1 = null;
            double score1 = 0;
            State state2 = null;
            double score2 = 0;

            // Möjligen logik som kan framstå som omvänd. Alla träningsdata skall tolkas som hur värt det är att stå på
            // ett visst sätt givet att det är den andra som skall dra. Avsikten är att göra ett nät som kan spela vit
            // och att räkna på ett omvänt bräde om nätet skall spela svart. Om vit dragit kommer det vara svarts tur
            // men det är ändå vits perspektiv. !state.turn == true betyder svarts tur och därmed är det också vits
            // perspektiv att lämna ifrån sig turen till denna ställning.
            if(!state.turn) {
                state1 = state;
                score1 = score;
                state2 = ChangeColor.turn(state);
                score2 = -score;
            }
            else {
                state1 = ChangeColor.turn(state);
                score1 = -score;
                state2 = state;
                score2 = score;
            }

            String encoderSymbol = hp.getString("details.encoder", null);
            BoardEncoder be = BoardEncoderFactory.get(encoderSymbol);
            if(be == null) {
                System.out.println("Fick ingen brädeskodare från '"+encoderSymbol+"'");
                System.exit(-1);
            }

            TDTuple tuple = getTuple(state1, score1, originLineFromFile, be);
            if(tuple != null) {
                set.add(tuple);
            }
            tuple = getTuple(state2, score2, originLineFromFile, be);
            if(tuple != null) {
                set.add(tuple);
            }
            if(set.size() >= output_size) {
                if(printTrace) {
                    System.out.println("Skriver ut nummer "+ctr);
                }
                File output = new File(outputFolder, "chessTrainingSet_"+ctr+".nnset");
                ctr++;
                TDImmutableSet iset = new TDImmutableSet(set);
                FileOutputStream fos = new FileOutputStream(output);
                iset.serialize(fos, StandardCharsets.UTF_8);
                fos.close();
                set.clear();
            }
        }
        if(set.size() > 0) {
            if(printTrace) {
                System.out.println("Skriver ut sista nummer "+ctr+" med "+set.size()+" element");
            }
            File output = new File(outputFolder, "chessTrainingSet_"+ctr+".nnset");
            TDImmutableSet iset = new TDImmutableSet(set);
            FileOutputStream fos = new FileOutputStream(output);
            iset.serialize(fos, StandardCharsets.UTF_8);
            fos.close();
        }
        System.out.println("Skapade "+(ctr-1)+" kompletta set med "+output_size+" element");
        System.out.println("Sammanlagt "+((ctr-1)*output_size+set.size())+" träningsdata");
        System.out.println("Tog bort "+nulls+" rader som hade null som godhetsvärde");
        System.out.println("Maximala värdet var "+max);
        set.clear();
    }

    private static Double getScoreInCentipawn(String trainingLine) {
        String[] parts = trainingLine.split("\\|");
        String[] metaParts = parts[0].split(":");
        String score = metaParts[4];
        if(score.equals("NULL")) {
            return null;
        }
        try {
            return Double.parseDouble(metaParts[4]);
        }
        catch(Exception e) {
            System.out.println(trainingLine+" gav "+metaParts[4]+" som godhetsvärde");
            System.exit(-1);
        }
        return null;
    }

    public static State getState(String trainingLine) {
        String[] parts = trainingLine.split("\\|");
        State out = Serialization.getState(parts[1]);
        return out;
    }

    public static TDTuple getTuple(State state, double cp, String originLine, BoardEncoder be) throws Exception {
        return new TDTuple(new TDArray(Encode786.codeBoard(state)), new TDArray(new double[] {cp}), originLine);
    }


}
