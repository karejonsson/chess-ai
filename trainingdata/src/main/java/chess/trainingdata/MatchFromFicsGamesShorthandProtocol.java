package chess.trainingdata;

import chess.board.Match;
import chess.board.Move;
import chess.board.State;
import chess.board.pieces.Pawn;
import chess.board.pieces.Piece;
import chess.operations.Getters;
import chess.operations.Initiation;
import chess.operations.Moving;

import java.io.IOException;
import java.util.List;

public class MatchFromFicsGamesShorthandProtocol {

    public static final String someGame1 = "1. e4 c5 2. Nf3 d6 3. Bc4 Nf6 4. d3 Nc6 5. Nc3 Bg4 6. h3 Bd7 7. a3 Na5 8. Ba2 e6 9. Qe2 Be7 10. Bd2 O-O 11. O-O-O Nc6 12. Rdg1 Nd4 13. Qd1 b5 14. Ng5 a5 15. Ne2 Nxe2+ 16. Qxe2 b4 17. f4 bxa3 18. bxa3 Rb8 19. Bc3 a4 20. Qf3 h6 21. h4 hxg5 22. hxg5 Nxe4 23. Qh5 f6 24. g6 Rb1+ 25. Bxb1 Nxc3 26. Qh7# {Black checkmated} 1-0";
    public static final String someGame2 = "1. Nc3 d5 2. e4 c6 3. d4 dxe4 4. Nxe4 Bf5 5. Ng3 Bg6 6. Be3 e6 7. h4 h6 8. h5 Bh7 9. Bd3 Bxd3 10. Qxd3 Nf6 11. O-O-O Bd6 12. Nf3 Bxg3 13. fxg3 Qe7 14. Bf4 O-O 15. Rh4 Na6 16. g4 Nb4 17. Qe2 Nh7 18. Kb1 b5 19. Bd2 Nd5 20. Rh3 a5 21. Rf1 b4 22. Ne5 Qd6 23. g5 hxg5 24. h6 g6 25. g4 Ra7 26. Rhf3 c5 27. Qd3 c4 28. Qxc4 f6 29. Nxg6 Rc7 30. Qd3 Rfc8 31. Ne5 Kh8 32. Qg6 Qe7 33. b3 Ra8 34. a4 Rg8 35. Qe4 Nf8 36. Nc4 Ra7 37. Qe1 Kh7 38. Kb2 Qd8 39. Qe4+ Ng6 40. Qxe6 Re7 41. Qa6 Ngf4 42. Nxa5 Qc7 43. Nc4 Rd8 44. Bxf4 gxf4 45. a5 Re4 46. Rd3 Re2 47. Rh3 Re4 48. Rh5 Qe7 49. Rf5 Qe6 50. Qxe6 Rxe6 51. Ra1 Kxh6 52. a6 Ne3 53. Rxf4 Kg5 54. Rf2 Nxg4 55. Ra5+ Kh4 56. Rf1 Ra8 57. a7 Kg3 58. Rfa1 Ne3 59. d5 Nxc4+ 60. bxc4 Rd6 61. Rd1 b3 62. c5 Rdd8 63. cxb3 Kf2 64. c6 {Black resigns} 1-0";
    public static final String someGame3 = "1. e4 e5 2. Nf3 Nc6 3. Bc4 Bc5 4. d3 Nf6 5. c3 d6 6. Bb3 a5 7. a4 h6 8. h3 O-O 9. O-O Re8 10. Re1 Bd7 11. Nbd2 Be6 12. Nc4 Qd7 13. Bd2 Rad8 14. Bc2 Qc8 15. Rc1 Bxh3 16. gxh3 Qxh3 17. Ne3 Bxe3 18. Rxe3 Ng4 19. Qf1 Qxf1+ 20. Kxf1 Nxe3+ 21. Bxe3 g5 22. d4 Kg7 23. Nd2 b6 24. Bd3 Ne7 25. Bb5 Rf8 26. Nc4 c6 27. Ba6 Ra8 28. Bb7 Ra7 29. Nxd6 Rb8 30. dxe5 Rbxb7 31. Nxb7 Rxb7 32. b4 Ng6 33. e6 fxe6 34. Rb1 axb4 35. cxb4 Rb8 36. Ra1 Kf7 37. a5 c5 38. Rc1 Ke8 39. axb6 Rxb6 40. Bxc5 Rb7 41. Rb1 Rb5 42. Ke2 Kf7 43. Ke3 h5 44. Kd4 g4 45. Bd6 Ke8 46. Kc4 Rg5 47. b5 Kd7 48. Rd1 Kc8 49. Kb4 Ne5 50. Rc1+ Kd7 51. Bxe5 Rxe5 52. Rc4 Rg5 53. b6 g3 54. fxg3 Rxg3 55. Rc7+ Kd8 56. e5 h4 57. Rc4 Kd7 58. b7 Rg1 59. b8=N+ Kd8 60. Kc5 Kc7 61. Na6+ Kb7 62. Nb4 Rg5 63. Kd6 h3 64. Rh4 Kb6 65. Kxe6 Kc5 66. Rxh3 Kxb4 67. Kd6 Rg2 68. Rh4+ Ka3 69. e6 Rg6 70. Re4 Kb2 71. Kd7 Rg1 72. e7 Rd1+ 73. Ke6 Ra1 74. e8=Q Ra6+ 75. Kd5 Ra5+ 76. Kc4 Rc5+ 77. Kxc5 Kc1 78. Re2 Kb1 79. Qa4 Kc1 80. Qc2# {Black checkmated} 1-0";
    public static final String someGame4 = "1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 a6 6. f4 e5 7. Nf3 Nbd7 8. a4 Qc7 9. Bd3 Be7 10. Qe2 O-O 11. O-O b6 12. fxe5 dxe5 13. Kh1 Nc5 14. Bg5 Be6 15. Nh4 Nxd3 16. cxd3 Kh8 17. Nf5 Ng8 18. Be3 Rad8 19. Nxe7 Nxe7 20. a5 bxa5 21. Na4 Qd6 22. Qf2 Rc8 23. Bc5 Qc7 24. Rfc1 Rfe8 25. Rc3 Nc6 26. Nb6 Rb8 27. Nd5 Qd7 28. Nf6 gxf6 29. Qxf6+ Kg8 30. d4 Rb3 31. d5 Bxd5 32. Qg5+ Kh8 33. Rd1 Rxc3 34. bxc3 Qd8 35. Qh5 Qf6 36. exd5 Nb8 37. Qh3 a4 38. c4 Qf4 39. Be3 Qe4 40. c5 Rg8 41. Qf3 Qxf3 42. gxf3 Rc8 43. Ra1 Rd8 44. c6 Rc8 45. Rc1 a3 46. c7 Kg7 47. d6 Kf6 48. cxb8=R Rxb8 49. Ra1 Ke6 50. Rxa3 Ra8 51. Bc5 a5 52. Ra4 Kd5 53. Ba3 Ra7 54. Kg2 f6 55. Kg3 f5 56. Kh4 Ke6 57. Rc4 Rb7 {Black resigns} 1-0";
    public static final String someGame5 = "1. e4 c6 2. Nc3 d5 3. d4 dxe4 4. Nxe4 Nd7 5. c3 Ngf6 6. Nxf6+ Nxf6 7. Nf3 Bg4 8. Be2 e6 9. Ne5 Bxe2 10. Qxe2 Bd6 11. Bg5 h6 12. Bxf6 gxf6 13. Nc4 Rg8 14. g3 Be7 15. O-O Qc7 16. Rfe1 O-O-O 17. Qh5 Rg6 18. Rad1 b5 19. Ne3 a6 20. a4 bxa4 21. Ra1 a3 22. bxa3 c5 23. Rab1 cxd4 24. cxd4 Bxa3 25. Qe2 a5 26. Qa6+ Kd7 27. Rb7 Qxb7 28. Qxb7+ Ke8 29. Qb5+ Kf8 30. Qxa5 Be7 31. Qa7 Re8 32. d5 f5 33. Qd4 Kg8 34. d6 Rd8 35. dxe7 Rxd4 36. e8=Q+ Kg7 37. Nxf5+ exf5 38. Qe5+ Kg8 39. Qxd4 Re6 40. Qd8+ Kh7 41. Rxe6 fxe6 42. Qe7+ Kg8 43. Qxe6+ Kf8 44. Qxh6+ Kg8 45. Qg6+ Kf8 46. Qxf5+ Ke8 47. Qd7+ Kxd7 48. h4 Kd6 49. h5 Ke5 50. h6 Kf6 51. h7 Kg7 52. g4 Kxh7 53. g5 Kg6 54. f4 Kf5 55. Kf1 Ke4 56. g6 Kxf4 57. g7 Ke5 58. g8=Q Kd6 59. Ke2 Ke7 60. Kd3 Kd7 61. Kd4 Ke7 62. Ke5 Kd7 63. Qc4 Ke8 64. Qc7 Kf8 65. Kf6 Ke8 66. Qc8# {Black checkmated} 1-0";
    public static final String someGame6 = "1. e4 e5 2. Nf3 Nc6 3. Bc4 Nf6 4. d3 Be7 5. O-O d6 6. c3 O-O 7. a4 a6 8. Re1 Na5 9. Ba2 c5 10. Na3 Bd7 11. Bd2 Qe8 12. Qb1 Bg4 13. Nc2 Nh5 14. Qd1 Nf4 15. Bxf4 exf4 16. Bd5 Nc6 17. d4 Bf6 18. Qd2 Bxf3 19. gxf3 g5 20. h4 Kh8 21. hxg5 Bxg5 22. Kf1 f5 23. exf5 Qh5 24. Qd3 Ne7 25. Bxb7 Rab8 26. Qxa6 Nxf5 27. Be4 Ng3+ 28. Kg2 Nxe4 29. Rh1 Qg6 30. fxe4 Qxe4+ 31. Kh2 f3 32. Kg1 Qg4+ 33. Kf1 Qg2+ 34. Ke1 Rfe8+ 35. Ne3 Rxb2 36. Rf1 Bxe3 {White resigns} 0-1";
    public static final String someGame7 = "1. d4 Nf6 2. e3 d5 3. f4 e6 4. Nf3 Be7 5. c3 O-O 6. Bd3 c5 7. Qe2 c4 8. Bc2 Nc6 9. O-O Qd6 10. Ne5 a5 11. a3 h5 12. g3 b5 13. h3 a4 14. Kg2 h4 15. g4 Bd7 16. Rg1 g6 17. Kf2 Be8 18. Nd2 Ra7 19. Rg2 g5 20. Kg1 Nd7 21. Ndf3 Ncxe5 22. fxe5 Qc6 23. e4 f6 24. exf6 Bxf6 25. e5 Be7 26. Nxg5 Qb6 27. Kh1 Bxg5 28. Bxg5 Nb8 29. Bxh4 Raf7 30. Rag1 Rf3 31. Rh2 Nc6 32. Rg3 Rf1+ 33. Rg1 R1f4 34. Bg3 Rf3 35. Rf2 Rxf2 36. Bxf2 Nxe5 37. Rf1 Nf7 38. Bh4 Bd7 39. Bg6 Qd6 40. Bxf7+ Rxf7 41. Rxf7 Kxf7 42. Qf3+ Kg8 43. Qf6 b4 44. axb4 Qf8 45. Qxf8+ Kxf8 46. Kg2 Kg8 47. Kf3 Bc6 48. Kf4 Be8 49. Ke5 Bd7 50. Be7 Kf7 51. Bc5 Kg8 52. h4 Bc8 53. h5 Kg7 54. g5 Kh7 55. b5 Kg8 56. b6 Kh7 57. Kf6 Kg8 58. g6 Bb7 59. h6 Bc8 60. Be7 Kh8 61. Ke5 Kg8 62. Bf6 Kf8 63. h7 Ke8 64. h8=Q+ Kd7 65. Qxc8+ Kxc8 66. g7 Kb7 67. g8=Q a3 68. bxa3 Kxb6 69. Qxe6+ Ka5 70. Qxd5+ Ka4 71. Qxc4+ Kxa3 72. Qd3 Kb3 73. c4+ Ka4 74. c5 Ka5 75. c6 Kb6 76. d5 Ka7 77. Qc4 Kb6 78. d6 Ka7 79. c7 Kb6 80. d7 Ka5 81. d8=Q Kb6 82. c8=Q+ Ka7 83. Qdd7+ Kb6 84. Qc8c6+ Ka5 85. Q4b5# {Black checkmated} 1-0";

    public static Move getMove(State stateMovingFrom, List<Move> possibleMoves, String moveShort) {
        String moveCleansed = moveShort.
                replaceAll("x", "").
                replaceAll("#", "").
                replaceAll("\\+", "").trim();
                //toLowerCase();
        moveCleansed = moveCleansed.substring(0, 1)+moveCleansed.substring(1).toLowerCase();
        //System.out.println("moveShort="+moveShort+", moveCleansed="+moveCleansed);
        for(Move m : possibleMoves) {
            if(MatchMoveToNotation.isSame_FicsGameNotation(stateMovingFrom, m, moveCleansed)) {
                //System.out.println("JA: m="+m+" = "+moveShort);
                return m;
            }
        }
        System.out.println("Draget finns inte ("+moveShort+")");
        return null;
    }

    public static void advance(Match match, String oneFullMove) {
        //System.out.println("MatchFromProtocol, oneFullMove="+oneFullMove);
        if(oneFullMove == null) {
            return;
        }
        if(oneFullMove.trim().length() == 0) {
            return;
        }
        String[] moves = oneFullMove.trim().split(" ");
        for(String move : moves) {
            List<Move> possibleMoves = match.state.getLegalMoves();
            Moving.real(match, getMove(match.state, possibleMoves, move));
        }
    }

    public static Match restore(String game) {
        Match match = new Match(Initiation.createStandardInitialState());
        restore(match, game);
        return match;
    }

    public static void restore(Match match, String game) {
        String pureMoves = game.substring(0, game.indexOf("{")).trim();
        //State s = Initiation.createStandardInitialState();
        //m.init(s);
        int move = 1;
        int idx = pureMoves.indexOf("1.");
        while(idx != -1) {
            int idxNext = pureMoves.indexOf(""+(move+1)+".");
            String oneMove = null;
            if(idxNext != -1) {
                oneMove = pureMoves.substring(idx, idxNext).trim();
            }
            else {
                oneMove = pureMoves.substring(idx).trim();
            }
            oneMove = oneMove.substring(oneMove.indexOf(".")+1).trim();
            //System.out.println("Ett drag '"+oneMove+"'");
            advance(match, oneMove);
            idx = idxNext;
            move++;
        }
    }

    public static final String testfile = "/Users/karejonsson/Documents/workspace/chess-ai/trainingdata/tmp/games/ficsgamesdb_2020_CvC_nomovetimes_241533.pgn";

    public static void main(String[] args) throws IOException {
        Match match = new Match(Initiation.createStandardInitialState());
        try {
            restore(match, someGame7);
        }
        catch(Exception e) { }
        match.write(System.out);
        //System.out.println(match.state.toString());
    }

}
