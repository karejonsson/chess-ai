package chess.trainingdata.encoders;

import chess.board.State;

public interface BoardEncoder {

    double[] codeBoard(State state);

}
