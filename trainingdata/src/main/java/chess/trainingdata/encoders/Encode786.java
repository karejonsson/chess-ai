package chess.trainingdata.encoders;

import chess.board.Move;
import chess.board.State;
import chess.operations.Serialization;

import java.util.ArrayList;
import java.util.List;

public class Encode786 {

    public static double[] codeBoard(State state) {
        List<Double> out = new ArrayList<>();
        String board = Serialization.getBoardString(state);
        for(int i = 0 ; i < board.length() ; i++) {
            Double[] square = encodePieceToSquare(board.charAt(i));
            for(Double d : square) {
                out.add(d);
            }
        }
        String castling = Serialization.getCastlingString(state);
        for(int i = 0 ; i < castling.length() ; i++) {
            out.add(castling.charAt(i) == 'F' ? 1.0 : 0.0);
        }
        Move m = state.lastMove;
        if(m.enpKill != null) {
            for(byte i = 0 ; i < 8 ; i++) {
                out.add(i == m.enpKill.file ? 1.0 : 0.0);
            }
        }
        else {
            for(int i = 0 ; i < 8 ; i++) {
                out.add(0.0);
            }
        }

        double[] da = new double[out.size()];
        for(int i = 0 ; i < out.size() ; i++) {
            da[i] = out.get(i);
        }
        return da;
    }

    private static Double[] encodePieceToSquare(char c) {
        Double[] out = new Double[12];
        for(int i = 0 ; i < 12 ; i++) {
            out[i] = 0.0;
        }
        if(c == 'P') {
            out[0] = 1.0;
        }
        if(c == 'N') {
            out[1] = 1.0;
        }
        if(c == 'B') {
            out[2] = 1.0;
        }
        if(c == 'R') {
            out[3] = 1.0;
        }
        if(c == 'Q') {
            out[4] = 1.0;
        }
        if(c == 'K') {
            out[5] = 1.0;
        }
        if(c == 'p') {
            out[6] = 1.0;
        }
        if(c == 'n') {
            out[7] = 1.0;
        }
        if(c == 'b') {
            out[8] = 1.0;
        }
        if(c == 'r') {
            out[9] = 1.0;
        }
        if(c == 'q') {
            out[10] = 1.0;
        }
        if(c == 'k') {
            out[11] = 1.0;
        }
        return out;
    }

}
