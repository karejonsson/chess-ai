package chess.trainingdata.encoders;

public class BoardEncoderFactory {

    public static BoardEncoder get(String symbol) {
        if(symbol.trim().toLowerCase().contains("786")) {
            return s -> Encode786.codeBoard(s);
        }
        return null;
    }

}
