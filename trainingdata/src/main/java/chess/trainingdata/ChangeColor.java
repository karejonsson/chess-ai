package chess.trainingdata;

import chess.board.Board;
import chess.board.Move;
import chess.board.Square;
import chess.board.State;
import chess.board.pieces.Piece;
import chess.operations.Getters;
import chess.operations.Setters;

public class ChangeColor {

    public static Square turn(Square s) {
        if(s == null) {
            return null;
        }
        return new Square((byte)(Board.ranks-s.rank-1), s.file);
    }

    public static Move turn(Move m) {
        Move out = new Move();
        out.to = turn(m.to);
        out.promotion = m.promotion;
        out.promoteTo = m.promoteTo;
        out.enpKill = turn(m.enpKill);
        out.castle = m.castle;
        out.message = m.message;
        return out;
    }

    public static State turn(State s) {
        State out = new State();
        out.turn = !s.turn;
        out.board = new Piece[Board.ranks][];
        for(byte r = 0 ; r < Board.ranks/2 ; r++) {
            out.board[r] = new Piece[Board.files];
            out.board[(byte)(Board.ranks-r-1)] = new Piece[Board.files];
            for(byte f = 0 ; f < Board.files ; f++) {
                Piece p1 = Getters.getPiece(s, r, f);
                Piece p2 = Getters.getPiece(s, (byte)(Board.ranks-r-1), f);
                if(p2 != null) {
                    Setters.setPiece(p2.copyToColor(!p2.getColor()), out, r, f);
                }
                if(p1 != null) {
                    Setters.setPiece(p1.copyToColor(!p1.getColor()), out, (byte)(Board.ranks-r-1), f);
                }
            }
        }
        out.lastMove = turn(s.lastMove);
        out.lastMove.state = out;
        out.bPieces = new byte[s.wPieces.length];
        out.wPieces = new byte[s.bPieces.length];
        System.arraycopy(s.wPieces, 0, out.bPieces, 0, out.bPieces.length);
        System.arraycopy(s.bPieces, 0, out.wPieces, 0, out.bPieces.length);
        out.blackKing = turn(s.blackKing);
        out.whiteKing = turn(s.whiteKing);

        out.whiteKingMoved = s.blackKingMoved;
        out.blackKingMoved = s.whiteKingMoved;
        out.whiteKRookMoved = s.blackKRookMoved;
        out.whiteQRookMoved = s.blackQRookMoved;
        out.blackKRookMoved = s.whiteKRookMoved;
        out.blackQRookMoved = s.whiteQRookMoved;
        out.whiteHasCastled = s.blackHasCastled;
        out.blackHasCastled = s.whiteHasCastled;

        out.check = s.check;
        out.repetitions = s.repetitions;
        out.timeBlack = s.timeWhite;
        out.timeWhite = s.timeBlack;

        out.isWon = s.isWon;

        return out;
    }

}
