package chess.trainingdata;

import chess.board.Move;
import chess.board.State;
import chess.board.pieces.Pawn;
import chess.board.pieces.Piece;
import chess.operations.Getters;

public class MatchMoveToNotation {

    public static boolean isSame_FicsGameNotation(State stateMovingFrom, Move m, String moveCleansed) {
        //System.out.println("Drag "+m+", moveCleansed='"+moveCleansed+"'");
        if (m.castle == 'K' && moveCleansed.equalsIgnoreCase("O-O")) {
            return true;
        }
        if (m.castle == 'Q' && moveCleansed.equalsIgnoreCase("O-O-O")) {
            return true;
        }
        String moveWithoutPromotion = moveCleansed;
        if (moveWithoutPromotion.contains("=")) {
            moveWithoutPromotion = moveWithoutPromotion.substring(0, moveWithoutPromotion.indexOf("="));
        }
        if (!moveWithoutPromotion.endsWith(m.to.toString().toLowerCase())) {
            // Slutar inte på rätt ruta
            return false;
        }

        // Slutar på rätt ruta
        String pieceChar = moveCleansed.substring(0, 1);

        if (m.promotion) {
            if (m.promoteTo == moveCleansed.substring(moveCleansed.length() - 1).toUpperCase().charAt(0)) {
                // Det är en pjäskonvertering och till rätt typ
                if (moveWithoutPromotion.length() == 2) {
                    // Det är raden för målrutan. Eftersom det bara är två tecken är saken klar.
                    return true;
                }
                //System.out.println("moveWithoutPromotion="+moveWithoutPromotion+", m.from="+m.from.toString()+", pieceChar="+pieceChar);
                if (moveWithoutPromotion.length() == 3) {
                    // Det är raden för utgångsrutan
                    if (m.from.toString().toLowerCase().contains(pieceChar)) {
                        // Det radspecificerande tecknet finns i utgångsrutans koordinat. Det är rätt.
                        return true;
                    }
                }
            }
            return false;
        }

        Piece piece = null;
        try {
            piece = Getters.getPiece(stateMovingFrom, m.from);
        } catch (Exception e) {
            System.out.println("State = " + stateMovingFrom + ", till = " + m.to);
            e.printStackTrace();
            System.exit(0);
        }

        //System.out.println("moveCleansed="+moveCleansed+", pieceChar="+pieceChar);
        if (piece.index() == Pawn.idx) {
            // Ett bondedrag. Det är raden för målrutan eller raden för utgångsrutan
            // Notera att de gånger ett extratecken specificerar vilken bonde som slagit är det en bokstav.
            if (moveCleansed.length() == 2) {
                // Det är raden för målrutan. Eftersom det bara är två tecken är saken klar.
                return true;
            }
            if (moveCleansed.length() == 3) {
                // Det är raden för utgångsrutan
                if (m.from.toString().toLowerCase().contains(pieceChar)) {
                    // Det radspecificerande tecknet finns i utgångsrutans koordinat. Det är rätt.
                    return true;
                }
            }
            return false;
        }
        // Det börjar med en stor bokstav, dvs inte en bonde
        String movingPieceCharacter = Getters.getCharForMoveNotation(piece);
        if (movingPieceCharacter.equals(pieceChar)) {
            // Det är rätt pjästyp
            // Rdg1
            if (moveCleansed.length() == 3) {
                // Draget har ej ett ytterligare specificerande tecken så det är det rätta draget
                return true;
            }
            String specifyingCharacter = moveCleansed.substring(1, 2);
            if (m.from.toString().toLowerCase().contains(specifyingCharacter)) {
                // Rutans koordinat innehåller det extra specificerande tecknet. Det är rätt drag
                return true;
            }
        }
        return false;
    }

    public static boolean isSame_TwoSquaresLower(Move m, String move) {
        boolean correctSquares =
            m.from.toString().equalsIgnoreCase(move.substring(0, 2)) &&
            m.to.toString().equalsIgnoreCase(move.substring(2, 4));
        if(!correctSquares) {
            return false;
        }
        if(move.length() == 4) {
            return true;
        }
        //System.out.println("move="+move+", m="+m);
        char castlingChar = move.toUpperCase().charAt(4);
        //System.out.println("move="+move+", m="+m+", m.promoteTo="+m.promoteTo+", castlingChar="+castlingChar);
        if(m.promoteTo == castlingChar) {
            return true;
        }
        return false;
    }

}