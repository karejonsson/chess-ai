package chess.trainingdata;

import chess.board.Match;
import chess.operations.Serialization;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class MakeProtocols {

    public static final String pgn_source_folder = "/Users/karejonsson/Documents/workspace/chess-ai/trainingdata/tmp/pgns";

    public static void main(String[] args) throws IOException {
        File pgnFolder = new File(pgn_source_folder);
        String[] pgnFileNames = pgnFolder.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".pgn");
            }
        });
        for(String onePgn : pgnFileNames) {
            File protocolFile = new File(pgnFolder, onePgn+".repr");
            FileOutputStream fos = new FileOutputStream(protocolFile);
            handle(fos, pgnFolder, onePgn);
        }
    }

    // [FICSGamesDBGameNo "430002398"]

    public static String extractId(String line) {
        int idx1 = line.indexOf("\"");
        int idx2 = line.indexOf("\"", idx1+1);
        return line.substring(idx1+1, idx2);
    }

    private static void handle(FileOutputStream fos, File folder, String onePgn) throws IOException {
        File pgnFile = new File(folder, onePgn);
        FileReader fr = new FileReader(pgnFile);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        String id = null;
        String game = null;
        while((line = br.readLine()) != null) {
            if(line.startsWith("[FICSGamesDBGameNo")) {
                id = extractId(line);
            }
            if(line.startsWith("1.")) {
                game = line;
            }
            if(id != null && game != null) {
                handle(fos, folder.getCanonicalPath()+File.separator+onePgn, id, game);
                id = null;
                game = null;
            }
        }
        br.close();
        fr.close();
    }

    private static void handle(FileOutputStream fos, String dir, String id, String game) throws IOException {
        //System.out.println("ID: '"+id+"'");
        //System.out.println("GAME: '"+game+"'");
        Match m = null;
        try {
            m = MatchFromFicsGamesShorthandProtocol.restore(game);
            m.write(s -> {
                String player = s.turn ? "W" : "B";
                String movenr = ""+s.fullMoves;
                fos.write((id+":"+player+":"+movenr+"|"+ Serialization.getStateString(s)+"\n").getBytes(StandardCharsets.UTF_8));
            });
        }
        catch(Exception e) {
            System.out.println("Dir "+dir+", id "+id+" funkade ej");
        }
    }

}
