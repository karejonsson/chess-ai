package chess.trainingdata;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import chess.trainingdata.engines.*;

public class ExternalClassifyer {

    public final static String analysisLineRegex = "info depth ([\\w]*) seldepth [\\w]* multipv ([\\w]*) score (cp ([\\-\\w]*)|mate ([\\w*])) [\\s\\w]*pv ([\\w]*)\\s*([\\s\\w]*)";
    public final static Pattern pattern = Pattern.compile(analysisLineRegex);

    public static List<String> evaluate(CmdClient client, String fen_position, long millisToPonder) throws InterruptedException, ExecutionException, TimeoutException {

        // We set the give position
        client.command("ucinewgame", UnaryOperator.identity(), s -> true, 2000l);

        // We set the give position
        client.command("position fen " + fen_position, UnaryOperator.identity(), s -> s.startsWith("readyok"), 2000l);

        // We set MultiPV to some value that implies all
        client.command("setoption name MultiPV value 160", UnaryOperator.identity(), s -> s.startsWith("readyok"), 2000l);

        Map<Integer, String> bestMoves =
                client.command(
                        "go movetime "+millisToPonder,
                        lines -> {
                            Map<Integer, String> result = new TreeMap<>();
                            for (String line : lines) {
                                Matcher matcher = pattern.matcher(line);
                                if (matcher.matches()) {
                                    Integer pv = Integer.parseInt(matcher.group(2));
                                    String move = matcher.group(6);
                                    String score = matcher.group(4);
                                    result.put(pv, move + ":" + score);
                                }
                            }
                            return result;
                        },
                        s -> s.startsWith("bestmove"),
                        millisToPonder+3000l);

        List<String> out = new ArrayList<>();
        bestMoves.forEach((k, v) -> {
            out.add(v);
            //System.out.printf("%d %s\n", k, v);
        });

        //client.close();
        return out;
    }

    // ;
    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {

        CmdClient client = new CmdClient();
        client.start("/usr/local/bin/stockfish");

        // We initialise the engine to use the UCI interface
        client.command("uci", UnaryOperator.identity(), (s) -> s.startsWith("uciok"), 2000l);

        String position = "r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 3 3";

        List<String> moves = evaluate(client, position, 10000);
        moves.forEach(m -> System.out.println(m));

        moves = evaluate(client, position, 10000);
        moves.forEach(m -> System.out.println(m));

        client.close();
    }

}
