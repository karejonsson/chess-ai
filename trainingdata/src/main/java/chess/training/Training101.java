package chess.training;

import general.reuse.properties.HarddriveProperties;
import se.prv.ai.mutating.training.algo.standard.*;
import se.prv.ai.mutating.training.net.structure.TTNetwork;
import se.prv.ai.mutating.training.schedule.ScheduleAbstraction;
import se.prv.ai.mutating.training.threading.ThreadDistribution;
import se.prv.ai.mutating.visualize.Spawn;
import se.prv.ai.mutating.visualize.manipulation.ExtractVisualizingNetwork;
import se.prv.ai.mutating.visualize.structure.VTNetwork;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Training101 {

    private static HarddriveProperties hp = new HarddriveProperties("en.symbol", "/Finns/ej/training.properties");

    public static void main(String[] args) throws Exception {
        String input_folder = hp.getString("training.folder", null);
        if(input_folder == null) {
            System.out.println("Ingen katalog för träningsdata");
            System.exit(-1);
        }
        File inputFolder = new File(input_folder);
        if(!inputFolder.exists()) {
            System.out.println("Katalog för träningsdata finns ej");
            System.exit(-1);
        }
        String filter = hp.getString("training.filter", null);
        String[] inputFiles = inputFolder.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(filter == null) {
                    return true;
                }
                return name.contains(filter);
            }
        });
        if(inputFiles == null || inputFiles.length == 0) {
            System.out.println("Inga träningsdatafiler");
            System.exit(-1);
        }

        String fileWithStartNet = hp.getString("in.file", null);
        if(fileWithStartNet == null || fileWithStartNet.trim().length() == 0) {
            System.out.println("Inget filnamn för nät att starta från");
            System.exit(-1);
        }
        fileWithStartNet = fileWithStartNet.trim();

        File f = new File(fileWithStartNet);
        if(!f.exists()) {
            System.out.println("Ingen fil för nät att starta från");
            System.exit(-1);
        }

        boolean training_trace = hp.getBoolean("training.trace", true);

        final int threads = hp.getInt("training.threads", 2);
        if(training_trace) {
            System.out.println("Antal trådar: "+threads);
        }
        FileInputStream fis = new FileInputStream(f);
        TTNetwork tnet = TTNetwork.deserialize(fis, StandardCharsets.UTF_8, threads);
        if(tnet == null) {
            System.out.println("Ingen nät i filen för nät att starta från");
            System.exit(-1);
        }

        save_folder = hp.getString("save.folder", "");
        if(save_folder == null ||save_folder.trim().length() == 0) {
            System.out.println("Inget förslag på katalog att spara i");
            System.exit(-1);
        }

        File saveFolder = new File(save_folder);
        {
            if(!saveFolder.exists()) {
                System.out.println("Förslag på katalog att spara i finns inte. save_folder="+save_folder);
                System.exit(-1);
            }
            if(!saveFolder.isDirectory()) {
                System.out.println("Förslag på katalog att spara i är inte en katalog. save_folder="+save_folder);
                System.exit(-1);
            }
        }

        millisBetweenSaves = hp.getLong("save.frequency", 18000000l);

        dateformat = hp.getString("save.dateformat", "yyyyMMdd-HH:mm:ss");

        filename = hp.getString("save.filename", "<date>.nnet");

        Integer epochs = hp.getInt("training.epochs", 1);
        final Integer batchSize = hp.getInt("training.batchsize", 100);
        if(training_trace) {
            System.out.println("Epoker "+epochs+", batchsize="+batchSize);
        }

        Double fractionToLearn = hp.getDouble("training.fractionToLearn", 1.0);
        NeuronErrorUpdater updater = new NeuronErrorUpdaterNormalizing(0.5, threads, batchSize);
        ThreadDistribution td = new ThreadDistribution(tnet, fractionToLearn, updater);

        ScheduleAbstraction scheduler = new ScheduleAbstraction(inputFolder, inputFiles, td);
        scheduler.setBatchSize(batchSize);
        scheduler.setFilenameTemplate(filename);
        scheduler.setSaveFolder(saveFolder);
        scheduler.setDateFormat(dateformat);

        ExtractVisualizingNetwork evn = new ExtractVisualizingNetwork();
        tnet.traverseLayers(evn);
        VTNetwork vtn = evn.getNetworkVisualizer();
        Spawn.gui(vtn, scheduler, 700);

        /*
        double cumulativeError = 0.0;

        for(int epoch = 0 ; epoch < epochs ; epoch++) {
            //int epochTrainingsCounter = 0;
            cumulativeError = 0.0;
            if(training_trace) {
                System.out.println("Epoch "+epoch);
            }
            for(String inputFile : inputFiles) {
                File trainingData = new File(inputFolder, inputFile);
                fis = new FileInputStream(trainingData);
                List<TDImmutableSet> subsets = null;
                {
                    TDImmutableSet largeSet = TDImmutableSet.deserialize(fis, StandardCharsets.UTF_8);
                    fis.close();
                    if(largeSet == null) {
                        if(training_trace) {
                            System.out.println("Filen "+inputFile+" kunde inte läsas in");
                        }
                        continue;
                    }
                    fis.close();
                    if(training_trace) {
                        System.out.println("Filen "+inputFile+" har "+largeSet.size()+" träningsdata");
                    }
                    subsets = largeSet.splitToGroupsOfMaximalSize(batchSize);
                }
                for(int i = 0 ; i < subsets.size() ; i++) {
                    double error = td.train(subsets.get(i));
                    cumulativeError += error;
                    td.updateSynapses();
                    if(training_trace) {
                        System.out.println("trainings="+td.getTrainings()+", error="+error);
                    }
                }
                saveIfIntervalPassed(epoch, (int) Math.round(cumulativeError), tnet);
            }
            System.out.println("Epok "+epoch+", kumulativt fel: "+cumulativeError+", tid "+(new Date()));
        }
        save("sista", (int) Math.round(cumulativeError), tnet);

        if(training_trace) {
            System.out.println("Slut "+(new Date()));
        }
        */
    }

    public static long millisOnSave = System.currentTimeMillis();
    public static long millisBetweenSaves = 0;
    public static String dateformat = null;
    public static String filename = null;
    public static String save_folder = null;

    public static void saveIfIntervalPassed(int epoch, int error, TTNetwork tnet) throws Exception {
        if(millisOnSave + millisBetweenSaves > System.currentTimeMillis()) {
            return;
        }
        save(""+epoch, error, tnet);
    }

    public static void save(String epoch, int error, TTNetwork tnet) throws Exception {
        String moment = (new SimpleDateFormat(dateformat)).format(new Date());
        String filenameFixed = filename.replace("<date>", moment);
        filenameFixed = filenameFixed.replace("<epoch>", epoch);
        filenameFixed = filenameFixed.replace("<error>", ""+error);

        File out = new File(save_folder, filenameFixed);
        FileOutputStream fos = new FileOutputStream(out);
        tnet.serialize(fos);
        fos.close();

        millisOnSave = System.currentTimeMillis();
    }

}
