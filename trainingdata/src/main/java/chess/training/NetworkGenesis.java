package chess.training;

import general.reuse.properties.HarddriveProperties;
import se.prv.ai.mutating.runtime.functions.*;
import se.prv.ai.mutating.runtime.net.RTNetwork;
import se.prv.ai.mutating.training.algo.manipulation.SynapseWeightInitializer;
import se.prv.ai.mutating.training.net.structure.TTNetwork;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class NetworkGenesis {

    private static HarddriveProperties hp = new HarddriveProperties("en.symbol", "/Finns/ej/genesis.properties");

    public static void main(String[] args) throws Exception {
        String store_folder = hp.getString("store.folder", null);
        if(store_folder == null) {
            System.out.println("Ingen inkatalog");
            System.exit(-1);
        }
        File inputFolder = new File(store_folder);
        if(!inputFolder.exists()) {
            System.out.println("Inkatalog en finns ej");
            System.exit(-1);
        }
        String store_file = hp.getString("store.file", null);
        if(store_file == null) {
            System.out.println("Ingen fil");
            System.exit(-1);
        }
        File inputFile = new File(inputFolder, store_file);
        if(inputFile.exists()) {
            System.out.println("Den fil som skall skapas finns redan");
            if(!inputFile.delete()) {
                System.out.println("Den befintliga filen kunde inte tas bort");
                System.exit(-1);
            }
        }

        Integer in = hp.getInt("net.input.in", null);
        if(in == null) {
            System.out.println("Saknar antal in-noder");
            System.exit(-1);
        }

        String infuncS = hp.getString("net.input.infunc", null);
        if(infuncS == null) {
            System.out.println("Saknar in-funktion");
            System.exit(-1);
        }
        Function1Arg infunc = Function1Arg.deserialize(infuncS);
        if(infunc == null) {
            System.out.println("Kunde inte deserialisera in-funktion från '"+infuncS+"'");
            System.exit(-1);
        }

        RTNetwork net = new RTNetwork(infunc);

        List<Function0Arg> initiators = new ArrayList<>();

        Integer out = hp.getInt("net.input.out", null);
        if(out == null) {
            System.out.println("Saknar antal ut-noder");
            System.exit(-1);
        }

        String biasfuncS = hp.getString("net.input.biasfunc", null);
        if(biasfuncS == null) {
            System.out.println("Saknar bias-funktion");
            System.exit(-1);
        }
        Function0Arg biasfunc = Function0Arg.deserialize(biasfuncS);
        if(biasfunc == null) {
            System.out.println("Kunde inte deserialisera bias-funktion från '"+biasfunc+"'");
            System.exit(-1);
        }

        String activfuncS = hp.getString("net.input.activation", null);
        if(activfuncS == null) {
            System.out.println("Saknar aktiverings-funktion");
            System.exit(-1);
        }
        Function1Arg activeFunc = Function1Arg.deserialize(activfuncS);
        if(activeFunc == null) {
            System.out.println("Kunde inte deserialisera aktiverings-funktion från '"+activfuncS+"'");
            System.exit(-1);
        }

        net.setInputLayer(in, out, activeFunc, biasfunc);

        System.out.println("Lägger till in-lager. in="+in+", out="+out+", actfunc="+activeFunc+", biasfunc="+biasfunc);

        String initfuncS = hp.getString("net.input.initfunc", null);
        if(initfuncS == null) {
            System.out.println("Saknar initieringserings-funktion för inputlagret");
            System.exit(-1);
        }
        Function0Arg initFunc = Function0Arg.deserialize(initfuncS);
        if(initFunc == null) {
            System.out.println("Kunde inte deserialisera initierings-funktion från '"+initfuncS+"'");
            System.exit(-1);
        }

        initiators.add(initFunc);

        int ctr = 1;
        while (true) {
            String property_out = "net.hidden."+ctr+".out";
            out = hp.getInt(property_out, null);
            if(out == null) {
                break;
            }
            String property_activation = "net.hidden."+ctr+".activation";
            String activation = hp.getString(property_activation, null);
            if(activation == null) {
                System.out.println("Saknar aktiverings-funktion från '"+activfuncS+"' med property "+property_activation);
                System.exit(-1);
            }
            Function1Arg actFunc = Function1Arg.deserialize(activation);
            if(actFunc == null) {
                System.out.println("Kunde inte deserialisera aktiverings-funktion från '"+activation+"' med property "+property_activation);
                System.exit(-1);
            }
            net.addHiddenLayer(out, actFunc);
            System.out.println("Lägger till dolt lager. out="+out+", func="+actFunc);

            String property_initfunc = "net.hidden."+ctr+".initfunc";
            initfuncS = hp.getString(property_initfunc, null);
            if(initfuncS == null) {
                System.out.println("Saknar initierings-funktion för dolt lager "+ctr);
            }
            else {
                Function0Arg initfunc = Function0Arg.deserialize(initfuncS);
                if(initfunc == null) {
                    System.out.println("Kunde inte deserialisera in-funktion från '"+initfuncS+"'");
                    System.exit(-1);
                }
                initiators.add(initfunc);
            }

            ctr++;
        }

        TTNetwork tnet = new TTNetwork(net,1);

        System.out.println("initiators "+initiators.size());//+", l"+tnet.traverseLayers(null);)
        SynapseWeightInitializer swi = new SynapseWeightInitializer(initiators);
        tnet.traverseLayers(swi);

        inputFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(inputFile);
        tnet.serialize(fos);
        fos.close();
    }

}
/*
    public interface JoltHandler {
        void handle(LayerSynapse synapse);
    }

    private JoltHandler handler = null;

    public SynapseWeightUpdaterTilt(JoltHandler handler) {
        this.handler = handler;
    }

 */